package com.org.iisg.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(schema = "hibernate", name = "grade")
public class Grade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "student_id", nullable = false)
    private Long studentId;

    @Column(name = "mark", nullable = false)
    private int mark;

    @Column(name = "date", nullable = false)
    private LocalDate date;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subject_id")
    private Subject subject;

    public Grade() {
    }

    public Grade(Long studentId, int mark, LocalDate date) {
        this.studentId = studentId;
        this.mark = mark;
        this.date = date;
    }

    public Grade(Long id, Long studentId, int mark, LocalDate date, Subject subject) {
        this.id = id;
        this.studentId = studentId;
        this.mark = mark;
        this.date = date;
        this.subject = subject;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }
}
