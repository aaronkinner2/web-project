package com.org.iisg.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "classes", schema = "hibernate")
public class StudyClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 10)
    private String name;

    @Column(name = "math_id")
    private Long mathematicsTeacherId;

    @Column(name = "inform_id")
    private Long informaticsTeacherId;

    @Column(name = "chem_id")
    private Long chemistryTeacherId;

    @Column(name = "phy_id")
    private Long physicsTeacherId;

    @Column(name = "rus_id")
    private Long russianTeacherId;

    @Column(name = "en_id")
    private Long englishTeacherId;

    @OneToMany(targetEntity = User.class, mappedBy = "studyClass", fetch = FetchType.LAZY)
    private List<User> students = new ArrayList<>();

    public StudyClass() {
    }

    public StudyClass(String name, Long mathematicsTeacherId, Long informaticsTeacherId, Long chemistryTeacherId,
                      Long physicsTeacherId, Long russianTeacherId, Long englishTeacherId) {
        this.name = name;
        this.mathematicsTeacherId = mathematicsTeacherId;
        this.informaticsTeacherId = informaticsTeacherId;
        this.chemistryTeacherId = chemistryTeacherId;
        this.physicsTeacherId = physicsTeacherId;
        this.russianTeacherId = russianTeacherId;
        this.englishTeacherId = englishTeacherId;
    }

    public StudyClass(Long id, String name, Long mathematicsTeacherId, Long informaticsTeacherId,
                      Long chemistryTeacherId, Long physicsTeacherId, Long russianTeacherId, Long englishTeacherId) {
        this.id = id;
        this.name = name;
        this.mathematicsTeacherId = mathematicsTeacherId;
        this.informaticsTeacherId = informaticsTeacherId;
        this.chemistryTeacherId = chemistryTeacherId;
        this.physicsTeacherId = physicsTeacherId;
        this.russianTeacherId = russianTeacherId;
        this.englishTeacherId = englishTeacherId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getMathematicsTeacherId() {
        return mathematicsTeacherId;
    }

    public void setMathematicsTeacherId(Long mathematicsId) {
        this.mathematicsTeacherId = mathematicsId;
    }

    public Long getInformaticsTeacherId() {
        return informaticsTeacherId;
    }

    public void setInformaticsTeacherId(Long informaticsTeacherId) {
        this.informaticsTeacherId = informaticsTeacherId;
    }

    public Long getChemistryTeacherId() {
        return chemistryTeacherId;
    }

    public void setChemistryTeacherId(Long chemistryTeacherId) {
        this.chemistryTeacherId = chemistryTeacherId;
    }

    public Long getPhysicsTeacherId() {
        return physicsTeacherId;
    }

    public void setPhysicsTeacherId(Long physicsTeacherId) {
        this.physicsTeacherId = physicsTeacherId;
    }

    public Long getRussianTeacherId() {
        return russianTeacherId;
    }

    public void setRussianTeacherId(Long russianTeacherId) {
        this.russianTeacherId = russianTeacherId;
    }

    public Long getEnglishTeacherId() {
        return englishTeacherId;
    }

    public void setEnglishTeacherId(Long englishTeacherId) {
        this.englishTeacherId = englishTeacherId;
    }

    public List<User> getStudents() {
        return students;
    }

    public void setStudents(List<User> students) {
        this.students = students;
    }

    public void addStudent(User student) {
        this.students.add(student);
    }

    public void removeStudent(User student) {
        this.students.remove(student);
    }
}
