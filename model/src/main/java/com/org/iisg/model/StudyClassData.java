package com.org.iisg.model;

import java.util.List;

public class StudyClassData {

    private final List<User> teachers;

    private final String name;

    public StudyClassData(List<User> teachers, String name) {
        this.teachers = teachers;
        this.name = name;
    }

    public List<User> getTeachers() {
        return teachers;
    }

    public String getName() {
        return name;
    }
}
