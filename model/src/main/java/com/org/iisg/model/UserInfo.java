package com.org.iisg.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import static com.org.iisg.db.parameter.ModelParameter.*;

@Embeddable
public class UserInfo {

    @Column(length = 30, nullable = false)
    private String firstName;

    @Column(length = 30, nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String mail;

    public UserInfo() {
    }

    public UserInfo(String firstName, String lastName, String mail) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMail() {
        return mail;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getCutFullName() {
        return this.firstName.charAt(START_SYMBOL) + DOT + this.lastName;
    }

    public String getFullName() {
        return this.firstName.charAt(START_SYMBOL) + SPACE + this.lastName;
    }
}
