package com.org.iisg.db.parameter;

public interface ModelParameter {

    String DOT = ".";

    String SPACE = " ";

    int START_SYMBOL = 0;
}
