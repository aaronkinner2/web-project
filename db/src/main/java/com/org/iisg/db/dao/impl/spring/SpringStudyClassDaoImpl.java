package com.org.iisg.db.dao.impl.spring;

import com.org.iisg.db.dao.inter.impl.StudyClassDao;
import com.org.iisg.db.repository.StudyClassRepository;
import com.org.iisg.model.StudyClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.org.iisg.db.parameter.DaoParameter.*;

@Service
public class SpringStudyClassDaoImpl implements StudyClassDao {

    @Autowired
    StudyClassRepository studyClassRepository;

    @Override
    public List<StudyClass> getByTeacherIdAndSubjectId(Long teacherId, Long subjectId) {
        List<StudyClass> studyClasses = null;
        switch (subjectId.intValue()) {
            case MATHEMATICS_INT_ID: {
                studyClasses = studyClassRepository.findStudyClassesByMathematicsTeacherId(teacherId);
                break;
            }
            case INFORMATICS_INT_ID: {
                studyClasses = studyClassRepository.findStudyClassesByInformaticsTeacherId(teacherId);
                break;
            }
            case CHEMISTRY_INT_ID: {
                studyClasses = studyClassRepository.findStudyClassesByChemistryTeacherId(teacherId);
                break;
            }
            case PHYSICS_INT_ID: {
                studyClasses = studyClassRepository.findStudyClassesByPhysicsTeacherId(teacherId);
                break;
            }
            case RUSSIAN_INT_ID: {
                studyClasses = studyClassRepository.findStudyClassesByRussianTeacherId(teacherId);
                break;
            }
            case ENGLISH_INT_ID: {
                studyClasses = studyClassRepository.findStudyClassesByEnglishTeacherId(teacherId);
                break;
            }
        }
        return studyClasses;
    }

    @Override
    public StudyClass get(Long id) {
        return studyClassRepository.findStudyClassById(id);
    }

    @Override
    public List<StudyClass> getAll() {
        return studyClassRepository.findStudyClassesByIdNotNullOrderByNameAsc();
    }

    @Override
    public Long create(StudyClass studyClass) {
        studyClassRepository.save(studyClass);
        return studyClassRepository.findStudyClassByNameEquals(studyClass.getName()).getId();
    }

    @Override
    public boolean update(StudyClass studyClass) {
        studyClassRepository.save(studyClass);
        return true;
    }

    @Override
    public void delete(Long id) {
        studyClassRepository.deleteStudyClassById(id);
    }
}
