package com.org.iisg.db.connection;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import static com.org.iisg.db.parameter.DaoParameter.*;

public class JdbcConnectionPool {

    private static final Logger LOGGER = LogManager.getLogger(JdbcConnectionPool.class);

    private static JdbcConnectionPool instance;
    private ComboPooledDataSource comboPooledDataSource;

    private JdbcConnectionPool() {
        final Properties properties = new Properties();
        final InputStream in = JdbcProvider.class.getClassLoader().getResourceAsStream(SQL_CONNECTION_PROPERTIES_FILE);
        try {
            properties.load(in);
            comboPooledDataSource = new ComboPooledDataSource();
            comboPooledDataSource.setDriverClass(properties.getProperty(JDBC_DRIVER_NAME_PROPERTY));
            comboPooledDataSource.setJdbcUrl(properties.getProperty(JDBC_URL_PROPERTY));
            comboPooledDataSource.setUser(properties.getProperty(JDBC_USERNAME_PROPERTY));
            comboPooledDataSource.setPassword(properties.getProperty(JDBC_PASSWORD_PROPERTY));
            comboPooledDataSource.setAcquireRetryAttempts(ACQUIRE_RETRY_ATTEMPTS);
            comboPooledDataSource.setMaxPoolSize(MAX_POOL_SIZE);
            comboPooledDataSource.setMinPoolSize(MIN_POOL_SIZE);
        } catch (IOException | PropertyVetoException e) {
            LOGGER.fatal(CONNECTION_POOL_CREATE_ERROR_LOG + e);
        }
    }

    public static JdbcConnectionPool getInstance() {
        if (instance == null) {
            synchronized (JdbcConnectionPool.class) {
                if (instance == null) {
                    instance = new JdbcConnectionPool();
                }
            }
        }
        return instance;
    }

    public Connection getConnection() {
        try {
            return this.comboPooledDataSource.getConnection();
        } catch (SQLException e) {
            LOGGER.error(CONNECTION_POOL_TAKE_CONNECTION_ERROR_LOG + e);
            return null;
        }
    }

    public void releaseConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            LOGGER.error(CONNECTION_POOL_RELEASE_CONNECTION_ERROR_LOG + e);
        }
    }
}