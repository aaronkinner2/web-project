package com.org.iisg.db.repository;

import com.org.iisg.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findUserById(Long id);

    User findUserByLoginEquals(String login);

    List<User> getUsersByRole_Id(Long roleId);

    List<User> getUsersByStudyClass_IdAndRole_Id(Long classId, Long roleId);

    List<User> getUsersByStudyClass_IdIsNullAndRole_Id(Long roleId);

    List<User> getUsersByRole_IdAndSubject_Id(Long roleId, Long subjectId);

    void deleteUserById(Long id);
}
