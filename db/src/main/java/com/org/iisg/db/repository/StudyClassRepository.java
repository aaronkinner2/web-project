package com.org.iisg.db.repository;

import com.org.iisg.model.StudyClass;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudyClassRepository extends CrudRepository<StudyClass, Long> {

    StudyClass findStudyClassByNameEquals(String name);

    StudyClass findStudyClassById(Long id);

    List<StudyClass> findStudyClassesByIdNotNullOrderByNameAsc();

    List<StudyClass> findStudyClassesByMathematicsTeacherId(Long id);

    List<StudyClass> findStudyClassesByInformaticsTeacherId(Long id);

    List<StudyClass> findStudyClassesByChemistryTeacherId(Long id);

    List<StudyClass> findStudyClassesByPhysicsTeacherId(Long id);

    List<StudyClass> findStudyClassesByRussianTeacherId(Long id);

    List<StudyClass> findStudyClassesByEnglishTeacherId(Long id);

    void deleteStudyClassById(Long id);
}
