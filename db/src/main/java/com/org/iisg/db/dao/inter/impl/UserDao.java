package com.org.iisg.db.dao.inter.impl;

import com.org.iisg.db.dao.inter.CommonDao;
import com.org.iisg.model.User;

import java.util.List;

public interface UserDao extends CommonDao<User> {

    Long create(User user, Long subjectId);

    User getUserByLogin(String login);

    List<User> getTeachers();

    List<User> getTeachers(int page);

    boolean changeUserMail(Long id, String newMail);

    List<User> getUsersByRole(Long roleId);

    List<User> getNoClassStudents(int page);

    List<User> getClassStudents(Long classId, int page);

    List<User> getClassStudents(Long classId);

    List<User> getTeachersBySubject(Long subjectId);

    boolean isLoginInUse(String login);

    Long getNoClassStudentCount();

    Long getClassStudentCount(Long classId);

    Long getTeacherCount();
}
