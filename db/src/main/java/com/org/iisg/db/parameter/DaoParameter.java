package com.org.iisg.db.parameter;

public interface DaoParameter {

    int DEFAULT_PAGE_SIZE = 7;

    int DEFAULT_TASK_PAGE_SIZE = 4;

    Long ERROR_ID = 0L;

    Long STUDENT_ID = 1L;

    Long TEACHER_ID = 2L;

    int MATHEMATICS_INT_ID = 1;

    int INFORMATICS_INT_ID = 2;

    int CHEMISTRY_INT_ID = 3;

    int PHYSICS_INT_ID = 4;

    int RUSSIAN_INT_ID = 5;

    int ENGLISH_INT_ID = 6;

    String NO_SUPPORT_MESSAGE = "This command isn't supported";

    String SUBJECT_ID_PARAM = "subjectId";

    String STUDENT_ID_PARAM = "studentId";

    String ID_PARAM = "id";

    String LOGIN_PARAM = "login";

    String CLASS_ID_PARAM = "classId";

    String ROLE_ID_PARAM = "roleId";

    String MATHEMATICS_TEACHER_ID_PARAM = "mathematicsTeacherId";

    String  INFORMATICS_TEACHER_ID_PARAM = "informaticsTeacherId";

    String  CHEMISTRY_TEACHER_ID_PARAM = "chemistryTeacherId";

    String  PHYSICS_TEACHER_ID_PARAM = "physicsTeacherId";

    String  RUSSIAN_TEACHER_ID_PARAM = "russianTeacherId";

    String  ENGLISH_TEACHER_ID_PARAM = "englishTeacherId";

    String HQL_GET_TEACHER_COUNT = "SELECT COUNT(*) FROM User u WHERE u.role.id = 2";

    String HQL_GET_CLASS_STUDENT_COUNT = "SELECT COUNT(*) FROM User u " +
            "WHERE u.role.id = 1 AND u.studyClass.id = :classId";

    String HQL_GET_TASK_STUDENT_COUNT = "SELECT COUNT(*) FROM Task t " +
            "WHERE t.studentId = :studentId AND t.subject.id = :subjectId";

    String HQL_GET_NO_CLASS_STUDENT_COUNT = "SELECT COUNT(*) FROM User u " +
            "WHERE u.role.id = 1 AND u.studyClass.id is null";

    String HQL_GET_USER_BY_ID = "SELECT u FROM User u " +
            "WHERE u.id = :id";

    String HQL_GET_USER_BY_LOGIN = "SELECT u FROM User u " +
            "WHERE u.login = :login";

    String HQL_GET_TASK_BY_STUDENT_AND_SUBJECT_ID = "SELECT t FROM Task t " +
            "WHERE t.studentId = :studentId AND t.subject.id = :subjectId";

    String HQL_GET_USERS_BY_ROLE_ID = "SELECT u FROM User u " +
            "WHERE u.role.id = :roleId";

    String HQL_GET_NO_CLASS_STUDENTS = "SELECT u FROM User u " +
            "WHERE u.role.id = 1 AND u.studyClass.id is null";

    String HQL_GET_CLASS_STUDENTS = "SELECT u FROM User u " +
            "WHERE u.role.id = 1 AND u.studyClass.id = :classId";

    String HQL_GET_TEACHERS_BY_SUBJECT_ID = "SELECT u FROM User u " +
            "WHERE u.subject.id = :subjectId";

    String HQL_GET_TEACHERS = "SELECT u FROM User u " +
            "WHERE u.role.id = 2";

    String HQL_GET_GRADES = "FROM Grade";

    String HQL_GET_STUDY_CLASSES = "FROM StudyClass";

    String HQL_GET_GRADES_BY_STUDENT_ID = "SELECT g FROM Grade g " +
            "WHERE g.studentId = :studentId";

    String HQL_GET_GRADES_BY_STUDENT_ID_AND_SUBJECT = "SELECT g " +
            "FROM Grade g " +
            "WHERE g.studentId = :studentId AND g.subject.id = :subjectId";

    String HQL_GET_CLASSES_BY_MATH_TEACHER_ID = "SELECT c " +
            "FROM StudyClass c " +
            "WHERE c.mathematicsTeacherId = :mathematicsTeacherId";

    String HQL_GET_CLASSES_BY_INFORM_TEACHER_ID = "SELECT c " +
            "FROM StudyClass c " +
            "WHERE c.informaticsTeacherId = :informaticsTeacherId";

    String HQL_GET_CLASSES_BY_CHEM_TEACHER_ID = "SELECT c " +
            "FROM StudyClass c " +
            "WHERE c.chemistryTeacherId = :chemistryTeacherId";

    String HQL_GET_CLASSES_BY_PHY_TEACHER_ID = "SELECT c " +
            "FROM StudyClass c " +
            "WHERE c.physicsTeacherId = :physicsTeacherId";

    String HQL_GET_CLASSES_BY_RU_TEACHER_ID = "SELECT c " +
            "FROM StudyClass c " +
            "WHERE c.russianTeacherId = :russianTeacherId";

    String HQL_GET_CLASSES_BY_EN_TEACHER_ID = "SELECT c " +
            "FROM StudyClass c " +
            "WHERE c.englishTeacherId = :englishTeacherId";

    String JDBC_DRIVER_NAME_PROPERTY = "jdbc.driveClassName";

    String JDBC_URL_PROPERTY = "jdbc.url";

    String JDBC_USERNAME_PROPERTY = "jdbc.username";

    String JDBC_PASSWORD_PROPERTY = "jdbc.password";

    int ACQUIRE_RETRY_ATTEMPTS = 0;

    int MAX_POOL_SIZE = 100;

    int MIN_POOL_SIZE = 0;

    String SQL_CONNECTION_PROPERTIES_FILE = "connection.properties";

    String CONNECTION_POOL_CREATE_ERROR_LOG = "Couldn't create connection pool: ";

    String CONNECTION_POOL_TAKE_CONNECTION_ERROR_LOG = "Couldn't take connection from pool:";

    String CONNECTION_POOL_RELEASE_CONNECTION_ERROR_LOG = "Couldn't release connection from pool: ";

    int FIRST_PARAM_INDEX = 1;

    int SECOND_PARAM_INDEX = 2;

    int THIRD_PARAM_INDEX = 3;

    int FOURTH_PARAM_INDEX = 4;

    int FIFTH_PARAM_INDEX = 5;

    int SIXTH_PARAM_INDEX = 6;

    int SEVENTH_PARAM_INDEX = 7;

    int EIGHTH_PARAM_INDEX = 8;

    int NINTH_PARAM_INDEX = 9;

    int TENTH_PARAM_INDEX = 10;

    int ELEVENTH_PARAM_INDEX = 11;

    int TWELFTH_PARAM_INDEX = 12;

    int THIRTEENTH_PARAM_INDEX = 13;

    int FOURTEENTH_PARAM_INDEX = 14;

    int FIFTEENTH_PARAM_INDEX = 15;

    int SIXTEENTH_PARAM_INDEX = 16;

    int SEVENTEENTH_PARAM_INDEX = 17;

    int EIGHTEENTH_PARAM_INDEX = 18;

    Long DEFAULT_ID = 0L;

    String ERROR_CLOSE_STATEMENT_LOG = "Couldn't close statement: ";

    String ERROR_CREATE_ENTITY_LOG = "Error in creating entity: ";

    String ERROR_DELETE_LOG = "Error in delete entity: ";

    String ERROR_CREATE_LOG = "Error in create entity: ";

    String ERROR_GET_LOG = "Error in get entity(ies): ";

    String ERROR_GET_ALL_LOG = "Error in get all entities: ";

    String ERROR_UPDATE_LOG = "Error in update entity: ";

    String SQL_GET_TASK_BY_ID = "SELECT t.id, t.student_id, t.description, s.id, s.name " +
            "FROM task t " +
            "JOIN subject s ON t.subject_id = s.id " +
            "WHERE t.id = ?";

    String SQL_GET_TASK_BY_STUDENT_ID_AND_SUBJECT_ID = "SELECT t.id, t.student_id, t.description, s.id, s.name " +
            "FROM task t " +
            "JOIN subject s ON t.subject_id = s.id " +
            "WHERE t.student_id = ? AND t.subject_id = ? " +
            "LIMIT ? " +
            "OFFSET ?";

    String SQL_GET_TASK_COUNT_BY_STUDENT_ID_AND_SUBJECT_ID = "SELECT COUNT(*) " +
            "FROM task t " +
            "JOIN subject s ON t.subject_id = s.id " +
            "WHERE t.student_id = ? AND t.subject_id = ?";

    String SQL_DELETE_TASK_BY_ID = "DELETE " +
            "FROM task t " +
            "WHERE t.id = ?";

    String SQL_CREATE_TASK = "INSERT INTO task " +
            "(description, student_id, subject_id) " +
            "VALUES (?, ?, ?)";

    String SQL_GET_ALL_GRADES = "SELECT g.id, g.student_id, g.mark, g.date, s.id, s.name " +
            "FROM grade g " +
            "JOIN subject s on s.id = g.subject_id";

    String SQL_GET_GRADES_BY_STUDENT_ID = "SELECT g.id, g.student_id, g.mark, g.date, s.id, s.name " +
            "FROM grade g " +
            "JOIN subject s on s.id = g.subject_id " +
            "WHERE g.student_id = ?";

    String SQL_GET_GRADES_BY_STUDENT_ID_AND_SUBJECT_ID = "SELECT g.id, g.student_id, g.mark, g.date, s.id, s.name " +
            "FROM grade g " +
            "JOIN subject s on s.id = g.subject_id " +
            "WHERE g.student_id = ? AND g.subject_id = ?";

    String SQL_DELETE_GRADE_BY_ID = "DELETE " +
            "FROM grade g " +
            "WHERE g.id = ?";

    String SQL_DELETE_GRADE_BY_STUDENT_ID = "DELETE " +
            "FROM grade g " +
            "WHERE g.student_id = ?";

    String SQL_DELETE_ALL_GRADES = "DELETE " +
            "FROM grade g ";

    String SQL_CREATE_GRADE = "INSERT INTO grade " +
            "(student_id, mark, date, subject_id) " +
            "VALUES (?, ?, ?, ?)";

    String SQL_DELETE_CLASS_BY_ID = "DELETE " +
            "FROM classes c " +
            "WHERE c.id = ?";

    String SQL_UPDATE_CLASS = "UPDATE classes c" +
            "SET c.math_id = ? AND c.inform_id = ? AND c.chem_id = ? AND c.phy_id = ? AND c.rus_id = ? AND c.en_id = ? " +
            "WHERE c.id = ?";

    String SQL_GET_ALL_CLASSES = "SELECT c.id, c.name, c.math_id, c.inform_id, c.chem_id, c.phy_id, c.rus_id, c.en_id " +
            "FROM classes c";

    String SQL_GET_CLASS_BY_ID = "SELECT c.id, c.name, c.math_id, c.inform_id, c.chem_id, c.phy_id, c.rus_id, c.en_id " +
            "FROM classes c " +
            "WHERE c.id = ?";

    String SQL_CREATE_CLASS = "INSERT INTO classes " +
            "(name, math_id, inform_id, chem_id, phy_id, rus_id, en_id) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?)";

    String SQL_GET_CLASS_BY_MATH_TEACHER_ID = "SELECT c.id, c.name, c.math_id, c.inform_id, c.chem_id, c.phy_id, " +
            "c.rus_id, c.en_id " +
            "FROM classes c " +
            "WHERE c.math_id = ?";

    String SQL_GET_CLASS_BY_INFORM_TEACHER_ID = "SELECT c.id, c.name, c.math_id, c.inform_id, c.chem_id, c.phy_id, " +
            "c.rus_id, c.en_id " +
            "FROM classes c " +
            "WHERE c.inform_id = ?";

    String SQL_GET_CLASS_BY_CHEM_TEACHER_ID = "SELECT c.id, c.name, c.math_id, c.inform_id, c.chem_id, c.phy_id, " +
            "c.rus_id, c.en_id " +
            "FROM classes c " +
            "WHERE c.chem_id = ?";

    String SQL_GET_CLASS_BY_PHY_TEACHER_ID = "SELECT c.id, c.name, c.math_id, c.inform_id, c.chem_id, c.phy_id, " +
            "c.rus_id, c.en_id " +
            "FROM classes c " +
            "WHERE c.phy_id = ?";

    String SQL_GET_CLASS_BY_RU_TEACHER_ID = "SELECT c.id, c.name, c.math_id, c.inform_id, c.chem_id, c.phy_id, " +
            "c.rus_id, c.en_id " +
            "FROM classes c " +
            "WHERE c.rus_id = ?";

    String SQL_GET_CLASS_BY_EN_TEACHER_ID = "SELECT c.id, c.name, c.math_id, c.inform_id, c.chem_id, c.phy_id, " +
            "c.rus_id, c.en_id " +
            "FROM classes c " +
            "WHERE c.en_id = ?";

    String SQL_GET_USER_BY_ID = "SELECT u.id, u.login, u.password, u.first_name, u.last_name, u.mail, " +
            "r.id, r.name, " +
            "s.id, s.name, " +
            "c.id, c.name, c.chem_id, c.en_id, c.inform_id, c.math_id, c.name, c.phy_id, c.rus_id " +
            "FROM user u " +
            "JOIN role r on r.id = u.role_id " +
            "LEFT JOIN subject s on s.id = u.subject_id " +
            "LEFT JOIN classes c on c.id = u.class_id " +
            "WHERE u.id = ?";

    String SQL_DELETE_USER_BY_ID = "DELETE " +
            "FROM user u " +
            "WHERE u.id = ?";

    String SQL_UPDATE_USER = "UPDATE user u " +
            "SET u.login = ? AND u.password = ? AND u.first_name = ? AND u.last_name = ? AND u.mail = ? " +
            "AND u.role_id = ? AND u.subject_id = ? AND u.class_id = ? " +
            "WHERE u.id = ?";

    String SQL_CREATE_USER = "INSERT INTO user " +
            "(login, password, first_name, last_name, mail, role_id, subject_id, class_id) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    String SQL_GET_CLASS_STUDENT_COUNT = "SELECT COUNT(*) " +
            "FROM user u " +
            "WHERE u.class_id = ?";

    String SQL_GET_NO_CLASS_STUDENT_COUNT = "SELECT COUNT(*) " +
            "FROM user u " +
            "WHERE u.class_id is null";

    String SQL_GET_TEACHER_COUNT = "SELECT COUNT(*) " +
            "FROM user u " +
            "WHERE u.role_id = 2";

    String SQL_GET_ALL_TEACHERS = "SELECT u.id, u.login, u.password, u.first_name, u.last_name, u.mail, " +
            "r.id, r.name, " +
            "s.id, s.name, " +
            "c.id, c.name, c.chem_id, c.en_id, c.inform_id, c.math_id, c.name, c.phy_id, c.rus_id " +
            "FROM user u " +
            "JOIN role r on r.id = u.role_id " +
            "LEFT JOIN subject s on s.id = u.subject_id " +
            "LEFT JOIN classes c on c.id = u.class_id " +
            "WHERE u.role_id = 2";

    String SQL_GET_TEACHERS_BY_SUBJECT_ID = "SELECT u.id, u.login, u.password, u.first_name, u.last_name, u.mail, " +
            "r.id, r.name, " +
            "s.id, s.name, " +
            "c.id, c.name, c.chem_id, c.en_id, c.inform_id, c.math_id, c.name, c.phy_id, c.rus_id " +
            "FROM user u " +
            "JOIN role r on r.id = u.role_id " +
            "LEFT JOIN subject s on s.id = u.subject_id " +
            "LEFT JOIN classes c on c.id = u.class_id " +
            "WHERE u.role_id = 2 AND u.subject_id = ?";

    String SQL_GET_TEACHERS_BY_PAGE = "SELECT u.id, u.login, u.password, u.first_name, u.last_name, u.mail, " +
            "r.id, r.name, " +
            "s.id, s.name, " +
            "c.id, c.name, c.chem_id, c.en_id, c.inform_id, c.math_id, c.name, c.phy_id, c.rus_id " +
            "FROM user u " +
            "JOIN role r on r.id = u.role_id " +
            "LEFT JOIN subject s on s.id = u.subject_id " +
            "LEFT JOIN classes c on c.id = u.class_id " +
            "WHERE u.role_id = 2 " +
            "LIMIT ? " +
            "OFFSET ?";

    String SQL_GET_USER_BY_LOGIN = "SELECT u.id, u.login, u.password, u.first_name, u.last_name, u.mail, " +
            "r.id, r.name, " +
            "s.id, s.name, " +
            "c.id, c.name, c.chem_id, c.en_id, c.inform_id, c.math_id, c.name, c.phy_id, c.rus_id " +
            "FROM user u " +
            "JOIN role r on r.id = u.role_id " +
            "LEFT JOIN subject s on s.id = u.subject_id " +
            "LEFT JOIN classes c on c.id = u.class_id " +
            "WHERE u.login = ?";

    String SQL_GET_USERS_BY_ROLE = "SELECT u.id, u.login, u.password, u.first_name, u.last_name, u.mail, " +
            "r.id, r.name, " +
            "s.id, s.name, " +
            "c.id, c.name, c.chem_id, c.en_id, c.inform_id, c.math_id, c.name, c.phy_id, c.rus_id " +
            "FROM user u " +
            "JOIN role r on r.id = u.role_id " +
            "LEFT JOIN subject s on s.id = u.subject_id " +
            "LEFT JOIN classes c on c.id = u.class_id " +
            "WHERE u.role_id = ?";

    String SQL_UPDATE_USER_MAIL = "UPDATE user u " +
            "SET u.mail = ? " +
            "WHERE u.id = ?";

    String SQL_GET_NO_CLASS_STUDENTS_BY_PAGE = "SELECT u.id, u.login, u.password, u.first_name, u.last_name, u.mail, " +
            "r.id, r.name, " +
            "s.id, s.name, " +
            "c.id, c.name, c.chem_id, c.en_id, c.inform_id, c.math_id, c.name, c.phy_id, c.rus_id " +
            "FROM user u " +
            "JOIN role r on r.id = u.role_id " +
            "LEFT JOIN subject s on s.id = u.subject_id " +
            "LEFT JOIN classes c on c.id = u.class_id " +
            "WHERE u.role_id = 1 AND u.class_id is null " +
            "LIMIT ? " +
            "OFFSET ?";

    String SQL_GET_CLASS_STUDENTS_BY_PAGE = "SELECT u.id, u.login, u.password, u.first_name, u.last_name, u.mail, " +
            "r.id, r.name, " +
            "s.id, s.name, " +
            "c.id, c.name, c.chem_id, c.en_id, c.inform_id, c.math_id, c.name, c.phy_id, c.rus_id " +
            "FROM user u " +
            "JOIN role r on r.id = u.role_id " +
            "LEFT JOIN subject s on s.id = u.subject_id " +
            "LEFT JOIN classes c on c.id = u.class_id " +
            "WHERE u.role_id = 1 AND u.class_id ? " +
            "LIMIT ? " +
            "OFFSET ?";

    String SQL_GET_CLASS_STUDENTS = "SELECT u.id, u.login, u.password, u.first_name, u.last_name, u.mail, " +
            "r.id, r.name, " +
            "s.id, s.name, " +
            "c.id, c.name, c.chem_id, c.en_id, c.inform_id, c.math_id, c.name, c.phy_id, c.rus_id " +
            "FROM user u " +
            "JOIN role r on r.id = u.role_id " +
            "LEFT JOIN subject s on s.id = u.subject_id " +
            "LEFT JOIN classes c on c.id = u.class_id " +
            "WHERE u.role_id = 1 AND u.class_id ?";

    String GET_GEN_ID = "SELECT LAST_INSERT_ID()";
}
