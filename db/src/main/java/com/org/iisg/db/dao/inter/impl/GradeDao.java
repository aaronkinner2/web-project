package com.org.iisg.db.dao.inter.impl;

import com.org.iisg.db.dao.inter.CommonDao;
import com.org.iisg.model.Grade;

import java.util.List;

public interface GradeDao extends CommonDao<Grade> {

    Long create(Grade grade, Long subjectId);

    List<Grade> getGradesByStudentId(Long studentId);

    List<Grade> getGradesByStudentIdAndSubjectId(Long studentId, Long subjectId);

    void deleteGradesByStudentId(Long studentId);

    void deleteGrades();
}
