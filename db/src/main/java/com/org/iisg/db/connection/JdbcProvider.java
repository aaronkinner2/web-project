package com.org.iisg.db.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import static com.org.iisg.db.parameter.DaoParameter.*;

public class JdbcProvider {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    public static Connection getConnection() throws SQLException, IOException, ClassNotFoundException {
        LOGGER.info("init connection");
        final Properties properties = new Properties();
        final InputStream in = JdbcProvider.class.getClassLoader().getResourceAsStream(SQL_CONNECTION_PROPERTIES_FILE);
        properties.load(in);
        String jdbcClassName = properties.getProperty(JDBC_DRIVER_NAME_PROPERTY);
        String jdbcUrl = properties.getProperty(JDBC_URL_PROPERTY);
        String jdbcUsername = properties.getProperty(JDBC_USERNAME_PROPERTY);
        String jdbcPassword = properties.getProperty(JDBC_PASSWORD_PROPERTY);
        Class.forName(jdbcClassName);
        return DriverManager.getConnection(jdbcUrl, jdbcUsername, jdbcPassword);
    }
}