package com.org.iisg.db.dao.impl.jdbc;

import com.org.iisg.db.connection.JdbcConnectionPool;
import com.org.iisg.db.dao.inter.impl.GradeDao;
import com.org.iisg.model.Grade;
import com.org.iisg.model.Subject;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.org.iisg.db.parameter.DaoParameter.*;

public class JdbcGradeDaoImpl implements GradeDao {

    private static final Logger LOGGER = LogManager.getLogger(JdbcGradeDaoImpl.class);

    private static GradeDao gradeDao;

    private final JdbcConnectionPool connectionPool;

    private JdbcGradeDaoImpl() {
        connectionPool = JdbcConnectionPool.getInstance();
    }

    public static GradeDao getInstance() {
        if (gradeDao == null) {
            synchronized (JdbcGradeDaoImpl.class) {
                if (gradeDao == null) {
                    gradeDao = new JdbcGradeDaoImpl();
                }
            }
        }
        return gradeDao;
    }

    @Override
    public Long create(Grade grade, Long subjectId) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        long id = DEFAULT_ID;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CREATE_GRADE);
            preparedStatement.setLong(FIRST_PARAM_INDEX, grade.getStudentId());
            preparedStatement.setInt(SECOND_PARAM_INDEX, grade.getMark());
            preparedStatement.setDate(THIRD_PARAM_INDEX, Date.valueOf(grade.getDate()));
            preparedStatement.setLong(FOURTH_PARAM_INDEX, subjectId);
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.executeQuery(GET_GEN_ID);
            if (rs.next()) id = rs.getLong(FIRST_PARAM_INDEX);
            return id;
        } catch (SQLException e) {
            LOGGER.error(ERROR_CREATE_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Grade> getGradesByStudentId(Long studentId) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        List<Grade> grades = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_GRADES_BY_STUDENT_ID);
            preparedStatement.setLong(FIRST_PARAM_INDEX, studentId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) grades.add(createGradeFromQueryResult(resultSet));
            return grades;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Grade> getGradesByStudentIdAndSubjectId(Long studentId, Long subjectId) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        List<Grade> grades = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_GRADES_BY_STUDENT_ID_AND_SUBJECT_ID);
            preparedStatement.setLong(FIRST_PARAM_INDEX, studentId);
            preparedStatement.setLong(SECOND_PARAM_INDEX, subjectId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) grades.add(createGradeFromQueryResult(resultSet));
            return grades;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public void deleteGradesByStudentId(Long studentId) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_DELETE_GRADE_BY_STUDENT_ID);
            preparedStatement.setLong(FIRST_PARAM_INDEX, studentId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(ERROR_DELETE_LOG + e);
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public void deleteGrades() {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_DELETE_ALL_GRADES);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(ERROR_DELETE_LOG + e);
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public Grade get(Long id) {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public List<Grade> getAll() {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        List<Grade> grades = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_ALL_GRADES);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) grades.add(createGradeFromQueryResult(resultSet));
            return grades;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_ALL_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public Long create(Grade grade) {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public boolean update(Grade grade) {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public void delete(Long id) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_DELETE_GRADE_BY_ID);
            preparedStatement.setLong(FIRST_PARAM_INDEX, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(ERROR_DELETE_LOG + e);
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    private Grade createGradeFromQueryResult(ResultSet resultSet) {
        try {
            return new Grade(resultSet.getLong(FIRST_PARAM_INDEX),
                    resultSet.getLong(SECOND_PARAM_INDEX),
                    resultSet.getInt(THIRD_PARAM_INDEX),
                    resultSet.getDate(FOURTH_PARAM_INDEX).toLocalDate(),
                    new Subject(resultSet.getLong(FOURTH_PARAM_INDEX), resultSet.getString(SIXTH_PARAM_INDEX)));
        } catch (SQLException e) {
            LOGGER.error(ERROR_CREATE_ENTITY_LOG + e);
            return null;
        }
    }

    private void close(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, ERROR_CLOSE_STATEMENT_LOG + e.getMessage());
        }
    }
}
