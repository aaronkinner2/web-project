package com.org.iisg.db.dao.inter;

import java.util.List;

public interface CommonDao<T> {

    T get(Long id);

    List<T> getAll();

    Long create(T t);

    boolean update(T t);

    void delete(Long id);
}
