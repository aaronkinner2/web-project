package com.org.iisg.db.dao.inter.impl;

import com.org.iisg.db.dao.inter.CommonDao;
import com.org.iisg.model.StudyClass;

import java.util.List;

public interface StudyClassDao extends CommonDao<StudyClass> {

    List<StudyClass> getByTeacherIdAndSubjectId(Long teacherId, Long subjectId);
}
