package com.org.iisg.db.dao.inter.impl;

import com.org.iisg.db.dao.inter.CommonDao;
import com.org.iisg.model.Task;

import java.util.List;

public interface TaskDao extends CommonDao<Task> {

    Long create(Task task, Long subjectId);

    List<Task> getByStudentId(Long studentId, Long subjectId, int page);

    Long getTaskCount(Long studentId, Long subjectId);
}