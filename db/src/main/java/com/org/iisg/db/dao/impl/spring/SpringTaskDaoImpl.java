package com.org.iisg.db.dao.impl.spring;

import com.org.iisg.db.dao.inter.impl.TaskDao;
import com.org.iisg.db.repository.TaskRepository;
import com.org.iisg.model.Subject;
import com.org.iisg.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.org.iisg.db.parameter.DaoParameter.*;

@Service
public class SpringTaskDaoImpl implements TaskDao {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public Long create(Task task, Long subjectId) {
        task.setSubject(new Subject(subjectId));
        taskRepository.save(task);
        return null;
    }

    @Override
    public List<Task> getByStudentId(Long studentId, Long subjectId, int page) {
        List<Task> taskList = taskRepository.getTasksByStudentIdAndSubject_Id(studentId, subjectId);
        if (taskList.isEmpty() || taskList.size() == 1) {
            return taskList;
        }
        if (taskList.size() < page * DEFAULT_TASK_PAGE_SIZE) {
            return taskList.subList((page - 1) * DEFAULT_TASK_PAGE_SIZE, taskList.size());
        }
        return taskList.subList((page - 1) * DEFAULT_TASK_PAGE_SIZE, page * DEFAULT_TASK_PAGE_SIZE);
    }

    @Override
    public Long getTaskCount(Long studentId, Long subjectId) {
        return (long) taskRepository.getTasksByStudentIdAndSubject_Id(studentId, subjectId).size();
    }

    @Override
    public Task get(Long id) {
        return taskRepository.findTaskById(id);
    }

    @Override
    public List<Task> getAll() {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public Long create(Task task) {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public boolean update(Task task) {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public void delete(Long id) {
        taskRepository.deleteTaskById(id);
    }
}
