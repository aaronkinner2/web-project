package com.org.iisg.db.dao.impl.hibernate;

import com.org.iisg.db.connection.HibernateUtil;
import com.org.iisg.db.dao.inter.impl.TaskDao;
import com.org.iisg.model.Subject;
import com.org.iisg.model.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.org.iisg.db.parameter.DaoParameter.*;

@Repository
public class HibernateTaskDaoImpl implements TaskDao {

    private final SessionFactory sessionFactory;

    private HibernateTaskDaoImpl() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    public static TaskDao getTaskDao() {
        return new HibernateTaskDaoImpl();
    }

    @Override
    public Task get(Long id) {
        try (final Session session = sessionFactory.openSession()) {
            return session.get(Task.class, id);
        }
    }

    @Override
    public List<Task> getAll() {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public Long create(Task task) { throw new IllegalStateException(NO_SUPPORT_MESSAGE); }

    @Override
    public Long create(Task task, Long subjectId) {
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            Subject subject = session.get(Subject.class, subjectId);
            subject.addTask(task);
            task.setSubject(subject);
            session.update(subject);
            Long id = (Long) session.save(task);
            session.getTransaction().commit();
            return id;
        }
    }

    @Override
    public boolean update(Task task) {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public void delete(Long id) {
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            Task task = session.get(Task.class, id);
            Subject subject = session.get(Subject.class, task.getSubject().getId());
            subject.removeTask(task);
            session.update(subject);
            session.delete(task);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Task> getByStudentId(Long studentId, Long subjectId, int page) {
        try (Session session = sessionFactory.openSession()) {
            final Query<Task> query = session
                    .createQuery(HQL_GET_TASK_BY_STUDENT_AND_SUBJECT_ID, Task.class)
                    .setParameter(STUDENT_ID_PARAM, studentId)
                    .setParameter(SUBJECT_ID_PARAM, subjectId)
                    .setFirstResult((page - 1) * DEFAULT_TASK_PAGE_SIZE)
                    .setMaxResults(DEFAULT_TASK_PAGE_SIZE);
            return query.getResultList();
        }
    }

    @Override
    public Long getTaskCount(Long studentId, Long subjectId) {
        try (Session session = sessionFactory.openSession()) {
            return (Long) session.createQuery(HQL_GET_TASK_STUDENT_COUNT)
                    .setParameter(STUDENT_ID_PARAM, studentId)
                    .setParameter(SUBJECT_ID_PARAM, subjectId)
                    .uniqueResult();
        }
    }
}
