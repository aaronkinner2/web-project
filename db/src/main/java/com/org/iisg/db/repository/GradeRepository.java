package com.org.iisg.db.repository;

import com.org.iisg.model.Grade;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GradeRepository extends CrudRepository<Grade, Long> {

    List<Grade> findGradesByStudentIdNotNull();

    List<Grade> findGradesByStudentId(Long id);

    List<Grade> findGradesByStudentIdAndSubject_Id(Long studentId, Long subjectId);

    void deleteGradeById(Long id);

    void deleteGradeByStudentId(Long id);
}
