package com.org.iisg.db.dao.impl.hibernate;

import com.org.iisg.db.connection.HibernateUtil;
import com.org.iisg.db.dao.inter.impl.GradeDao;
import com.org.iisg.model.Grade;
import com.org.iisg.model.Subject;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.org.iisg.db.parameter.DaoParameter.*;

@Repository
public class HibernateGradeDaoImpl implements GradeDao {

    private final SessionFactory sessionFactory;

    private HibernateGradeDaoImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    public static GradeDao getGradeDao() {
        return new HibernateGradeDaoImpl();
    }

    @Override
    public Grade get(Long id) {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public List<Grade> getAll() {
        try (final Session session = sessionFactory.openSession()) {
            final Query<Grade> query = session.createQuery(HQL_GET_GRADES, Grade.class);
            return query.getResultList();
        }
    }

    @Override
    public Long create(Grade grade) {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public Long create(Grade grade, Long subjectId) {
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            Subject subject = session.get(Subject.class, subjectId);
            grade.setSubject(subject);
            Long id = (Long) session.save(grade);
            session.getTransaction().commit();
            return id;
        }
    }

    @Override
    public boolean update(Grade grade) {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public void delete(Long id) {
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            Grade grade = session.get(Grade.class, id);
            session.delete(grade);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Grade> getGradesByStudentId(Long studentId) {
        try (final Session session = sessionFactory.openSession()) {
            final Query<Grade> query = session
                    .createQuery(HQL_GET_GRADES_BY_STUDENT_ID, Grade.class)
                    .setParameter(STUDENT_ID_PARAM, studentId);
            return query.getResultList();
        }
    }

    @Override
    public List<Grade> getGradesByStudentIdAndSubjectId(Long studentId, Long subjectId) {
        try (final Session session = sessionFactory.openSession()) {
            final Query<Grade> query = session
                    .createQuery(HQL_GET_GRADES_BY_STUDENT_ID_AND_SUBJECT, Grade.class)
                    .setParameter(STUDENT_ID_PARAM, studentId)
                    .setParameter(SUBJECT_ID_PARAM, subjectId);
            return query.getResultList();
        }
    }

    @Override
    public void deleteGradesByStudentId(Long studentId) {
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            List<Grade> grades = getGradesByStudentId(studentId);
            for (Grade g : grades) {
                session.delete(g);
            }
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteGrades() {
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            final Query<Grade> query = session.createQuery(HQL_GET_GRADES, Grade.class);
            List<Grade> grades = query.getResultList();
            for (Grade grade : grades) {
                session.delete(grade);
            }
            session.getTransaction().commit();
        }
    }
}
