package com.org.iisg.db.dao.impl.spring;

import com.org.iisg.db.dao.inter.impl.UserDao;
import com.org.iisg.db.repository.UserRepository;
import com.org.iisg.model.Role;
import com.org.iisg.model.Subject;
import com.org.iisg.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.org.iisg.db.parameter.DaoParameter.*;

@Service
public class SpringUserDaoImpl implements UserDao {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Long create(User user, Long subjectId) {
        user.setRole(new Role(TEACHER_ID));
        user.setSubject(new Subject(subjectId));
        userRepository.save(user);
        return userRepository.findUserByLoginEquals(user.getLogin()).getId();
    }

    @Override
    public User getUserByLogin(String login) {
        return userRepository.findUserByLoginEquals(login);
    }

    @Override
    public List<User> getTeachers() {
        return userRepository.getUsersByRole_Id(TEACHER_ID);
    }

    @Override
    public List<User> getTeachers(int page) {
        List<User> userList = userRepository.getUsersByRole_Id(TEACHER_ID);
        if (userList.isEmpty() || userList.size() == 1) {
            return userList;
        }
        if (userList.size() < page * DEFAULT_PAGE_SIZE) {
            return userList.subList((page - 1) * DEFAULT_PAGE_SIZE, userList.size());
        }
        return userList.subList((page - 1) * DEFAULT_PAGE_SIZE, page * DEFAULT_PAGE_SIZE);
    }

    @Override
    public boolean changeUserMail(Long id, String newMail) {
        User user = userRepository.findUserById(id);
        if (user == null) {
            return false;
        } else {
            user.getUserInfo().setMail(newMail);
            return true;
        }
    }

    @Override
    public List<User> getUsersByRole(Long roleId) {
        return userRepository.getUsersByRole_Id(roleId);
    }

    @Override
    public List<User> getNoClassStudents(int page) {
        List<User> userList = userRepository.getUsersByStudyClass_IdIsNullAndRole_Id(STUDENT_ID);
        if (userList.isEmpty() || userList.size() == 1) {
            return userList;
        }
        if (userList.size() < page * DEFAULT_PAGE_SIZE) {
            return userList.subList((page - 1) * DEFAULT_PAGE_SIZE, userList.size());
        }
        return userList.subList((page - 1) * DEFAULT_PAGE_SIZE, page * DEFAULT_PAGE_SIZE);
    }

    @Override
    public List<User> getClassStudents(Long classId, int page) {
        List<User> userList = userRepository.getUsersByStudyClass_IdAndRole_Id(classId, STUDENT_ID);
        if (userList.isEmpty()) {
            return userList;
        }
        if (userList.size() < page * DEFAULT_PAGE_SIZE) {
            return userList.subList((page - 1) * DEFAULT_PAGE_SIZE, userList.size());
        }
        return userList.subList((page - 1) * DEFAULT_PAGE_SIZE, page * DEFAULT_PAGE_SIZE);
    }

    @Override
    public List<User> getClassStudents(Long classId) {
        return userRepository.getUsersByStudyClass_IdAndRole_Id(classId, STUDENT_ID);
    }

    @Override
    public List<User> getTeachersBySubject(Long subjectId) {
        return userRepository.getUsersByRole_IdAndSubject_Id(TEACHER_ID, subjectId);
    }

    @Override
    public boolean isLoginInUse(String login) {
        User user = userRepository.findUserByLoginEquals(login);
        return user != null;
    }

    @Override
    public Long getNoClassStudentCount() {
        return (long) userRepository.getUsersByStudyClass_IdIsNullAndRole_Id(STUDENT_ID).size();
    }

    @Override
    public Long getClassStudentCount(Long classId) {
        return (long) userRepository.getUsersByStudyClass_IdAndRole_Id(classId, STUDENT_ID).size();
    }

    @Override
    public Long getTeacherCount() {
        return (long) userRepository.getUsersByRole_Id(TEACHER_ID).size();
    }

    @Override
    public User get(Long id) {
        return userRepository.findUserById(id);
    }

    @Override
    public List<User> getAll() {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public Long create(User user) {
        user.setRole(new Role(STUDENT_ID));
        userRepository.save(user);
        User newUser = userRepository.findUserByLoginEquals(user.getLogin());
        return newUser.getId();
    }

    @Override
    public boolean update(User user) {
        userRepository.save(user);
        return true;
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteUserById(id);
    }
}
