package com.org.iisg.db.dao.impl.hibernate;

import com.org.iisg.db.connection.HibernateUtil;
import com.org.iisg.db.dao.inter.impl.StudyClassDao;
import com.org.iisg.model.StudyClass;
import com.org.iisg.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.OptimisticLockException;
import java.util.List;

import static com.org.iisg.db.parameter.DaoParameter.*;

@Repository
public class HibernateStudyClassDaoImpl implements StudyClassDao {

    private final SessionFactory sessionFactory;

    private HibernateStudyClassDaoImpl() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    public static StudyClassDao getStudyClassDao() {
        return new HibernateStudyClassDaoImpl();
    }

    @Override
    public StudyClass get(Long id) {
        try (final Session session = sessionFactory.openSession()) {
            return session.get(StudyClass.class, id);
        }
    }

    @Override
    public List<StudyClass> getAll() {
        try (final Session session = sessionFactory.openSession()) {
            final Query<StudyClass> query = session.createQuery(HQL_GET_STUDY_CLASSES, StudyClass.class);
            return query.getResultList();
        }
    }

    @Override
    public Long create(StudyClass studyClass) {
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            Long id = (Long) session.save(studyClass);
            session.getTransaction().commit();
            return id;
        } catch (ConstraintViolationException e) {
            return ERROR_ID;
        }
    }

    @Override
    public boolean update(StudyClass studyClass) {
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            session.update(studyClass);
            session.getTransaction().commit();
            return true;
        } catch (OptimisticLockException e) {
            return false;
        }
    }

    @Override
    public void delete(Long id) {
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            List<User> userList = session
                    .createQuery(HQL_GET_CLASS_STUDENTS, User.class)
                    .setParameter(CLASS_ID_PARAM, id).getResultList();
            for (User user : userList) {
                user.setStudyClass(null);
                session.update(user);
                session.flush();
            }
            StudyClass studyClass = session.get(StudyClass.class, id);
            if (studyClass != null) {
                session.remove(studyClass);
            }
            session.getTransaction().commit();
        }
    }

    @Override
    public List<StudyClass> getByTeacherIdAndSubjectId(Long teacherId, Long subjectId) {
        try (final Session session = sessionFactory.openSession()) {
            Query<StudyClass> query = null;
            switch (subjectId.intValue()) {
                case MATHEMATICS_INT_ID: {
                    query = session
                            .createQuery(HQL_GET_CLASSES_BY_MATH_TEACHER_ID, StudyClass.class)
                            .setParameter(MATHEMATICS_TEACHER_ID_PARAM, teacherId);
                    break;
                }
                case INFORMATICS_INT_ID: {
                    query = session
                            .createQuery(HQL_GET_CLASSES_BY_INFORM_TEACHER_ID, StudyClass.class)
                            .setParameter(INFORMATICS_TEACHER_ID_PARAM, teacherId);
                    break;
                }
                case CHEMISTRY_INT_ID: {
                    query = session
                            .createQuery(HQL_GET_CLASSES_BY_CHEM_TEACHER_ID, StudyClass.class)
                            .setParameter(CHEMISTRY_TEACHER_ID_PARAM, teacherId);
                    break;
                }
                case PHYSICS_INT_ID: {
                    query = session
                            .createQuery(HQL_GET_CLASSES_BY_PHY_TEACHER_ID, StudyClass.class)
                            .setParameter(PHYSICS_TEACHER_ID_PARAM, teacherId);
                    break;
                }
                case RUSSIAN_INT_ID: {
                    query = session
                            .createQuery(HQL_GET_CLASSES_BY_RU_TEACHER_ID, StudyClass.class)
                            .setParameter(RUSSIAN_TEACHER_ID_PARAM, teacherId);
                    break;
                }
                case ENGLISH_INT_ID: {
                    query = session
                            .createQuery(HQL_GET_CLASSES_BY_EN_TEACHER_ID, StudyClass.class)
                            .setParameter(ENGLISH_TEACHER_ID_PARAM, teacherId);
                    break;
                }
            }
            if (query == null) return null;
            return query.getResultList();
        }
    }
}
