package com.org.iisg.db.dao.impl.jdbc;

import com.org.iisg.db.connection.JdbcConnectionPool;
import com.org.iisg.db.dao.inter.impl.StudyClassDao;
import com.org.iisg.model.StudyClass;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.org.iisg.db.parameter.DaoParameter.*;

public class JdbcStudyClassDaoImpl implements StudyClassDao {

    private static final Logger LOGGER = LogManager.getLogger(JdbcStudyClassDaoImpl.class);

    private static StudyClassDao studyClassDao;

    private final JdbcConnectionPool connectionPool;

    private JdbcStudyClassDaoImpl() {
        connectionPool = JdbcConnectionPool.getInstance();
    }

    public static StudyClassDao getInstance() {
        if (studyClassDao == null) {
            synchronized (JdbcStudyClassDaoImpl.class) {
                if (studyClassDao == null) {
                    studyClassDao = new JdbcStudyClassDaoImpl();
                }
            }
        }
        return studyClassDao;
    }

    @Override
    public List<StudyClass> getByTeacherIdAndSubjectId(Long teacherId, Long subjectId) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        List<StudyClass> studyClasses = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            switch (subjectId.intValue()) {
                case INFORMATICS_INT_ID: {
                    preparedStatement = connection.prepareStatement(SQL_GET_CLASS_BY_INFORM_TEACHER_ID);
                    break;
                }
                case CHEMISTRY_INT_ID: {
                    preparedStatement = connection.prepareStatement(SQL_GET_CLASS_BY_CHEM_TEACHER_ID);
                    break;
                }
                case PHYSICS_INT_ID: {
                    preparedStatement = connection.prepareStatement(SQL_GET_CLASS_BY_PHY_TEACHER_ID);
                    break;
                }
                case RUSSIAN_INT_ID: {
                    preparedStatement = connection.prepareStatement(SQL_GET_CLASS_BY_RU_TEACHER_ID);
                    break;
                }
                case ENGLISH_INT_ID: {
                    preparedStatement = connection.prepareStatement(SQL_GET_CLASS_BY_EN_TEACHER_ID);
                    break;
                }
                default: {
                    preparedStatement = connection.prepareStatement(SQL_GET_CLASS_BY_MATH_TEACHER_ID);
                    break;
                }
            }
            preparedStatement.setLong(FIRST_PARAM_INDEX, teacherId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) studyClasses.add(createStudyClassFromQueryResult(resultSet));
            return studyClasses;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_ALL_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public StudyClass get(Long id) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_CLASS_BY_ID);
            preparedStatement.setLong(FIRST_PARAM_INDEX, id);
            resultSet = preparedStatement.executeQuery();
            return resultSet.next() ? createStudyClassFromQueryResult(resultSet) : null;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<StudyClass> getAll() {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        List<StudyClass> studyClasses = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_ALL_CLASSES);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) studyClasses.add(createStudyClassFromQueryResult(resultSet));
            return studyClasses;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_ALL_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public Long create(StudyClass studyClass) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        long id = DEFAULT_ID;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CREATE_CLASS);
            preparedStatement.setString(FIRST_PARAM_INDEX, studyClass.getName());
            preparedStatement.setLong(SECOND_PARAM_INDEX, studyClass.getMathematicsTeacherId());
            preparedStatement.setLong(THIRD_PARAM_INDEX, studyClass.getInformaticsTeacherId());
            preparedStatement.setLong(FOURTH_PARAM_INDEX, studyClass.getChemistryTeacherId());
            preparedStatement.setLong(FIFTH_PARAM_INDEX, studyClass.getPhysicsTeacherId());
            preparedStatement.setLong(SIXTH_PARAM_INDEX, studyClass.getRussianTeacherId());
            preparedStatement.setLong(SEVENTH_PARAM_INDEX, studyClass.getEnglishTeacherId());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.executeQuery(GET_GEN_ID);
            if (rs.next()) id = rs.getLong(FIRST_PARAM_INDEX);
            return id;
        } catch (SQLException e) {
            LOGGER.error(ERROR_CREATE_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean update(StudyClass studyClass) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_UPDATE_CLASS);
            preparedStatement.setLong(FIRST_PARAM_INDEX, studyClass.getMathematicsTeacherId());
            preparedStatement.setLong(SECOND_PARAM_INDEX, studyClass.getInformaticsTeacherId());
            preparedStatement.setLong(THIRD_PARAM_INDEX, studyClass.getChemistryTeacherId());
            preparedStatement.setLong(FOURTH_PARAM_INDEX, studyClass.getPhysicsTeacherId());
            preparedStatement.setLong(FIFTH_PARAM_INDEX, studyClass.getRussianTeacherId());
            preparedStatement.setLong(SIXTH_PARAM_INDEX, studyClass.getEnglishTeacherId());
            preparedStatement.setLong(SEVENTH_PARAM_INDEX, studyClass.getId());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            LOGGER.error(ERROR_DELETE_LOG + e);
            return false;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public void delete(Long id) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_DELETE_CLASS_BY_ID);
            preparedStatement.setLong(FIRST_PARAM_INDEX, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(ERROR_DELETE_LOG + e);
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    private StudyClass createStudyClassFromQueryResult(ResultSet resultSet) {
        try {
            return new StudyClass(resultSet.getLong(FIRST_PARAM_INDEX),
                    resultSet.getString(SECOND_PARAM_INDEX),
                    resultSet.getLong(THIRD_PARAM_INDEX),
                    resultSet.getLong(FOURTH_PARAM_INDEX),
                    resultSet.getLong(FIFTH_PARAM_INDEX),
                    resultSet.getLong(SIXTH_PARAM_INDEX),
                    resultSet.getLong(SEVENTH_PARAM_INDEX),
                    resultSet.getLong(EIGHTH_PARAM_INDEX));
        } catch (SQLException e) {
            LOGGER.error(ERROR_CREATE_ENTITY_LOG + e);
            return null;
        }
    }

    private void close(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, ERROR_CLOSE_STATEMENT_LOG + e.getMessage());
        }
    }
}
