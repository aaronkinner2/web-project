package com.org.iisg.db.dao.impl.spring;

import com.org.iisg.db.dao.inter.impl.GradeDao;
import com.org.iisg.db.repository.GradeRepository;
import com.org.iisg.model.Grade;
import com.org.iisg.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.org.iisg.db.parameter.DaoParameter.NO_SUPPORT_MESSAGE;

@Service
public class SpringGradeDaoImpl implements GradeDao {

    @Autowired
    private GradeRepository gradeRepository;

    @Override
    public Long create(Grade grade, Long subjectId) {
        grade.setSubject(new Subject(subjectId));
        gradeRepository.save(grade);
        return null;
    }

    @Override
    public List<Grade> getGradesByStudentId(Long studentId) {
        return gradeRepository.findGradesByStudentId(studentId);
    }

    @Override
    public List<Grade> getGradesByStudentIdAndSubjectId(Long studentId, Long subjectId) {
        return gradeRepository.findGradesByStudentIdAndSubject_Id(studentId, subjectId);
    }

    @Override
    public void deleteGradesByStudentId(Long studentId) {
        gradeRepository.deleteGradeByStudentId(studentId);
    }

    @Override
    public void deleteGrades() {
        gradeRepository.deleteAll();
    }

    @Override
    public Grade get(Long id) {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public List<Grade> getAll() {
        return gradeRepository.findGradesByStudentIdNotNull();
    }

    @Override
    public Long create(Grade grade) {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public boolean update(Grade grade) {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public void delete(Long id) {
        gradeRepository.deleteGradeById(id);
    }
}
