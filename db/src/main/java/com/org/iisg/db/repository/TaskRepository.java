package com.org.iisg.db.repository;

import com.org.iisg.model.Task;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {

    Task findTaskById(Long id);

    List<Task> getTasksByStudentIdAndSubject_Id(Long studentId, Long subjectId);

    void deleteTaskById(Long id);
}
