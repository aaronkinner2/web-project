package com.org.iisg.db.dao.impl.jdbc;

import com.org.iisg.db.connection.JdbcConnectionPool;
import com.org.iisg.db.dao.inter.impl.UserDao;
import com.org.iisg.model.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.org.iisg.db.parameter.DaoParameter.*;

public class JdbcUserDaoImpl implements UserDao {

    private static final Logger LOGGER = LogManager.getLogger(JdbcUserDaoImpl.class);

    private static UserDao userDao;

    private final JdbcConnectionPool connectionPool;

    private JdbcUserDaoImpl() {
        connectionPool = JdbcConnectionPool.getInstance();
    }

    public static UserDao getInstance() {
        if (userDao == null) {
            synchronized (JdbcUserDaoImpl.class) {
                if (userDao == null) {
                    userDao = new JdbcUserDaoImpl();
                }
            }
        }
        return userDao;
    }

    @Override
    public Long create(User user, Long subjectId) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        long id = DEFAULT_ID;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CREATE_USER);
            preparedStatement.setString(FIRST_PARAM_INDEX, user.getLogin());
            preparedStatement.setString(SECOND_PARAM_INDEX, user.getPassword());
            preparedStatement.setString(THIRD_PARAM_INDEX, user.getUserInfo().getFirstName());
            preparedStatement.setString(FOURTH_PARAM_INDEX, user.getUserInfo().getLastName());
            preparedStatement.setString(FIFTH_PARAM_INDEX, user.getUserInfo().getMail());
            preparedStatement.setLong(SIXTH_PARAM_INDEX, TEACHER_ID);
            preparedStatement.setLong(SEVENTH_PARAM_INDEX, subjectId);
            preparedStatement.setNull(EIGHTH_PARAM_INDEX, Types.BIGINT);
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.executeQuery(GET_GEN_ID);
            if (rs.next()) id = rs.getLong(FIRST_PARAM_INDEX);
            return id;
        } catch (SQLException e) {
            LOGGER.error(ERROR_CREATE_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public User getUserByLogin(String login) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_USER_BY_LOGIN);
            preparedStatement.setString(FIRST_PARAM_INDEX, login);
            resultSet = preparedStatement.executeQuery();
            return resultSet.next() ? createUserFromQueryResult(resultSet) : null;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<User> getTeachers() {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        List<User> teachers = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_ALL_TEACHERS);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) teachers.add(createUserFromQueryResult(resultSet));
            return teachers;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_ALL_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<User> getTeachers(int page) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        List<User> teachers = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_TEACHERS_BY_PAGE);
            preparedStatement.setInt(FIRST_PARAM_INDEX, DEFAULT_PAGE_SIZE);
            preparedStatement.setInt(SECOND_PARAM_INDEX, DEFAULT_PAGE_SIZE * page - DEFAULT_PAGE_SIZE);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) teachers.add(createUserFromQueryResult(resultSet));
            return teachers;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_ALL_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean changeUserMail(Long id, String newMail) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_UPDATE_USER_MAIL);
            preparedStatement.setString(FIRST_PARAM_INDEX, newMail);
            preparedStatement.setLong(SECOND_PARAM_INDEX, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            LOGGER.error("Error in update entity: " + e);
            return false;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<User> getUsersByRole(Long roleId) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        List<User> users = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_USERS_BY_ROLE);
            preparedStatement.setLong(FIRST_PARAM_INDEX, roleId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) users.add(createUserFromQueryResult(resultSet));
            return users;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_ALL_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<User> getNoClassStudents(int page) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        List<User> students = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_NO_CLASS_STUDENTS_BY_PAGE);
            preparedStatement.setInt(FIRST_PARAM_INDEX, DEFAULT_PAGE_SIZE);
            preparedStatement.setInt(SECOND_PARAM_INDEX, DEFAULT_PAGE_SIZE * page - DEFAULT_PAGE_SIZE);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) students.add(createUserFromQueryResult(resultSet));
            return students;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_ALL_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<User> getClassStudents(Long classId) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        List<User> students = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_CLASS_STUDENTS);
            preparedStatement.setLong(FIRST_PARAM_INDEX, classId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) students.add(createUserFromQueryResult(resultSet));
            return students;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_ALL_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<User> getClassStudents(Long classId, int page) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        List<User> students = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_CLASS_STUDENTS_BY_PAGE);
            preparedStatement.setLong(FIRST_PARAM_INDEX, classId);
            preparedStatement.setInt(SECOND_PARAM_INDEX, DEFAULT_PAGE_SIZE);
            preparedStatement.setInt(THIRD_PARAM_INDEX, DEFAULT_PAGE_SIZE * page - DEFAULT_PAGE_SIZE);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) students.add(createUserFromQueryResult(resultSet));
            return students;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_ALL_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<User> getTeachersBySubject(Long subjectId) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        List<User> teachers = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_TEACHERS_BY_SUBJECT_ID);
            preparedStatement.setLong(FIRST_PARAM_INDEX, subjectId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) teachers.add(createUserFromQueryResult(resultSet));
            return teachers;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_ALL_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean isLoginInUse(String login) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_NO_CLASS_STUDENT_COUNT);
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            Long count = Long.parseLong(String.valueOf(resultSet.getInt("COUNT(*)")));
            return count.equals(DEFAULT_ID);
        } catch (SQLException e) {
            LOGGER.error("Error in get entity count: " + e);
            return false;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public Long getNoClassStudentCount() {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_NO_CLASS_STUDENT_COUNT);
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return Long.parseLong(String.valueOf(resultSet.getInt("COUNT(*)")));
        } catch (SQLException e) {
            LOGGER.error("Error in get entity count: " + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public Long getClassStudentCount(Long classId) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_CLASS_STUDENT_COUNT);
            preparedStatement.setLong(FIRST_PARAM_INDEX, classId);
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return Long.parseLong(String.valueOf(resultSet.getInt("COUNT(*)")));
        } catch (SQLException e) {
            LOGGER.error("Error in get entity count: " + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public Long getTeacherCount() {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_TEACHER_COUNT);
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return Long.parseLong(String.valueOf(resultSet.getInt("COUNT(*)")));
        } catch (SQLException e) {
            LOGGER.error("Error in get entity count: " + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public User get(Long id) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_USER_BY_ID);
            preparedStatement.setLong(FIRST_PARAM_INDEX, id);
            resultSet = preparedStatement.executeQuery();
            return resultSet.next() ? createUserFromQueryResult(resultSet) : null;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<User> getAll() {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public Long create(User user) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        long id = DEFAULT_ID;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CREATE_USER);
            preparedStatement.setString(FIRST_PARAM_INDEX, user.getLogin());
            preparedStatement.setString(SECOND_PARAM_INDEX, user.getPassword());
            preparedStatement.setString(THIRD_PARAM_INDEX, user.getUserInfo().getFirstName());
            preparedStatement.setString(FOURTH_PARAM_INDEX, user.getUserInfo().getLastName());
            preparedStatement.setString(FIFTH_PARAM_INDEX, user.getUserInfo().getMail());
            preparedStatement.setLong(SIXTH_PARAM_INDEX, STUDENT_ID);
            preparedStatement.setNull(SEVENTH_PARAM_INDEX, Types.BIGINT);
            preparedStatement.setNull(EIGHTH_PARAM_INDEX, Types.BIGINT);
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.executeQuery(GET_GEN_ID);
            if (rs.next()) id = rs.getLong(FIRST_PARAM_INDEX);
            return id;
        } catch (SQLException e) {
            LOGGER.error(ERROR_CREATE_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean update(User user) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_UPDATE_USER);
            preparedStatement.setString(FIRST_PARAM_INDEX, user.getLogin());
            preparedStatement.setString(SECOND_PARAM_INDEX, user.getPassword());
            preparedStatement.setString(THIRD_PARAM_INDEX, user.getUserInfo().getFirstName());
            preparedStatement.setString(FOURTH_PARAM_INDEX, user.getUserInfo().getLastName());
            preparedStatement.setString(FIFTH_PARAM_INDEX, user.getUserInfo().getMail());
            preparedStatement.setLong(SIXTH_PARAM_INDEX, user.getRole().getId());
            if ((user.getSubject() == null)) {
                preparedStatement.setNull(SEVENTH_PARAM_INDEX, Types.BIGINT);
            } else {
                preparedStatement.setLong(SEVENTH_PARAM_INDEX, user.getSubject().getId());
            }
            if ((user.getStudyClass() == null)) {
                preparedStatement.setNull(EIGHTH_PARAM_INDEX, Types.BIGINT);
            } else {
                preparedStatement.setLong(EIGHTH_PARAM_INDEX, user.getStudyClass().getId());
            }
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            LOGGER.error(ERROR_UPDATE_LOG + e);
            return false;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public void delete(Long id) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_DELETE_USER_BY_ID);
            preparedStatement.setLong(FIRST_PARAM_INDEX, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(ERROR_DELETE_LOG + e);
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    private User createUserFromQueryResult(ResultSet resultSet) {
        try {
            Subject subject = null;
            Long subjectId = resultSet.getLong(NINTH_PARAM_INDEX);
            if (!subjectId.equals(DEFAULT_ID)) {
                subject = new Subject(subjectId, resultSet.getString(resultSet.getString(TENTH_PARAM_INDEX)));
            }
            StudyClass studyClass = null;
            Long classId = resultSet.getLong(ELEVENTH_PARAM_INDEX);
            if (!classId.equals(DEFAULT_ID)) {
                studyClass = new StudyClass(classId, resultSet.getString(TWELFTH_PARAM_INDEX),
                        resultSet.getLong(THIRTEENTH_PARAM_INDEX),
                        resultSet.getLong(FOURTEENTH_PARAM_INDEX), resultSet.getLong(FIFTEENTH_PARAM_INDEX),
                        resultSet.getLong(SIXTEENTH_PARAM_INDEX), resultSet.getLong(SEVENTEENTH_PARAM_INDEX),
                        resultSet.getLong(EIGHTEENTH_PARAM_INDEX));
            }
            return new User(resultSet.getLong(FIRST_PARAM_INDEX),
                    resultSet.getString(SECOND_PARAM_INDEX),
                    resultSet.getString(THIRD_PARAM_INDEX),
                    new UserInfo(resultSet.getString(FOURTH_PARAM_INDEX),
                            resultSet.getString(FIFTH_PARAM_INDEX),
                            resultSet.getString(SIXTH_PARAM_INDEX)),
                    new Role(resultSet.getLong(SEVENTH_PARAM_INDEX), resultSet.getString(EIGHTH_PARAM_INDEX)),
                    subject,
                    studyClass);
        } catch (SQLException e) {
            LOGGER.error(ERROR_CREATE_ENTITY_LOG + e);
            return null;
        }
    }

    private void close(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, ERROR_CLOSE_STATEMENT_LOG + e.getMessage());
        }
    }
}
