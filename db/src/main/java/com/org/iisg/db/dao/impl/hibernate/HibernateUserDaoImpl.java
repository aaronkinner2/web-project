package com.org.iisg.db.dao.impl.hibernate;

import com.org.iisg.db.connection.HibernateUtil;
import com.org.iisg.db.dao.inter.impl.UserDao;
import com.org.iisg.model.Role;
import com.org.iisg.model.StudyClass;
import com.org.iisg.model.Subject;
import com.org.iisg.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import java.util.List;

import static com.org.iisg.db.parameter.DaoParameter.*;

@Repository
public class HibernateUserDaoImpl implements UserDao {

    private final SessionFactory sessionFactory;

    private HibernateUserDaoImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    public static UserDao getUserDao() {
        return new HibernateUserDaoImpl();
    }

    @Override
    public User get(Long id) {
        try (final Session session = sessionFactory.openSession()) {
            return session.get(User.class, id);
        }
    }

    @Override
    public List<User> getAll() {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public Long create(User user) {
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            Role role = session.get(Role.class, STUDENT_ID);
            role.addUser(user);
            user.setRole(role);
            session.update(role);
            Long id = (Long) session.save(user);
            session.getTransaction().commit();
            return id;
        }
    }

    @Override
    public Long create(User user, Long subjectId) {
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            Role role = session.get(Role.class, TEACHER_ID);
            Subject subject = session.get(Subject.class, subjectId);
            role.addUser(user);
            subject.addUser(user);
            user.setRole(role);
            user.setSubject(subject);
            session.update(role);
            session.update(subject);
            Long id = (Long) session.save(user);
            session.getTransaction().commit();
            return id;
        }
    }

    @Override
    public boolean update(User user) {
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            session.update(user);
            session.getTransaction().commit();
            return true;
        } catch (OptimisticLockException e) {
            return false;
        }
    }

    @Override
    public void delete(Long id) {
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            User user = session.get(User.class, id);
            if (user != null) {
                Role role = session.get(Role.class, user.getRole().getId());
                if (role.getId().equals(TEACHER_ID)) {
                    Subject subject = session.get(Subject.class, user.getSubject().getId());
                    switch (subject.getId().intValue()) {
                        case MATHEMATICS_INT_ID: {
                            final Query<StudyClass> query = session
                                    .createQuery(HQL_GET_CLASSES_BY_MATH_TEACHER_ID, StudyClass.class)
                                    .setParameter(MATHEMATICS_TEACHER_ID_PARAM, id);
                            List<StudyClass> classes = query.getResultList();
                            for (StudyClass c : classes) {
                                c.setMathematicsTeacherId(null);
                                session.update(c);
                            }
                            break;
                        }
                        case INFORMATICS_INT_ID: {
                            final Query<StudyClass> query = session
                                    .createQuery(HQL_GET_CLASSES_BY_INFORM_TEACHER_ID, StudyClass.class)
                                    .setParameter(INFORMATICS_TEACHER_ID_PARAM, id);
                            List<StudyClass> classes = query.getResultList();
                            for (StudyClass c : classes) {
                                c.setInformaticsTeacherId(null);
                                session.update(c);
                            }
                            break;
                        }
                        case CHEMISTRY_INT_ID: {
                            final Query<StudyClass> query = session
                                    .createQuery(HQL_GET_CLASSES_BY_CHEM_TEACHER_ID, StudyClass.class)
                                    .setParameter(CHEMISTRY_TEACHER_ID_PARAM, id);
                            List<StudyClass> classes = query.getResultList();
                            for (StudyClass c : classes) {
                                c.setChemistryTeacherId(null);
                                session.update(c);
                            }
                            break;
                        }
                        case PHYSICS_INT_ID: {
                            final Query<StudyClass> query = session
                                    .createQuery(HQL_GET_CLASSES_BY_PHY_TEACHER_ID, StudyClass.class)
                                    .setParameter(PHYSICS_TEACHER_ID_PARAM, id);
                            List<StudyClass> classes = query.getResultList();
                            for (StudyClass c : classes) {
                                c.setPhysicsTeacherId(null);
                                session.update(c);
                            }
                            break;
                        }
                        case RUSSIAN_INT_ID: {
                            final Query<StudyClass> query = session
                                    .createQuery(HQL_GET_CLASSES_BY_RU_TEACHER_ID, StudyClass.class)
                                    .setParameter(RUSSIAN_TEACHER_ID_PARAM, id);
                            List<StudyClass> classes = query.getResultList();
                            for (StudyClass c : classes) {
                                c.setRussianTeacherId(null);
                                session.update(c);
                            }
                            break;
                        }
                        case ENGLISH_INT_ID: {
                            final Query<StudyClass> query = session
                                    .createQuery(HQL_GET_CLASSES_BY_EN_TEACHER_ID, StudyClass.class)
                                    .setParameter(ENGLISH_TEACHER_ID_PARAM, id);
                            List<StudyClass> classes = query.getResultList();
                            for (StudyClass c : classes) {
                                c.setEnglishTeacherId(null);
                                session.update(c);
                            }
                            break;
                        }
                    }
                }
                session.delete(user);
            }
            session.getTransaction().commit();
        }
    }

    @Override
    public User getUserByLogin(String login) {
        try (Session session = sessionFactory.openSession()) {
            final Query<User> query = session
                    .createQuery(HQL_GET_USER_BY_LOGIN, User.class)
                    .setParameter(LOGIN_PARAM, login);
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<User> getUsersByRole(Long roleId) {
        try (Session session = sessionFactory.openSession()) {
            final Query<User> query = session
                    .createQuery(HQL_GET_USERS_BY_ROLE_ID, User.class)
                    .setParameter(ROLE_ID_PARAM, roleId);
            return query.getResultList();
        }
    }

    @Override
    public List<User> getNoClassStudents(int page) {
        try (Session session = sessionFactory.openSession()) {
            final Query<User> query = session
                    .createQuery(HQL_GET_NO_CLASS_STUDENTS, User.class)
                    .setFirstResult((page - 1) * DEFAULT_PAGE_SIZE)
                    .setMaxResults(DEFAULT_PAGE_SIZE);
            return query.getResultList();
        }
    }

    @Override
    public List<User> getClassStudents(Long classId, int page) {
        try (Session session = sessionFactory.openSession()) {
            final Query<User> query = session
                    .createQuery(HQL_GET_CLASS_STUDENTS, User.class)
                    .setParameter(CLASS_ID_PARAM, classId)
                    .setFirstResult((page - 1) * DEFAULT_PAGE_SIZE)
                    .setMaxResults(DEFAULT_PAGE_SIZE);
            return query.getResultList();
        }
    }

    @Override
    public List<User> getClassStudents(Long classId) {
        try (Session session = sessionFactory.openSession()) {
            final Query<User> query = session
                    .createQuery(HQL_GET_CLASS_STUDENTS, User.class)
                    .setParameter(CLASS_ID_PARAM, classId);
            return query.getResultList();
        }
    }

    @Override
    public List<User> getTeachersBySubject(Long subjectId) {
        try (Session session = sessionFactory.openSession()) {
            final Query<User> query = session
                    .createQuery(HQL_GET_TEACHERS_BY_SUBJECT_ID, User.class)
                    .setParameter(SUBJECT_ID_PARAM, subjectId);
            return query.getResultList();
        }
    }

    @Override
    public List<User> getTeachers() {
        try (Session session = sessionFactory.openSession()) {
            final Query<User> query = session.createQuery(HQL_GET_TEACHERS, User.class);
            return query.getResultList();
        }
    }

    @Override
    public List<User> getTeachers(int page) {
        try (Session session = sessionFactory.openSession()) {
            final Query<User> query = session
                    .createQuery(HQL_GET_TEACHERS, User.class)
                    .setFirstResult((page - 1) * DEFAULT_PAGE_SIZE)
                    .setMaxResults(DEFAULT_PAGE_SIZE);
            return query.getResultList();
        }
    }

    @Override
    public boolean changeUserMail(Long id, String newMail) {
        try (Session session = sessionFactory.openSession()) {
            final Query<User> query = session
                    .createQuery(HQL_GET_USER_BY_ID, User.class)
                    .setParameter(ID_PARAM, id);
            User user = query.getSingleResult();
            user.getUserInfo().setMail(newMail);
            session.getTransaction().begin();
            session.update(user);
            session.getTransaction().commit();
            return true;
        } catch (NoResultException e) {
            return false;
        }
    }

    @Override
    public boolean isLoginInUse(String login) {
        try (Session session = sessionFactory.openSession()) {
            final Query<User> query = session
                    .createQuery(HQL_GET_USER_BY_LOGIN, User.class)
                    .setParameter(LOGIN_PARAM, login);
            query.getSingleResult();
            return true;
        } catch (NoResultException e) {
            return false;
        }
    }

    @Override
    public Long getNoClassStudentCount() {
        try (Session session = sessionFactory.openSession()) {
            return (Long) session.createQuery(HQL_GET_NO_CLASS_STUDENT_COUNT).uniqueResult();
        }
    }

    @Override
    public Long getClassStudentCount(Long classId) {
        try (Session session = sessionFactory.openSession()) {
            return (Long) session.createQuery(HQL_GET_CLASS_STUDENT_COUNT)
                    .setParameter(CLASS_ID_PARAM, classId)
                    .uniqueResult();
        }
    }

    @Override
    public Long getTeacherCount() {
        try (Session session = sessionFactory.openSession()) {
            return (Long) session.createQuery(HQL_GET_TEACHER_COUNT).uniqueResult();
        }
    }
}
