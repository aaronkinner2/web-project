package com.org.iisg.db.dao.impl.jdbc;

import com.org.iisg.db.connection.JdbcConnectionPool;
import com.org.iisg.db.dao.inter.impl.TaskDao;
import com.org.iisg.model.Subject;
import com.org.iisg.model.Task;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.org.iisg.db.parameter.DaoParameter.*;

public class JdbcTaskDaoImpl implements TaskDao {

    private static final Logger LOGGER = LogManager.getLogger(JdbcTaskDaoImpl.class);

    private static TaskDao taskDao;

    private final JdbcConnectionPool connectionPool;

    private JdbcTaskDaoImpl() {
        connectionPool = JdbcConnectionPool.getInstance();
    }

    public static TaskDao getInstance() {
        if (taskDao == null) {
            synchronized (JdbcTaskDaoImpl.class) {
                if (taskDao == null) {
                    taskDao = new JdbcTaskDaoImpl();
                }
            }
        }
        return taskDao;
    }

    @Override
    public List<Task> getByStudentId(Long studentId, Long subjectId, int page) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_TASK_BY_STUDENT_ID_AND_SUBJECT_ID);
            preparedStatement.setLong(FIRST_PARAM_INDEX, studentId);
            preparedStatement.setLong(SECOND_PARAM_INDEX, subjectId);
            preparedStatement.setInt(THIRD_PARAM_INDEX, DEFAULT_TASK_PAGE_SIZE);
            preparedStatement.setInt(FOURTH_PARAM_INDEX, DEFAULT_TASK_PAGE_SIZE * page - DEFAULT_TASK_PAGE_SIZE);
            resultSet = preparedStatement.executeQuery();
            List<Task> tasks = new ArrayList<>();
            while (resultSet.next()) tasks.add(createTaskFromQueryResult(resultSet));
            return tasks;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_LOG + e);
            return new ArrayList<>();
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public Long getTaskCount(Long studentId, Long subjectId) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_TASK_COUNT_BY_STUDENT_ID_AND_SUBJECT_ID);
            preparedStatement.setLong(FIRST_PARAM_INDEX, studentId);
            preparedStatement.setLong(SECOND_PARAM_INDEX, subjectId);
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return Long.parseLong(String.valueOf(resultSet.getInt("COUNT(*)")));
        } catch (SQLException e) {
            LOGGER.error("Error in get entity count: " + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public Task get(Long id) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_TASK_BY_ID);
            preparedStatement.setLong(FIRST_PARAM_INDEX, id);
            resultSet = preparedStatement.executeQuery();
            return resultSet.next() ? createTaskFromQueryResult(resultSet) : null;
        } catch (SQLException e) {
            LOGGER.error(ERROR_GET_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Task> getAll() {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public Long create(Task task) {
        throw new IllegalStateException(NO_SUPPORT_MESSAGE);
    }

    @Override
    public Long create(Task task, Long subjectId) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        long id = DEFAULT_ID;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CREATE_TASK);
            preparedStatement.setString(FIRST_PARAM_INDEX, task.getDescription());
            preparedStatement.setLong(SECOND_PARAM_INDEX, task.getStudentId());
            preparedStatement.setLong(THIRD_PARAM_INDEX, subjectId);
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.executeQuery(GET_GEN_ID);
            if (rs.next()) id = rs.getLong(FIRST_PARAM_INDEX);
            return id;
        } catch (SQLException e) {
            LOGGER.error(ERROR_CREATE_LOG + e);
            return null;
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean update(Task task) {
        return false;
    }

    @Override
    public void delete(Long id) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_DELETE_TASK_BY_ID);
            preparedStatement.setLong(FIRST_PARAM_INDEX, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(ERROR_DELETE_LOG + e);
        } finally {
            close(preparedStatement);
            if (connection != null) connectionPool.releaseConnection(connection);
        }
    }

    private Task createTaskFromQueryResult(ResultSet resultSet) {
        try {
            return new Task(resultSet.getLong(FIRST_PARAM_INDEX),
                    resultSet.getLong(SECOND_PARAM_INDEX),
                    resultSet.getString(THIRD_PARAM_INDEX),
                    new Subject(resultSet.getLong(FOURTH_PARAM_INDEX),
                            resultSet.getString(FIFTH_PARAM_INDEX)));
        } catch (SQLException e) {
            LOGGER.error(ERROR_CREATE_ENTITY_LOG + e);
            return null;
        }
    }

    private void close(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, ERROR_CLOSE_STATEMENT_LOG + e.getMessage());
        }
    }

}
