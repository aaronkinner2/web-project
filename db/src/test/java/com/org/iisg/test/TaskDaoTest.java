package com.org.iisg.test;

import com.org.iisg.db.dao.impl.jdbc.JdbcTaskDaoImpl;
import com.org.iisg.db.dao.inter.impl.TaskDao;
import com.org.iisg.model.Task;
import org.junit.Test;

public class TaskDaoTest {

    @Test
    public void testJdbcTaskCount() {
        TaskDao taskDao = JdbcTaskDaoImpl.getInstance();
        System.out.println(taskDao.getTaskCount(1L, 1L));
    }

    @Test
    public void testJdbcCreate() {
        TaskDao taskDao = JdbcTaskDaoImpl.getInstance();
        System.out.println(taskDao.create(new Task(1L, "Dimas"), 2L));
    }
}
