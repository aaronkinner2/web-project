package com.org.iisg.test;

import com.org.iisg.db.connection.HibernateUtil;
import com.org.iisg.model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.junit.Test;

import java.time.LocalDate;

public class HibernateUtilTest {

    @Test
    public void testSessionFactory() {
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        final StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            User user = new User("alex89", passwordEncryptor.encryptPassword("12345Al89"),
                    new UserInfo("Alex", "Volkov", "lll@gmail.com"));
            Role role = new Role("student");
            Subject subject = new Subject("Mathematics");
            Grade grade = new Grade(1L, 8, LocalDate.now());
            StudyClass cl = new StudyClass("10A", null, null,
                    null, null, null, null);
            Task task = new Task(1L, "Test task.");

            role.addUser(user);
            user.setRole(role);

            subject.addGrade(grade);
            grade.setSubject(subject);

            subject.addTask(task);
            task.setSubject(subject);

            cl.addStudent(user);
            user.setStudyClass(cl);

            session.saveOrUpdate(role);
            session.saveOrUpdate(subject);
            session.saveOrUpdate(cl);

            session.save(grade);
            session.save(user);
            session.save(task);
            session.flush();
            session.getTransaction().commit();
        } finally {
            HibernateUtil.closeSessionFactory();
        }

    }
}
