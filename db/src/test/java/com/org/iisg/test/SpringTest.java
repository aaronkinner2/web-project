package com.org.iisg.test;

import com.org.iisg.db.repository.UserRepository;
import com.org.iisg.model.Role;
import com.org.iisg.model.Subject;
import com.org.iisg.model.User;
import com.org.iisg.model.UserInfo;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/db-spring-context.xml")
public class SpringTest {

    @Autowired
    UserRepository userRepository;

    @Test
    public void findById() {
        User user = userRepository.findUserById(5L);
        System.out.println(user.getId());
    }

    @Test
    public void findByLogin() {
        User user = userRepository.findUserByLoginEquals("admin");
        System.out.println(user.getId());
    }

    @Test
    public void createUser() {
        User user = new User("dima105", "8935Selby",
                new UserInfo("Dima", "Tupov", "lll@gmail.com"));
        user.setRole(new Role(1L, ""));
        userRepository.save(user);
        User newUser = userRepository.findUserByLoginEquals(user.getLogin());
        System.out.println(newUser.getId());
        System.out.println();
    }

    @Test
    public void createTeacher() {
        final StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
        User user = new User("AlexIvanov", passwordEncryptor.encryptPassword( "8935Selby"),
                new UserInfo("Alexander", "Ivanov", "dkaezi@gmail.com"));
        user.setRole(new Role(2L ));
        user.setSubject(new Subject(5L ));
        userRepository.save(user);
        User newUser = userRepository.findUserByLoginEquals(user.getLogin());
        System.out.println(newUser.getId());
        System.out.println();
    }

    @Test
    public void getNoClassStudents() {
        List<User> list = userRepository.getUsersByStudyClass_IdIsNullAndRole_Id(1L);
        System.out.println(list.size());
    }
}
