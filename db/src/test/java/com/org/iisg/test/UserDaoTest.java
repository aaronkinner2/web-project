package com.org.iisg.test;

import com.org.iisg.db.connection.HibernateUtil;
import com.org.iisg.db.dao.impl.hibernate.HibernateUserDaoImpl;
import com.org.iisg.db.dao.inter.impl.UserDao;
import com.org.iisg.model.User;
import com.org.iisg.model.Role;
import com.org.iisg.model.Subject;
import com.org.iisg.model.UserInfo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.junit.Test;

import static com.org.iisg.db.parameter.DaoParameter.*;

public class UserDaoTest {

    @Test
    public void testGetAllStudentsByRole() {
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            Role role = session.get(Role.class, 1L);
            for (User user : role.getUsers()) {
                System.out.println(user.getLogin() + " " +
                        user.getRole().getName());
            }
            session.getTransaction().commit();
        } finally {
            HibernateUtil.closeSessionFactory();
        }
    }

    @Test
    public void testGetAllTeachersBySubject() {
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            Subject subject = session.get(Subject.class, 1L);
            for (User user : subject.getUsers()) {
                System.out.println(user.getLogin() + " " +
                        user.getRole().getName());
            }
            session.getTransaction().commit();
        } finally {
            HibernateUtil.closeSessionFactory();
        }
    }

    @Test
    public void testGetStudentById() {
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            User user = session.get(User.class, 6L);
            System.out.println(user.getLogin() + " " + user.getRole().getName());
            session.getTransaction().commit();
        } finally {
            HibernateUtil.closeSessionFactory();
        }
    }

    @Test
    public void testDeleteUserById() {
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            User user = session.get(User.class, 3L);
            Role role = session.get(Role.class, 2L);
            role.removeUser(user);
            session.saveOrUpdate(role);
            session.delete(user);
            session.getTransaction().commit();
        } finally {
            HibernateUtil.closeSessionFactory();
        }
    }

    @Test
    public void testUpdate() {
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            User user = session.get(User.class, 3L);
            Subject subject = session.get(Subject.class, 1L);
            user.setSubject(subject);
            session.update(user);
            session.getTransaction().commit();
        } finally {
            HibernateUtil.closeSessionFactory();
        }
    }

    @Test
    public void testGetUserByIdAndPassword() {
        final UserDao userDao = HibernateUserDaoImpl.getUserDao();
        User user = userDao.getUserByLogin("dimasss");
        if (user != null) System.out.println(user.getLogin() + " " + user.getPassword());
    }

    @Test
    public void testChangeUserMail() {
        final UserDao userDao = HibernateUserDaoImpl.getUserDao();
        System.out.println(userDao.changeUserMail(1L, "mmm@gmail.com"));
    }

    @Test
    public void testGetStudents() {
        final UserDao userDao = HibernateUserDaoImpl.getUserDao();
        for (User u : userDao.getUsersByRole(STUDENT_ID)) {
            System.out.println(u.getLogin() + " " + u.getPassword());
        }
    }

    @Test
    public void testGetTeachers() {
        final UserDao userDao = HibernateUserDaoImpl.getUserDao();
        for (User u : userDao.getUsersByRole(TEACHER_ID)) {
            System.out.println(u.getLogin() + " " + u.getPassword());
        }
    }

    @Test
    public void testGetTeachersBySubject() {
        final UserDao userDao = HibernateUserDaoImpl.getUserDao();
        for (User u : userDao.getTeachersBySubject(1L)) {
            System.out.println(u.getLogin() + " " + u.getPassword());
        }
    }

    @Test
    public void testIsLoginInUse() {
        final UserDao userDao = HibernateUserDaoImpl.getUserDao();
        System.out.println(userDao.getUserByLogin("dima16"));
    }

    @Test
    public void testCreateStudent() {
        final UserDao userDao = HibernateUserDaoImpl.getUserDao();
        System.out.println(userDao.create(new User("alex89", "12345Al89",
                new UserInfo("Alex", "Volkov", "lll@gmail.com"))));
    }

    @Test
    public void testCreateTeacher() {
        final UserDao userDao = HibernateUserDaoImpl.getUserDao();
        System.out.println(userDao.create(new User("alex60", "alex6000",
                new UserInfo("Alex", "Opers", "dkaezi@gmail.com")), 1L));
    }

    @Test
    public void testPasswordEncryption() {
        final StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
        final UserDao userDao = HibernateUserDaoImpl.getUserDao();
        User user = userDao.get(1L);
        System.out.println(passwordEncryptor.checkPassword("12345Al89j", user.getPassword()));
    }

    @Test
    public void testCreateAdmin() {
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        final StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
        try (final Session session = sessionFactory.openSession()) {
            User user = new User("admin", passwordEncryptor.encryptPassword("admin"),
                    new UserInfo("Admin", "Admin", "dkaezi@gmail.com"));
            session.getTransaction().begin();
            Role role = session.get(Role.class, 3L);
            role.addUser(user);
            user.setRole(role);
            session.update(role);
            Long id = (Long) session.save(user);
            session.getTransaction().commit();
            System.out.println(id);
        }
    }

    @Test
    public void testGetTeachersCount() {
        final UserDao userDao = HibernateUserDaoImpl.getUserDao();
        System.out.println(userDao.getTeacherCount());
    }

    @Test
    public void testUpdateNotExistingUser() {
        final UserDao userDao = HibernateUserDaoImpl.getUserDao();
        User user = userDao.get(2L);
        user.setId(3L);
        System.out.println(userDao.update(user));
    }

    @Test
    public void testDeleteNotExistingUser() {
        final UserDao userDao = HibernateUserDaoImpl.getUserDao();
        userDao.delete(10L);
    }
}
