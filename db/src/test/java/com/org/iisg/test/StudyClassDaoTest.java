package com.org.iisg.test;

import com.org.iisg.db.connection.HibernateUtil;
import com.org.iisg.db.dao.impl.hibernate.HibernateStudyClassDaoImpl;
import com.org.iisg.db.dao.inter.impl.StudyClassDao;
import com.org.iisg.model.StudyClass;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;

public class StudyClassDaoTest {

    @Test
    public void testDeleteStudent() {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (final Session session = sessionFactory.openSession()) {
            StudyClass studyClass = session.get(StudyClass.class, 10L);
            System.out.println(studyClass);
        }
    }

    @Test
    public void testGet() {
        final StudyClassDao dao = HibernateStudyClassDaoImpl.getStudyClassDao();
        System.out.println(dao.get(10L));
    }

    @Test
    public void testDelete() {
        final StudyClassDao dao = HibernateStudyClassDaoImpl.getStudyClassDao();
        dao.delete(4L);
    }
}
