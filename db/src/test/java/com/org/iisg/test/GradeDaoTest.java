package com.org.iisg.test;

import com.org.iisg.db.dao.impl.hibernate.HibernateGradeDaoImpl;
import com.org.iisg.db.dao.inter.impl.GradeDao;
import com.org.iisg.model.Grade;
import org.junit.Test;

import java.time.LocalDate;
import java.util.List;

public class GradeDaoTest {

    @Test
    public void testCreate() {
        final GradeDao gradeDao = HibernateGradeDaoImpl.getGradeDao();
        System.out.println(gradeDao.create(new Grade(1L, 8, LocalDate.now()), 1L));
    }

    @Test
    public void testGetAll() {
        final GradeDao gradeDao = HibernateGradeDaoImpl.getGradeDao();
        List<Grade> grades = gradeDao.getAll();
        for (Grade g : grades) System.out.println(g.getId() + " " + g.getMark() + " " + g.getDate());
    }

    @Test
    public void testDelete() {
        final GradeDao gradeDao = HibernateGradeDaoImpl.getGradeDao();
        gradeDao.delete(1L);
    }

    @Test
    public void testGetGradesByStudentId() {
        final GradeDao gradeDao = HibernateGradeDaoImpl.getGradeDao();
        List<Grade> grades = gradeDao.getGradesByStudentId(2L);
        for (Grade g : grades) System.out.println(g.getId() + " " + g.getMark() + " " + g.getDate());
    }

    @Test
    public void testGetGradesByStudentIdAndSubject() {
        final GradeDao gradeDao = HibernateGradeDaoImpl.getGradeDao();
        List<Grade> grades = gradeDao.getGradesByStudentIdAndSubjectId(1L, 1L);
        for (Grade g : grades) System.out.println(g.getId() + " " + g.getMark() + " " + g.getDate());
    }

    @Test
    public void testDeleteGradesByStudentId() {
        final GradeDao gradeDao = HibernateGradeDaoImpl.getGradeDao();
        gradeDao.deleteGradesByStudentId(1L);
    }

    @Test
    public void testDeleteGrades() {
        final GradeDao gradeDao = HibernateGradeDaoImpl.getGradeDao();
        gradeDao.deleteGrades();
    }

    @Test
    public void testAddTenGrades() {
        final GradeDao gradeDao = HibernateGradeDaoImpl.getGradeDao();
        for (int i = 0; i < 10; i++)
            gradeDao.create(new Grade(1L, 8, LocalDate.now()), 1L);
    }

    @Test
    public void testDeleteAll() {
        final GradeDao gradeDao = HibernateGradeDaoImpl.getGradeDao();
        gradeDao.deleteGrades();
    }
}
