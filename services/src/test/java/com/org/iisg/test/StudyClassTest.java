package com.org.iisg.test;

import com.org.iisg.model.StudyClass;
import com.org.iisg.service.impl.StudyClassServiceImpl;
import com.org.iisg.service.inter.StudyClassService;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class StudyClassTest {

    @Test
    public void testGetClassesByTeacherIdAndSubjectId() {
        final StudyClassService classService = StudyClassServiceImpl.getStudyClassService();
        List<StudyClass> classes = classService.getClassesByTeacherIdAndSubjectId(4L, 2L);
        for (StudyClass c : classes) {
            System.out.println(c.getId());
        }
    }

    @Test
    public void testCreate() {
        final StudyClassService classService = StudyClassServiceImpl.getStudyClassService();
        classService.createStudyClass("10A", Arrays.asList(null, 2L, null, null, null, null));
    }
}
