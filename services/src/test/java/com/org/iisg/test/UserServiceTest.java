package com.org.iisg.test;

import com.org.iisg.model.User;
import com.org.iisg.service.impl.UserServiceImpl;
import com.org.iisg.service.inter.UserService;
import org.junit.Test;

import java.util.List;

public class UserServiceTest {

    @Test
    public void testAuthorize() {
        final UserService userService = UserServiceImpl.getInstance();
        System.out.println(userService.authorize("alex89", "1234k5Al89"));
    }

    @Test
    public void testCreateTeacher() {
        final UserService userService = UserServiceImpl.getInstance();
        System.out.println(userService.createTeacher("alex1", "8935Selby",
               "Alex", "Chroms", "dkaezi@gmail.com", 2L));
    }

    @Test
    public void testGetTeachers() {
        final UserService userService = UserServiceImpl.getInstance();
        List<User> users = userService.getTeachers(true, 2);
        for (User u : users) System.out.println(u.getId() + " " + u.getLogin());
    }

    @Test
    public void testAddStudent() {
        final UserService userService = UserServiceImpl.getInstance();
        userService.changeClass(1L, 2L);
    }
}
