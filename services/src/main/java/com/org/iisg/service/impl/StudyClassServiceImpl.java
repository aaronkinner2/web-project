package com.org.iisg.service.impl;

import com.org.iisg.db.dao.inter.impl.StudyClassDao;
import com.org.iisg.db.dao.inter.impl.UserDao;
import com.org.iisg.model.StudyClass;
import com.org.iisg.model.StudyClassData;
import com.org.iisg.model.User;
import com.org.iisg.service.inter.StudyClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.org.iisg.service.parameter.ServiceParameter.*;

@Service
public class StudyClassServiceImpl implements StudyClassService {

    @Qualifier("springUserDaoImpl")
    @Autowired
    private UserDao userDao;

    @Qualifier("springStudyClassDaoImpl")
    @Autowired
    private StudyClassDao studyClassDao;

    private StudyClassServiceImpl() {

    }

    public static StudyClassService getStudyClassService() {
        return new StudyClassServiceImpl();
    }

    @Override
    public boolean createStudyClass(String name, List<Long> ids) {
        Long id = studyClassDao.create(new StudyClass(name, ids.get(MATHEMATICS_POS),
                ids.get(INFORMATICS_POS), ids.get(CHEMISTRY_POS), ids.get(PHYSICS_POS),
                ids.get(RUSSIAN_POS), ids.get(ENGLISH_POS)));
        return !id.equals(ERROR_ID);
    }

    @Override
    public StudyClassData getClassDataByStudentId(Long studentId) {
        final User user = userDao.get(studentId);
        if (user == null) return null;
        if (user.getStudyClass() == null) return null;
        StudyClass studyClass = studyClassDao.get(user.getStudyClass().getId());
        if (studyClass == null) return null;
        User mathTeacher = (studyClass.getMathematicsTeacherId() == null) ? null :
                userDao.get(studyClass.getMathematicsTeacherId());
        User informTeacher = (studyClass.getInformaticsTeacherId() == null) ? null :
                userDao.get(studyClass.getInformaticsTeacherId());
        User chemTeacher = (studyClass.getChemistryTeacherId() == null) ? null :
                userDao.get(studyClass.getChemistryTeacherId());
        User phyTeacher = (studyClass.getPhysicsTeacherId() == null) ? null :
                userDao.get(studyClass.getPhysicsTeacherId());
        User ruTeacher = (studyClass.getRussianTeacherId() == null) ? null :
                userDao.get(studyClass.getRussianTeacherId());
        User enTeacher = (studyClass.getEnglishTeacherId() == null) ? null :
                userDao.get(studyClass.getEnglishTeacherId());
        return new StudyClassData(
                Arrays.asList(mathTeacher, informTeacher, chemTeacher, phyTeacher, ruTeacher, enTeacher),
                studyClass.getName()
        );
    }

    @Override
    public StudyClassData getClassDataByClassId(Long classId) {
        StudyClass studyClass = studyClassDao.get(classId);
        if (studyClass == null) return null;
        User mathTeacher = (studyClass.getMathematicsTeacherId() == null) ? null :
                userDao.get(studyClass.getMathematicsTeacherId());
        User informTeacher = (studyClass.getInformaticsTeacherId() == null) ? null :
                userDao.get(studyClass.getInformaticsTeacherId());
        User chemTeacher = (studyClass.getChemistryTeacherId() == null) ? null :
                userDao.get(studyClass.getChemistryTeacherId());
        User phyTeacher = (studyClass.getPhysicsTeacherId() == null) ? null :
                userDao.get(studyClass.getPhysicsTeacherId());
        User ruTeacher = (studyClass.getRussianTeacherId() == null) ? null :
                userDao.get(studyClass.getRussianTeacherId());
        User enTeacher = (studyClass.getEnglishTeacherId() == null) ? null :
                userDao.get(studyClass.getEnglishTeacherId());
        return new StudyClassData(
                Arrays.asList(mathTeacher, informTeacher, chemTeacher, phyTeacher, ruTeacher, enTeacher),
                studyClass.getName()
        );
    }

    @Override
    public List<StudyClass> getAll() {
        List<StudyClass> classes = studyClassDao.getAll();
        if (classes == null) return new ArrayList<>();
        return classes;
    }

    @Override
    public List<StudyClass> getClassesByTeacherIdAndSubjectId(Long teacherId, Long subjectId) {
        List<StudyClass> classes = studyClassDao.getByTeacherIdAndSubjectId(teacherId, subjectId);
        if (classes == null) return new ArrayList<>();
        return classes;
    }

    @Override
    public boolean changeClass(Long classId, List<Long> teacherIds) {
        StudyClass studyClass = studyClassDao.get(classId);
        if (studyClass == null) return false;
        for (int i = START_ARRAY; i < teacherIds.size(); i++)
            if (teacherIds.get(i).equals(NO_CLASS_TEACHER_ID)) teacherIds.set(i, null);
        studyClass.setMathematicsTeacherId(teacherIds.get(MATHEMATICS_POS));
        studyClass.setInformaticsTeacherId(teacherIds.get(INFORMATICS_POS));
        studyClass.setChemistryTeacherId(teacherIds.get(CHEMISTRY_POS));
        studyClass.setPhysicsTeacherId(teacherIds.get(PHYSICS_POS));
        studyClass.setRussianTeacherId(teacherIds.get(RUSSIAN_POS));
        studyClass.setEnglishTeacherId(teacherIds.get(ENGLISH_POS));
        return studyClassDao.update(studyClass);
    }

    @Override
    public void deleteClass(Long classId) {
        studyClassDao.delete(classId);
    }
}
