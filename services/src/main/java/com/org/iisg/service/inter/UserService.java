package com.org.iisg.service.inter;

import com.org.iisg.model.User;

import java.util.List;

public interface UserService {

    User authorize(String login, String password);

    User getUserByLogin(String login);

    boolean createStudent(String login, String password, String name, String surname, String mail);

    boolean createTeacher(String login, String password, String name, String surname,
                          String mail, Long subjectId);

    User getUser(Long userId);

    List<User> getNoClassStudents(int page);

    List<User> getClassStudents(Long classId, int page);

    List<User> getTeachers(boolean isRuLocale, int page);

    void deleteUser(Long id);

    boolean changePassword(Long userId, String oldPassword, String newPassword);

    boolean checkEqualityOfPasswords(String password1, String password2);

    void changeMail(Long userId, String newMail);

    List<List<User>> getTeachersBySubject(boolean isRuLocale);

    User changeClass(Long studentId, Long newStudyClassId);

    List<Integer> getNoClassStudentTablePages();

    List<Integer> getClassStudentTablePages(Long classId);

    List<Integer> getTeacherTablePages();

    boolean isAccountExists(Long userId);
}
