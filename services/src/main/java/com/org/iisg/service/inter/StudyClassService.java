package com.org.iisg.service.inter;

import com.org.iisg.model.StudyClass;
import com.org.iisg.model.StudyClassData;

import java.util.List;

public interface StudyClassService {

    boolean createStudyClass(String name, List<Long> ids);

    StudyClassData getClassDataByStudentId(Long studentId);

    StudyClassData getClassDataByClassId(Long classId);

    List<StudyClass> getAll();

    List<StudyClass> getClassesByTeacherIdAndSubjectId(Long teacherId, Long subjectId);

    boolean changeClass(Long classId, List<Long> teacherIds);

    void deleteClass(Long classId);
}
