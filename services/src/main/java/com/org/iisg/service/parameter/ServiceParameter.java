package com.org.iisg.service.parameter;

public interface ServiceParameter {

    int SUBJECT_COUNT = 6;

    int START_MARK_COUNT = 0;

    double START_AVERAGE_MARK = 0.0;

    int START_DAY = 7;

    int END_DAY = 0;

    Long START_ID = 1L;

    Long END_ID = 6L;

    Long MATHEMATICS_ID = 1L;

    int MATHEMATICS_INT_ID = 1;

    int MATHEMATICS_POS = 0;

    Long INFORMATICS_ID = 2L;

    int INFORMATICS_INT_ID = 2;

    int INFORMATICS_POS = 1;

    Long CHEMISTRY_ID = 3L;

    int CHEMISTRY_INT_ID = 3;

    int CHEMISTRY_POS = 2;

    Long PHYSICS_ID = 4L;

    int PHYSICS_INT_ID = 4;

    int PHYSICS_POS = 3;

    Long RUSSIAN_ID = 5L;

    int RUSSIAN_INT_ID = 5;

    int RUSSIAN_POS = 4;

    Long ENGLISH_ID = 6L;

    int ENGLISH_INT_ID = 6;

    int ENGLISH_POS = 5;

    String MATHEMATICS_RU_NAME = "Математика";

    String INFORMATICS_RU_NAME = "Информатика";

    String CHEMISTRY_RU_NAME = "Химия";

    String PHYSICS_RU_NAME = "Физика";

    String RUSSIAN_RU_NAME = "Русский язык";

    String ENGLISH_RU_NAME = "Английский язык";

    Long NO_CLASS_STUDENT = 0L;

    Long NO_CLASS_TEACHER_ID = 0L;

    int DEFAULT_PAGE = 1;

    int DEFAULT_USER_COUNT = 7;

    int DEFAULT_TASK_COUNT = 4;

    int DEFAULT_PAGE_GRADE_COUNT = 8;

    Long ERROR_ID = 0L;

    int START_ARRAY = 0;
}
