package com.org.iisg.service.impl.manager;

import com.org.iisg.service.inter.GradeService;
import com.org.iisg.service.inter.StudyClassService;
import com.org.iisg.service.inter.TaskService;
import com.org.iisg.service.inter.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class ServletServiceManager {

    private static ServletServiceManager serviceManager;

    private final GradeService gradeService;

    private final TaskService taskService;

    private final UserService userService;

    private final StudyClassService studyClassService;

    @Autowired
    private ServletServiceManager(GradeService gradeService, TaskService taskService, UserService userService,
                           StudyClassService studyClassService) {
        this.gradeService = gradeService;
        this.taskService = taskService;
        this.userService = userService;
        this.studyClassService = studyClassService;
    }

    public static ServletServiceManager getInstance() {
        if (serviceManager == null) {
            synchronized (ControllerServiceManager.class) {
                if (serviceManager == null) {
                    try (ClassPathXmlApplicationContext context =
                                 new ClassPathXmlApplicationContext("applicationContext.xml")) {
                        serviceManager = context.getBean(ServletServiceManager.class);
                    }
                }
            }
        }
        return serviceManager;
    }

    public GradeService getGradeService() {
        return gradeService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public UserService getUserService() {
        return userService;
    }

    public StudyClassService getStudyClassService() {
        return studyClassService;
    }

}
