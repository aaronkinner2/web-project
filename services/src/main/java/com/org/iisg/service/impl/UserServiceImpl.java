package com.org.iisg.service.impl;

import com.org.iisg.db.dao.inter.impl.StudyClassDao;
import com.org.iisg.db.dao.inter.impl.UserDao;
import com.org.iisg.model.StudyClass;
import com.org.iisg.model.User;
import com.org.iisg.model.UserInfo;
import com.org.iisg.service.inter.UserService;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.org.iisg.service.parameter.ServiceParameter.*;

@Service
public class UserServiceImpl implements UserService {

    @Qualifier("springUserDaoImpl")
    @Autowired
    private UserDao userDao;

    @Qualifier("springStudyClassDaoImpl")
    @Autowired
    private StudyClassDao studyClassDao;

    private UserServiceImpl() {

    }

    public static UserService getInstance() {
        return new UserServiceImpl();
    }

    @Override
    public User authorize(String login, String password) {
        final StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
        User user = userDao.getUserByLogin(login);
        if (user == null) {
            return null;
        } else {
            if (passwordEncryptor.checkPassword(password, user.getPassword())) {
                return user;
            } else {
                return null;
            }
        }
    }

    @Override
    public User getUserByLogin(String login) {
        return userDao.getUserByLogin(login);
    }

    @Override
    public boolean createStudent(String login, String password, String name, String surname, String mail) {
        if (userDao.isLoginInUse(login)) {
            return false;
        } else {
            final StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
            User user = new User(login, passwordEncryptor.encryptPassword(password),
                    new UserInfo(name, surname, mail));
            userDao.create(user);
            return true;
        }
    }

    @Override
    public boolean createTeacher(String login, String password, String name, String surname,
                                 String mail, Long subjectId) {
        if (userDao.isLoginInUse(login)) {
            return false;
        } else {
            final StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
            User user = new User(login, passwordEncryptor.encryptPassword(password),
                    new UserInfo(name, surname, mail));
            userDao.create(user, subjectId);
            return true;
        }
    }

    @Override
    public User getUser(Long userId) {
        return userDao.get(userId);
    }

    @Override
    public List<User> getNoClassStudents(int page) {
        List<User> students = userDao.getNoClassStudents(page);
        if (students == null) return new ArrayList<>();
        return students;
    }

    @Override
    public List<User> getClassStudents(Long classId, int page) {
        List<User> students = userDao.getClassStudents(classId, page);
        if (students == null) return new ArrayList<>();
        return students;
    }

    @Override
    public List<User> getTeachers(boolean isRuLocale, int page) {
        List<User> teachers = userDao.getTeachers(page);
        if (isRuLocale) {
            return setRuLocaleForUsers(teachers);
        } else {
            return teachers;
        }
    }

    @Override
    public List<List<User>> getTeachersBySubject(boolean isRuLocale) {
        List<List<User>> allTeachers = new ArrayList<>();
        List<User> teachers = userDao.getTeachers();
        if (teachers == null) return null;
        if (isRuLocale) setRuLocaleForUsers(teachers);
        for (long i = START_ID; i <= END_ID; i++)
            allTeachers.add(getTeachersBySubject(teachers, i));
        return allTeachers;
    }

    @Override
    public void deleteUser(Long id) {
        userDao.delete(id);
    }

    @Override
    public boolean changePassword(Long userId, String oldPassword, String newPassword) {
        final StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
        User user = userDao.get(userId);
        if (user == null) {
            return false;
        } else {
            if (passwordEncryptor.checkPassword(oldPassword, user.getPassword())) {
                user.setPassword(passwordEncryptor.encryptPassword(newPassword));
                userDao.update(user);
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public boolean checkEqualityOfPasswords(String password1, String password2) {
        return password1.equals(password2);
    }

    @Override
    public void changeMail(Long userId, String newMail) {
        User user = userDao.get(userId);
        if (user != null) {
            user.getUserInfo().setMail(newMail);
            userDao.update(user);
        }
    }

    @Override
    public User changeClass(Long studentId, Long newStudyClassId) {
        User student = userDao.get(studentId);
        if (student == null) {
            return null;
        } else {
            if (newStudyClassId.equals(NO_CLASS_STUDENT)) {
                student.setStudyClass(null);
            } else {
                StudyClass newStudyClass = studyClassDao.get(newStudyClassId);
                student.setStudyClass(newStudyClass);
            }
            if (userDao.update(student)) {
                return student;
            } else {
                return null;
            }
        }
    }

    @Override
    public List<Integer> getNoClassStudentTablePages() {
        Long studentCount = userDao.getNoClassStudentCount();
        int pageCount = studentCount.intValue() / DEFAULT_USER_COUNT + DEFAULT_PAGE;
        List<Integer> pageNumbers = new ArrayList<>();
        for (int i = 1; i <= pageCount; i++) pageNumbers.add(i);
        return pageNumbers;
    }

    @Override
    public List<Integer> getClassStudentTablePages(Long classId) {
        Long studentCount = userDao.getClassStudentCount(classId);
        int pageCount = studentCount.intValue() / DEFAULT_USER_COUNT + DEFAULT_PAGE;
        List<Integer> pageNumbers = new ArrayList<>();
        for (int i = 1; i <= pageCount; i++) pageNumbers.add(i);
        return pageNumbers;
    }

    @Override
    public List<Integer> getTeacherTablePages() {
        Long teacherCount = userDao.getTeacherCount();
        int pageCount = teacherCount.intValue() / DEFAULT_USER_COUNT + DEFAULT_PAGE;
        List<Integer> pageNumbers = new ArrayList<>();
        for (int i = 1; i <= pageCount; i++) pageNumbers.add(i);
        return pageNumbers;
    }

    @Override
    public boolean isAccountExists(Long userId) {
        User user = userDao.get(userId);
        return user != null;
    }

    private List<User> setRuLocaleForUsers(List<User> users) {
        for (User u : users) {
            switch (u.getSubject().getId().intValue()) {
                case MATHEMATICS_INT_ID: {
                    u.getSubject().setName(MATHEMATICS_RU_NAME);
                    break;
                }
                case INFORMATICS_INT_ID: {
                    u.getSubject().setName(INFORMATICS_RU_NAME);
                    break;
                }
                case CHEMISTRY_INT_ID: {
                    u.getSubject().setName(CHEMISTRY_RU_NAME);
                    break;
                }
                case PHYSICS_INT_ID: {
                    u.getSubject().setName(PHYSICS_RU_NAME);
                    break;
                }
                case RUSSIAN_INT_ID: {
                    u.getSubject().setName(RUSSIAN_RU_NAME);
                    break;
                }
                case ENGLISH_INT_ID: {
                    u.getSubject().setName(ENGLISH_RU_NAME);
                    break;
                }
            }
        }
        return users;
    }

    private List<User> getTeachersBySubject(List<User> teachers, Long subjectId) {
        return teachers.stream().filter(t -> t.getSubject().getId().equals(subjectId)).collect(Collectors.toList());
    }
}
