package com.org.iisg.service.impl;

import com.org.iisg.db.dao.inter.impl.GradeDao;
import com.org.iisg.model.Grade;
import com.org.iisg.service.inter.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.org.iisg.service.parameter.ServiceParameter.*;

@Service
public class GradeServiceImpl implements GradeService {

    @Qualifier("springGradeDaoImpl")
    @Autowired
    private GradeDao gradeDao;

    private GradeServiceImpl() {

    }

    public static GradeService getGradeService() {
        return new GradeServiceImpl();
    }

    @Override
    public void addGrade(Long studentId, int mark, LocalDate date, Long subjectId) {
        gradeDao.create(new Grade(studentId, mark, date), subjectId);
    }

    @Override
    public List<Grade> getGradesByUserId(Long userId, boolean isRuLocale) {
        List<Grade> grades = gradeDao.getGradesByStudentId(userId);
        if (grades == null) return new ArrayList<>();
        if (isRuLocale) {
            return setRuLocaleForGrades(grades);
        } else {
            return grades;
        }
    }

    @Override
    public List<Grade> getGradesByUserIdAndSubjectId(Long userId, Long subjectId, boolean isRuLocale) {
        List<Grade> grades = gradeDao.getGradesByStudentIdAndSubjectId(userId, subjectId);
        if (grades == null) {
            grades = new ArrayList<>();
        }
        if (isRuLocale) {
            return setRuLocaleForGrades(grades);
        } else {
            return grades;
        }
    }

    @Override
    public void deleteGrade(Long gradeId) {
        gradeDao.delete(gradeId);
    }

    @Override
    public void deleteAllGrades() {
        gradeDao.deleteGrades();
    }

    @Override
    public List<Double> getAverageMark(List<Grade> grades) {
        Double[] subjectAverage = new Double[6];
        int[] subjectCount = new int[6];
        for (int i = 0; i < SUBJECT_COUNT; i++) {
            subjectAverage[i] = START_AVERAGE_MARK;
            subjectCount[i] = START_MARK_COUNT;
        }
        if (grades == null) {
            List<Double> doubles = new ArrayList<>();
            doubles.add(START_AVERAGE_MARK);
            doubles.addAll(Arrays.asList(subjectAverage));
            return doubles;
        } else {
            for (Grade g : grades) {
                if (g.getSubject().getId().equals(MATHEMATICS_ID)) {
                    subjectAverage[MATHEMATICS_POS] += g.getMark();
                    subjectCount[MATHEMATICS_POS]++;
                } else if (g.getSubject().getId().equals(INFORMATICS_ID)) {
                    subjectAverage[INFORMATICS_POS] += g.getMark();
                    subjectCount[INFORMATICS_POS]++;
                } else if (g.getSubject().getId().equals(CHEMISTRY_ID)) {
                    subjectAverage[CHEMISTRY_POS] += g.getMark();
                    subjectCount[CHEMISTRY_POS]++;
                } else if (g.getSubject().getId().equals(PHYSICS_ID)) {
                    subjectAverage[PHYSICS_POS] += g.getMark();
                    subjectCount[PHYSICS_POS]++;
                } else if (g.getSubject().getId().equals(RUSSIAN_ID)) {
                    subjectAverage[RUSSIAN_POS] += g.getMark();
                    subjectCount[RUSSIAN_POS]++;
                } else if (g.getSubject().getId().equals(ENGLISH_ID)) {
                    subjectAverage[ENGLISH_POS] += g.getMark();
                    subjectCount[ENGLISH_POS]++;
                }
            }
            for (int i = 0; i < SUBJECT_COUNT; i++) {
                subjectAverage[i] = getAverage(subjectAverage[i], subjectCount[i]);
            }
            double commonAv = getAverage(Arrays
                    .stream(subjectAverage)
                    .mapToDouble(Double::doubleValue).sum(), SUBJECT_COUNT);
            List<Double> doubles = new ArrayList<>();
            doubles.add(commonAv);
            doubles.addAll(Arrays.asList(subjectAverage));
            return doubles;
        }
    }

    @Override
    public List<LocalDate> getGradeNearbyDates() {
        LocalDate start = LocalDate.now();
        List<LocalDate> dates = new ArrayList<>();
        for (int i = START_DAY; i >= END_DAY; i--) {
            LocalDate ld = start.minusDays(i);
            dates.add(ld);
        }
        return dates;
    }

    @Override
    public List<Grade> getCurrentPageGrades(List<Grade> grades, int page) {
        if (grades.isEmpty()) return grades;
        int startIndex = (page - 1) * DEFAULT_PAGE_GRADE_COUNT;
        int endIndex = page * DEFAULT_PAGE_GRADE_COUNT;
        int gradeCurrentPageSize = grades.size() - DEFAULT_PAGE_GRADE_COUNT * (page - 1);
        if (gradeCurrentPageSize < DEFAULT_PAGE_GRADE_COUNT) {
            return grades.subList(startIndex, startIndex + gradeCurrentPageSize);
        }
        return grades.subList(startIndex, endIndex);
    }

    @Override
    public List<Integer> getGradeTablePages(List<Grade> grades) {
        int pageCount = grades.size() / DEFAULT_PAGE_GRADE_COUNT + 1;
        List<Integer> pageNumbers = new ArrayList<>();
        for (int i = 1; i <= pageCount; i++) {
            pageNumbers.add(i);
        }
        return pageNumbers;
    }

    private List<Grade> setRuLocaleForGrades(List<Grade> grades) {
        for (Grade g : grades) {
            switch (g.getSubject().getId().intValue()) {
                case MATHEMATICS_INT_ID: {
                    g.getSubject().setName(MATHEMATICS_RU_NAME);
                    break;
                }
                case INFORMATICS_INT_ID: {
                    g.getSubject().setName(INFORMATICS_RU_NAME);
                    break;
                }
                case CHEMISTRY_INT_ID: {
                    g.getSubject().setName(CHEMISTRY_RU_NAME);
                    break;
                }
                case PHYSICS_INT_ID: {
                    g.getSubject().setName(PHYSICS_RU_NAME);
                    break;
                }
                case RUSSIAN_INT_ID: {
                    g.getSubject().setName(RUSSIAN_RU_NAME);
                    break;
                }
                case ENGLISH_INT_ID: {
                    g.getSubject().setName(ENGLISH_RU_NAME);
                    break;
                }
            }
        }
        return grades;
    }

    private double getAverage(double sum, int count) {
        if (count == START_MARK_COUNT) return START_AVERAGE_MARK;
        double res = sum / count;
        return Double.parseDouble(new DecimalFormat("#.##").format(res));
    }
}
