package com.org.iisg.service.impl;

import com.org.iisg.db.dao.inter.impl.TaskDao;
import com.org.iisg.db.dao.inter.impl.UserDao;
import com.org.iisg.model.Task;
import com.org.iisg.model.User;
import com.org.iisg.service.inter.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.org.iisg.service.parameter.ServiceParameter.*;

@Service
public class TaskServiceImpl implements TaskService {

    @Qualifier("springTaskDaoImpl")
    @Autowired
    private TaskDao taskDao;

    @Qualifier("springUserDaoImpl")
    @Autowired
    private UserDao userDao;

    private TaskServiceImpl() {

    }

    public static TaskService getTaskService() {
        return new TaskServiceImpl();
    }

    @Override
    public void create(String description, Long studentId, Long subjectId) {
        if (userDao.get(studentId) != null) {
            taskDao.create(new Task(studentId, description), subjectId);
        }
    }

    @Override
    public void createForClass(String description, Long classId, Long subjectId) {
        List<User> userList = userDao.getClassStudents(classId);
        for (User user : userList) {
            taskDao.create(new Task(user.getId(), description), subjectId);
        }
    }

    @Override
    public List<Task> getTasksByStudentId(Long studentId, Long subjectId, int page) {
        return taskDao.getByStudentId(studentId, subjectId, page);
    }

    @Override
    public List<Integer> getStudentTaskTablePages(Long studentId, Long subjectId) {
        Long taskCount = taskDao.getTaskCount(studentId, subjectId);
        int pageCount = taskCount.intValue() / DEFAULT_TASK_COUNT + DEFAULT_PAGE;
        List<Integer> pageNumbers = new ArrayList<>();
        for (int i = 1; i <= pageCount; i++) {
            pageNumbers.add(i);
        }
        return pageNumbers;
    }

    @Override
    public void deleteTask(Long id) {
        if (id != null) {
            taskDao.delete(id);
        }
    }

    @Override
    public String getRuSubjectName(Long subjectId) {
        switch (subjectId.intValue()) {
            case MATHEMATICS_INT_ID: {
                return MATHEMATICS_RU_NAME;
            }
            case INFORMATICS_INT_ID: {
                return INFORMATICS_RU_NAME;
            }
            case CHEMISTRY_INT_ID: {
                return CHEMISTRY_RU_NAME;
            }
            case PHYSICS_INT_ID: {
                return PHYSICS_RU_NAME;
            }
            case RUSSIAN_INT_ID: {
                return RUSSIAN_RU_NAME;
            }
            case ENGLISH_INT_ID: {
                return ENGLISH_RU_NAME;
            }
        }
        return MATHEMATICS_RU_NAME;
    }
}
