package com.org.iisg.service.parameter;

public interface MailParameter {

    String APPLICATION_CONTEXT_PATH = "applicationContext.xml";

    String ORGANIZATION_EMAIL = "com.iisg.org@gmail.com";

    String ORGANIZATION_PASSWORD = "450381Isg";

    int PORT = 587;

    String HOST = "smtp.gmail.com";

    String PROTOCOL_KEY = "mail.transport.protocol";

    String PROTOCOL_VALUE = "smtp";

    String AUTH_KEY = "mail.smtp.auth";

    String AUTH_VALUE = "true";

    String STARTTLS_ENABLE_KEY = "mail.smtp.starttls.enable";

    String STARTTLS_ENABLE_VALUE = "true";

    String DEBUG_KEY = "mail.debug";

    String DEBUG_VALUE = "true";

    String SUCCESS_SEND_LOG = "Message successfully send.";
}
