package com.org.iisg.service.inter;

import com.org.iisg.model.Task;

import java.util.List;

public interface TaskService {

    void create(String description, Long studentId, Long subjectId);

    void createForClass(String description, Long classId, Long subjectId);

    List<Task> getTasksByStudentId(Long studentId, Long subjectId, int page);

    List<Integer> getStudentTaskTablePages(Long studentId, Long subjectId);

    void deleteTask(Long id);

    String getRuSubjectName(Long subjectId);
}
