package com.org.iisg.service.mail;

import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.util.Properties;

import static com.org.iisg.service.parameter.MailParameter.*;

@Component
public class MailConfig {

    @Bean(name = "emailSender")
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(HOST);
        mailSender.setPort(PORT);
        mailSender.setUsername(ORGANIZATION_EMAIL);
        mailSender.setPassword(ORGANIZATION_PASSWORD);
        Properties props = mailSender.getJavaMailProperties();
        props.put(PROTOCOL_VALUE, PROTOCOL_KEY);
        props.put(AUTH_KEY, AUTH_VALUE);
        props.put(STARTTLS_ENABLE_KEY, STARTTLS_ENABLE_VALUE);
        props.put(DEBUG_KEY, DEBUG_VALUE);
        return mailSender;
    }
}
