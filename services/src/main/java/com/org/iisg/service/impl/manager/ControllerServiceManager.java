package com.org.iisg.service.impl.manager;

import com.org.iisg.service.inter.GradeService;
import com.org.iisg.service.inter.StudyClassService;
import com.org.iisg.service.inter.TaskService;
import com.org.iisg.service.inter.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ControllerServiceManager {

    private final GradeService gradeService;

    private final TaskService taskService;

    private final UserService userService;

    private final StudyClassService studyClassService;

    @Autowired
    private ControllerServiceManager(GradeService gradeService, TaskService taskService, UserService userService,
                                     StudyClassService studyClassService) {
        this.gradeService = gradeService;
        this.taskService = taskService;
        this.userService = userService;
        this.studyClassService = studyClassService;
    }

    public GradeService getGradeService() {
        return gradeService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public UserService getUserService() {
        return userService;
    }

    public StudyClassService getStudyClassService() {
        return studyClassService;
    }
}
