package com.org.iisg.service.inter;

import com.org.iisg.model.Grade;

import java.time.LocalDate;
import java.util.List;

public interface GradeService {

    void addGrade(Long studentId, int mark, LocalDate date, Long subjectId);

    List<Grade> getGradesByUserId(Long userId, boolean isRuLocale);

    List<Grade> getGradesByUserIdAndSubjectId(Long userId, Long subjectId, boolean isRuLocale);

    void deleteGrade(Long gradeId);

    void deleteAllGrades();

    List<Double> getAverageMark(List<Grade> grades);

    List<LocalDate> getGradeNearbyDates();

    List<Grade> getCurrentPageGrades(List<Grade> grades, int page);

    List<Integer> getGradeTablePages(List<Grade> grades);
}
