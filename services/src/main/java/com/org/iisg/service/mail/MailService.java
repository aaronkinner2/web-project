package com.org.iisg.service.mail;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import static com.org.iisg.service.parameter.MailMessages.*;
import static com.org.iisg.service.parameter.MailParameter.*;

@Component
public class MailService {

    private static final Logger LOGGER = LogManager.getLogger(MailService.class);

    public static MailService mailService;

    private final JavaMailSender mailSender;

    @Autowired
    private MailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public static MailService getInstance() {
        if (mailService == null) {
            synchronized (MailService.class) {
                if (mailService == null) {
                    ClassPathXmlApplicationContext context =
                            new ClassPathXmlApplicationContext(APPLICATION_CONTEXT_PATH);
                    mailService = context.getBean(MailService.class);
                    context.close();
                }
            }
        }
        return mailService;
    }

    public void sendRegMessage(String destinationMail, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(ORGANIZATION_EMAIL);
        message.setTo(destinationMail);
        message.setSubject(subject);
        message.setText(text);
        try {
            this.mailSender.send(message);
        } catch (MailException ex) {
            LOGGER.error(ex.getMessage());
        }
        LOGGER.info(SUCCESS_SEND_LOG);
    }

    public void sendEmailChangeMessage(String login, String email) {
        sendRegMessage(
                email,
                SUCCESSFUL_CHANGE_MAIL,
                START_TEXT + login + SUCCESSFUL_CHANGE_MAIL_TEXT
        );
    }

    public void sendPasswordChangeMessage(String login, String email) {
        sendRegMessage(
                email,
                SUCCESSFUL_CHANGE_PASSWORD,
                START_TEXT + login + SUCCESSFUL_CHANGE_PASSWORD_TEXT
        );
    }

    public void sendSuccessStudentSignUp(String login, String email) {
        sendRegMessage(email, SUCCESSFUL_SIGN_UP, START_TEXT + login + SUCCESSFUL_SIGN_UP_TEXT);
    }

    public void sendSuccessTeacherSignUp(String login, String password, String email) {
        String messageText = SUCCESSFUL_SIGN_UP_1 + login + SUCCESSFUL_SIGN_UP_2 + password + SUCCESSFUL_SIGN_UP_TEXT_3;
        mailService.sendRegMessage(
                email,
                SUCCESSFUL_SIGN_UP,
                messageText
        );
    }
}
