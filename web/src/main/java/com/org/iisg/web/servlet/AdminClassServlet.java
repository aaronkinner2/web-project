package com.org.iisg.web.servlet;

import com.org.iisg.model.StudyClassData;
import com.org.iisg.model.User;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "AdminClass", urlPatterns = "/adminClass")
public class AdminClassServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        String locale = (String) session.getAttribute(PARAM_LOCALE);
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case CHANGE_COMMAND: {
                if (doChangeClassPage(request, classId, locale)) {
                    getServletContext().getRequestDispatcher(CHANGE_CLASS_PAGE).forward(request, response);
                } else {
                    doAdminClassesPage(request, session);
                    getServletContext().getRequestDispatcher(ADMIN_CLASSES_PAGE).forward(request, response);
                }
                break;
            }
            case DELETE_COMMAND: {
                deleteClass(classId);
                doAdminClassesPage(request, session);
                getServletContext().getRequestDispatcher(ADMIN_CLASSES_PAGE).forward(request, response);
                break;
            }
            case MAIN_COMMAND: {
                doAdminClassesPage(request, session);
                getServletContext().getRequestDispatcher(ADMIN_CLASSES_PAGE).forward(request, response);
                break;
            }
            case GET_COMMAND: {
                if (doAdminStudentsPage(request, session, classId)) {
                    getServletContext().getRequestDispatcher(ADMIN_STUDENTS_PAGE).forward(request, response);
                } else {
                    doAdminClassesPage(request, session);
                    getServletContext().getRequestDispatcher(ADMIN_CLASSES_PAGE).forward(request, response);
                }
                break;
            }
        }
    }

    private void doAdminClassesPage(HttpServletRequest request, HttpSession session) {
        request.setAttribute(PARAM_CLASSES, serviceManager.getStudyClassService().getAll());
        session.removeAttribute(PARAM_CLASS_ID);
    }

    private boolean doAdminStudentsPage(HttpServletRequest request, HttpSession session,  Long classId) {
        List<User> students = serviceManager.getUserService().getClassStudents(classId, DEFAULT_PAGE);
        request.setAttribute(PARAM_PAGES, serviceManager.getUserService().getClassStudentTablePages(classId));
        request.setAttribute(PARAM_STUDENTS, students);
        session.setAttribute(PARAM_CLASS_ID, classId);
        return true;
    }

    private boolean doChangeClassPage(HttpServletRequest request, Long classId, String locale) {
        StudyClassData studyClassData = serviceManager.getStudyClassService().getClassDataByClassId(classId);
        if (studyClassData == null) return false;
        request.setAttribute(PARAM_CLASS_NAME, studyClassData.getName());
        for (int i = START_ARRAY_COUNT; i < SUBJECT_COUNT; i++) {
            User teacher = studyClassData.getTeachers().get(i);
            if (teacher == null) {
                request.setAttribute(DEFAULT_CLASS_TEACHER_PARAMS.get(i), null);
            } else {
                request.setAttribute(DEFAULT_CLASS_TEACHER_PARAMS.get(i), teacher.getUserInfo().getFirstName() + SPACE +
                        teacher.getUserInfo().getLastName());
            }
        }
        List<List<User>> teachers =
                serviceManager.getUserService().getTeachersBySubject(locale.equals(RU_LOCALE));
        if (teachers == null) {
            for (int i = START_ARRAY_COUNT; i < SUBJECT_COUNT; i++)
                request.setAttribute(TEACHER_LISTS_PARAMS.get(i), null);
        } else {
            for (int i = START_ARRAY_COUNT; i < SUBJECT_COUNT; i++)
                request.setAttribute(TEACHER_LISTS_PARAMS.get(i), teachers.get(i));
        }
        return true;
    }

    private void deleteClass(Long classId) {
        serviceManager.getStudyClassService().deleteClass(classId);
    }
}
