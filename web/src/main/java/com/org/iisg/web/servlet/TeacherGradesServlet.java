package com.org.iisg.web.servlet;

import com.org.iisg.model.Grade;
import com.org.iisg.model.User;
import com.org.iisg.db.parameter.ServletParameter;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "TeacherGrades", urlPatterns = "/teacherGrades")
public class TeacherGradesServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        Long teacherId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case MAIN_COMMAND: {
                if (serviceManager.getUserService().isAccountExists(teacherId)) {
                    if (doTeacherStudentsPage(request, session)) {
                        getServletContext().getRequestDispatcher(TEACHER_STUDENTS_PAGE).forward(request, response);
                    } else {
                        doMainTeacherPage(request, session, teacherId, subjectId);
                        getServletContext().getRequestDispatcher(MAIN_TEACHER_PAGE).forward(request, response);
                    }
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case DELETE_COMMAND: {
                if (serviceManager.getUserService().isAccountExists(teacherId)) {
                    String gradeParam = request.getParameter(PARAM_GRADE_ID);
                    if (gradeParam != null) deleteGrade(Long.parseLong(gradeParam));
                    if (doTeacherGradesPage(request, session, subjectId, DEFAULT_PAGE)) {
                        getServletContext().getRequestDispatcher(TEACHER_GRADES_PAGE).forward(request, response);
                    } else {
                        if (doTeacherStudentsPage(request, session)) {
                            getServletContext().getRequestDispatcher(TEACHER_STUDENTS_PAGE).forward(request, response);
                        } else {
                            doMainTeacherPage(request, session, teacherId, subjectId);
                            getServletContext().getRequestDispatcher(MAIN_TEACHER_PAGE).forward(request, response);
                        }
                    }
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case ADD_COMMAND: {
                if (serviceManager.getUserService().isAccountExists(teacherId)) {
                    if (doTeacherGradesPage(request, session, subjectId, DEFAULT_PAGE)) {
                        addGrade((Long) session.getAttribute(PARAM_STUDENT_ID),
                                Integer.parseInt(request.getParameter(PARAM_MARK)),
                                LocalDate.parse(request.getParameter(PARAM_DATE)),
                                subjectId);
                        getServletContext().getRequestDispatcher(TEACHER_GRADES_PAGE).forward(request, response);
                    } else {
                        if (doTeacherStudentsPage(request, session)) {
                            getServletContext().getRequestDispatcher(TEACHER_STUDENTS_PAGE).forward(request, response);
                        } else {
                            doMainTeacherPage(request, session, teacherId, subjectId);
                            getServletContext().getRequestDispatcher(MAIN_TEACHER_PAGE).forward(request, response);
                        }
                    }
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case PAGE_COMMAND: {
                if (serviceManager.getUserService().isAccountExists(teacherId)) {
                    int pageId = (int) session.getAttribute(PARAM_PAGE_ID);
                    if (doTeacherGradesPage(request, session, subjectId, pageId)) {
                        getServletContext().getRequestDispatcher(TEACHER_GRADES_PAGE).forward(request, response);
                    } else {
                        if (doTeacherStudentsPage(request, session)) {
                            getServletContext().getRequestDispatcher(TEACHER_STUDENTS_PAGE).forward(request, response);
                        } else {
                            doMainTeacherPage(request, session, teacherId, subjectId);
                            getServletContext().getRequestDispatcher(MAIN_TEACHER_PAGE).forward(request, response);
                        }
                    }
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
            }
            case PROFILE_COMMAND: {
                if (doProfilePage(request, teacherId)) {
                    getServletContext().getRequestDispatcher(USER_PROFILE_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case TASKS_COMMAND: {
                if (doTeacherTasksPage(request, (Long) session.getAttribute(PARAM_STUDENT_ID), subjectId)) {
                    getServletContext().getRequestDispatcher(TEACHER_TASKS_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
        }
    }

    private boolean doTeacherStudentsPage(HttpServletRequest request, HttpSession session) {
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        List<User> students = serviceManager.getUserService().getClassStudents(classId, DEFAULT_PAGE);
        List<Integer> pages = serviceManager.getUserService().getClassStudentTablePages(classId);
        request.setAttribute(PARAM_STUDENTS, students);
        request.setAttribute(PARAM_PAGES, pages);
        return true;
    }

    private boolean doTeacherGradesPage(HttpServletRequest request, HttpSession session,
                                        Long subjectId, int page) {
        User student = serviceManager.getUserService().getUser((Long) session.getAttribute(PARAM_STUDENT_ID));
        if (student == null) return false;
        request.setAttribute(PARAM_STUDENT_LOGIN, student.getLogin());
        request.setAttribute(PARAM_STUDENT_NAME, student.getUserInfo().getFirstName());
        request.setAttribute(PARAM_STUDENT_SURNAME, student.getUserInfo().getLastName());
        request.setAttribute(PARAM_STUDENT_MAIL, student.getUserInfo().getMail());
        List<Grade> grades = serviceManager
                .getGradeService()
                .getGradesByUserIdAndSubjectId(student.getId(),
                        subjectId,
                        session.getAttribute(PARAM_LOCALE).equals(RU_LOCALE));
        request.setAttribute(PARAM_PAGES, serviceManager.getGradeService().getGradeTablePages(grades));
        request.setAttribute(PARAM_GRADES,
                serviceManager.getGradeService().getCurrentPageGrades(grades, page));
        request.setAttribute(PARAM_DATES, serviceManager.getGradeService().getGradeNearbyDates());
        return true;
    }

    private void doMainTeacherPage(HttpServletRequest request, HttpSession session, Long teacherId, Long subjectId) {
        session.removeAttribute(PARAM_CLASS_ID);
        request.setAttribute(PARAM_CLASSES,
                serviceManager.getStudyClassService()
                        .getClassesByTeacherIdAndSubjectId(teacherId, subjectId));
    }

    private boolean doProfilePage(HttpServletRequest request, Long teacherId) {
        User teacher = serviceManager.getUserService().getUser(teacherId);
        if (teacher == null) return false;
        request.setAttribute(ServletParameter.PARAM_NAME, teacher.getUserInfo().getFirstName());
        request.setAttribute(ServletParameter.PARAM_SURNAME, teacher.getUserInfo().getLastName());
        request.setAttribute(ServletParameter.PARAM_EMAIL, teacher.getUserInfo().getMail());
        return true;
    }

    private boolean doTeacherTasksPage(HttpServletRequest request, Long studentId, Long subjectId) {
        if (!serviceManager.getUserService().isAccountExists(studentId)) return false;
        request.setAttribute(PARAM_TASKS, serviceManager
                .getTaskService()
                .getTasksByStudentId(studentId, subjectId, DEFAULT_PAGE));
        request.setAttribute(PARAM_PAGES, serviceManager
                .getTaskService()
                .getStudentTaskTablePages(studentId, subjectId));
        request.setAttribute(PARAM_SUBJECT, serviceManager.getTaskService().getRuSubjectName(subjectId));
        return true;
    }

    private void addGrade(Long studentId, int mark, LocalDate date, Long subjectId) {
        serviceManager.getGradeService().addGrade(studentId, mark, date, subjectId);
    }

    private void deleteGrade(Long gradeId) {
        serviceManager.getGradeService().deleteGrade(gradeId);
    }
}
