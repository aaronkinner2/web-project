package com.org.iisg.web.servlet;

import com.org.iisg.model.Grade;
import com.org.iisg.model.StudyClassData;
import com.org.iisg.model.User;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "MainStudent", urlPatterns = "/mainStudent")
public class MainStudentServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        Long studentId = (Long) session.getAttribute(PARAM_ID);
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case PROFILE_COMMAND: {
                if (doProfilePage(request, studentId)) {
                    getServletContext().getRequestDispatcher(USER_PROFILE_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case CHANGE_COMMAND: {
                if (doMainStudentPage(request, studentId, Long.parseLong(request.getParameter(PARAM_SUBJECT_ID)),
                        session, DEFAULT_PAGE)) {
                    getServletContext().getRequestDispatcher(MAIN_STUDENT_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case PAGE_COMMAND: {
                int page = Integer.parseInt(request.getParameter(PARAM_PAGE_ID));
                if (doMainStudentPage(request, studentId, (Long) (session.getAttribute(PARAM_SUBJECT_ID)),
                        session, page)) {
                    getServletContext().getRequestDispatcher(MAIN_STUDENT_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case TASKS_COMMAND: {
                if (doStudentTasks(request, session, studentId,
                        Long.parseLong(request.getParameter(PARAM_SUBJECT_ID)))) {
                    getServletContext().getRequestDispatcher(STUDENT_TASKS_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
        }
    }

    private boolean doProfilePage(HttpServletRequest request, Long userId) {
        User user = serviceManager.getUserService().getUser(userId);
        if (user == null) return false;
        request.setAttribute(PARAM_NAME, user.getUserInfo().getFirstName());
        request.setAttribute(PARAM_SURNAME, user.getUserInfo().getLastName());
        request.setAttribute(PARAM_EMAIL, user.getUserInfo().getMail());
        return true;
    }

    private boolean doMainStudentPage(HttpServletRequest request, Long studentId, Long subjectId,
                                      HttpSession session, int page) {
        if (!serviceManager.getUserService().isAccountExists(studentId)) return false;
        List<Grade> grades = serviceManager.getGradeService().getGradesByUserId(studentId,
                session.getAttribute(PARAM_LOCALE).equals(RU_LOCALE));
        List<Double> subjectMarks = serviceManager.getGradeService().getAverageMark(grades);
        for (int i = 0; i <= SUBJECT_COUNT; i++) request.setAttribute(MARK_PARAMS.get(i), subjectMarks.get(i));
        if (!subjectId.equals(ALL_SUBJECTS_ID)) {
            grades = grades.stream().filter(g -> g.getSubject().getId().equals(subjectId)).collect(Collectors.toList());
        }
        request.setAttribute(PARAM_PAGES, serviceManager.getGradeService().getGradeTablePages(grades));
        request.setAttribute(PARAM_GRADES, serviceManager.getGradeService().getCurrentPageGrades(grades, page));
        StudyClassData classData = serviceManager.getStudyClassService().getClassDataByStudentId(studentId);
        if (classData == null) return false;
        for (int i = 0; i < SUBJECT_COUNT; i++) {
            if (classData.getTeachers().get(i) == null) {
                request.setAttribute(TEACHER_NAME_SURNAME_PARAMS.get(i), null);
                request.setAttribute(TEACHER_MAIL_PARAMS.get(i), null);
            } else {
                request.setAttribute(TEACHER_NAME_SURNAME_PARAMS.get(i),
                        classData.getTeachers().get(i).getUserInfo().getCutFullName());
                request.setAttribute(TEACHER_MAIL_PARAMS.get(i),
                        classData.getTeachers().get(i).getUserInfo().getMail());
            }
        }
        request.setAttribute(PARAM_STUDY_CLASS_NAME, classData.getName());
        session.setAttribute(PARAM_SUBJECT_ID, subjectId);
        return true;
    }

    private boolean doStudentTasks(HttpServletRequest request, HttpSession session, Long studentId, Long subjectId) {
        if (!serviceManager.getUserService().isAccountExists(studentId)) return false;
        request.setAttribute(PARAM_TASKS, serviceManager
                .getTaskService()
                .getTasksByStudentId(studentId, subjectId, DEFAULT_PAGE));
        request.setAttribute(PARAM_PAGES, serviceManager
                .getTaskService()
                .getStudentTaskTablePages(studentId, subjectId));
        session.setAttribute(PARAM_SUBJECT_ID, subjectId);
        request.setAttribute(PARAM_SUBJECT, serviceManager.getTaskService().getRuSubjectName(subjectId));
        return true;
    }
}
