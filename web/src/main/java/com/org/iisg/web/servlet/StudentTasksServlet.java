package com.org.iisg.web.servlet;

import com.org.iisg.model.Grade;
import com.org.iisg.model.StudyClassData;
import com.org.iisg.model.User;
import com.org.iisg.db.parameter.ServletParameter;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "StudentTasks", urlPatterns = "/studentTasks")
public class StudentTasksServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        Long studentId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case MAIN_COMMAND: {
                if (doMainStudentPage(request, studentId, session)) {
                    getServletContext().getRequestDispatcher(MAIN_STUDENT_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case CHANGE_COMMAND: {
                String taskParam = request.getParameter(PARAM_TASK_ID);
                if (taskParam != null) markTaskAsCompleted(Long.parseLong(taskParam));
                if (doStudentTasksPage(request, studentId, subjectId, DEFAULT_PAGE)) {
                    getServletContext().getRequestDispatcher(STUDENT_TASKS_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case PAGE_COMMAND: {
                int page = Integer.parseInt(request.getParameter(PARAM_PAGE_ID));
                if (doStudentTasksPage(request, studentId, subjectId, page)) {
                    getServletContext().getRequestDispatcher(STUDENT_TASKS_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case SUBJECT_COMMAND: {
                session.setAttribute(PARAM_SUBJECT_ID, Long.parseLong(request.getParameter(PARAM_SUBJECT_ID)));
                if (doStudentTasksPage(request, studentId, Long.parseLong(request.getParameter(PARAM_SUBJECT_ID)), DEFAULT_PAGE)) {
                    getServletContext().getRequestDispatcher(STUDENT_TASKS_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case PROFILE_COMMAND: {
                if (doProfilePage(request, session, studentId)) {
                    getServletContext().getRequestDispatcher(USER_PROFILE_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
        }
    }

    private boolean doStudentTasksPage(HttpServletRequest request, Long studentId, Long subjectId, int page) {
        if (!serviceManager.getUserService().isAccountExists(studentId)) return false;
        request.setAttribute(PARAM_TASKS, serviceManager
                .getTaskService()
                .getTasksByStudentId(studentId, subjectId, page));
        request.setAttribute(PARAM_PAGES, serviceManager
                .getTaskService()
                .getStudentTaskTablePages(studentId, subjectId));
        request.setAttribute(PARAM_SUBJECT, serviceManager.getTaskService().getRuSubjectName(subjectId));
        return true;
    }

    private boolean doProfilePage(HttpServletRequest request, HttpSession session, Long teacherId) {
        User teacher = serviceManager.getUserService().getUser(teacherId);
        if (teacher == null) return false;
        request.setAttribute(ServletParameter.PARAM_NAME, teacher.getUserInfo().getFirstName());
        request.setAttribute(ServletParameter.PARAM_SURNAME, teacher.getUserInfo().getLastName());
        request.setAttribute(ServletParameter.PARAM_EMAIL, teacher.getUserInfo().getMail());
        session.removeAttribute(PARAM_SUBJECT_ID);
        return true;
    }

    private boolean doMainStudentPage(HttpServletRequest request, Long studentId, HttpSession session) {
        if (!serviceManager.getUserService().isAccountExists(studentId)) return false;
        List<Grade> grades = serviceManager.getGradeService().getGradesByUserId(studentId,
                session.getAttribute(PARAM_LOCALE).equals(RU_LOCALE));
        List<Double> subjectMarks = serviceManager.getGradeService().getAverageMark(grades);
        for (int i = 0; i <= SUBJECT_COUNT; i++) {
            request.setAttribute(MARK_PARAMS.get(i), subjectMarks.get(i));
        }
        StudyClassData classData = serviceManager.getStudyClassService().getClassDataByStudentId(studentId);
        if (classData == null) return false;
        for (int i = 0; i < SUBJECT_COUNT; i++) {
            if (classData.getTeachers().get(i) == null) {
                request.setAttribute(TEACHER_NAME_SURNAME_PARAMS.get(i), null);
                request.setAttribute(TEACHER_MAIL_PARAMS.get(i), null);
            } else {
                request.setAttribute(TEACHER_NAME_SURNAME_PARAMS.get(i),
                        classData.getTeachers().get(i).getUserInfo().getCutFullName());
                request.setAttribute(TEACHER_MAIL_PARAMS.get(i),
                        classData.getTeachers().get(i).getUserInfo().getMail());
            }
        }
        request.setAttribute(PARAM_STUDY_CLASS_NAME, classData.getName());
        request.setAttribute(PARAM_GRADES, serviceManager.getGradeService().getCurrentPageGrades(grades, DEFAULT_PAGE));
        request.setAttribute(PARAM_PAGES, serviceManager.getGradeService().getGradeTablePages(grades));
        session.setAttribute(PARAM_SUBJECT_ID, ALL_SUBJECTS_ID);
        return true;
    }

    private void markTaskAsCompleted(Long taskId) {
        serviceManager.getTaskService().deleteTask(taskId);
    }
}
