package com.org.iisg.web.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

import static com.org.iisg.db.parameter.ServletParameter.PARAM_LOGIN;
import static com.org.iisg.db.parameter.ServletParameter.PARAM_PASSWORD;

@WebFilter(urlPatterns = "/signIn")
public class SignInFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        String login = servletRequest.getParameter(PARAM_LOGIN);
        String password = servletRequest.getParameter(PARAM_PASSWORD);
        if (login != null & password != null) {
            login = login.trim();
            password = password.trim();
            servletRequest.setAttribute(PARAM_LOGIN, login);
            servletRequest.setAttribute(PARAM_PASSWORD, password);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
