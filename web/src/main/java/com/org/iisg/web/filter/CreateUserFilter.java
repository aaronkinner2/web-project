package com.org.iisg.web.filter;

import com.org.iisg.db.parameter.ServletParameter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(urlPatterns = {"/createStudentAccount", "/createTeacherAccount"})
public class CreateUserFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        String login = servletRequest.getParameter(ServletParameter.PARAM_LOGIN);
        String name = servletRequest.getParameter(ServletParameter.PARAM_NAME);
        String surname = servletRequest.getParameter(ServletParameter.PARAM_SURNAME);
        String email = servletRequest.getParameter(ServletParameter.PARAM_EMAIL);
        String password = servletRequest.getParameter(ServletParameter.PARAM_PASSWORD);
        if (login != null & password != null & name != null & surname != null & email != null) {
            login = login.trim();
            password = password.trim();
            name = name.trim();
            surname = surname.trim();
            email = email.trim();
            servletRequest.setAttribute(ServletParameter.PARAM_LOGIN, login);
            servletRequest.setAttribute(ServletParameter.PARAM_PASSWORD, password);
            servletRequest.setAttribute(ServletParameter.PARAM_NAME, name);
            servletRequest.setAttribute(ServletParameter.PARAM_SURNAME, surname);
            servletRequest.setAttribute(ServletParameter.PARAM_EMAIL, email);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
