package com.org.iisg.web.servlet;

import com.org.iisg.db.parameter.ServletParameter;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "AdminTeachers", urlPatterns = "/adminTeachers")
public class AdminTeachersServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(ServletParameter.PARAM_COMMAND);
        HttpSession session = request.getSession();
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case DELETE_COMMAND: {
                String teacherParam = request.getParameter(PARAM_TEACHER_ID_PARAM);
                if (teacherParam != null) deleteTeacher(Long.parseLong(teacherParam));
                doAdminTeachersPage(request, (String) session.getAttribute(ServletParameter.PARAM_LOCALE), DEFAULT_PAGE);
                getServletContext().getRequestDispatcher(ADMIN_TEACHERS_PAGE).forward(request, response);
                break;
            }
            case ADD_COMMAND: {
                getServletContext().getRequestDispatcher(CREATE_TEACHER_ACCOUNT_PAGE).forward(request, response);
                break;
            }
            case MAIN_COMMAND: {
                getServletContext().getRequestDispatcher(MAIN_ADMIN_PAGE).forward(request, response);
                break;
            }
            case PAGE_COMMAND: {
                int page = Integer.parseInt(request.getParameter(PARAM_PAGE_ID));
                doAdminTeachersPage(request, (String) session.getAttribute(ServletParameter.PARAM_LOCALE), page);
                getServletContext().getRequestDispatcher(ADMIN_TEACHERS_PAGE).forward(request, response);
                break;
            }
        }
    }

    private void doAdminTeachersPage(HttpServletRequest request, String locale, int page) {
        request.setAttribute(PARAM_PAGES, serviceManager.getUserService().getTeacherTablePages());
        request.setAttribute(PARAM_TEACHERS,
                serviceManager.getUserService().getTeachers(locale.equals(RU_LOCALE), page));
    }

    private void deleteTeacher(Long teacherId) {
        serviceManager.getUserService().deleteUser(teacherId);
    }
}
