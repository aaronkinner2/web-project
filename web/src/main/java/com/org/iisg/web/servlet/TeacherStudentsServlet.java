package com.org.iisg.web.servlet;

import com.org.iisg.model.Grade;
import com.org.iisg.model.User;
import com.org.iisg.db.parameter.ServletParameter;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "TeacherStudents", urlPatterns = "/teacherStudents")
public class TeacherStudentsServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        Long teacherId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case PROFILE_COMMAND: {
                if (doProfilePage(request, teacherId)) {
                    getServletContext().getRequestDispatcher(USER_PROFILE_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case MAIN_COMMAND: {
                if (doMainTeacherPage(request, session, teacherId, subjectId)) {
                    getServletContext().getRequestDispatcher(MAIN_TEACHER_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case PAGE_COMMAND: {
                if (serviceManager.getUserService().isAccountExists(teacherId)) {
                    Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
                    int page = Integer.parseInt(request.getParameter(PARAM_PAGE_ID));
                    doTeacherStudentsPage(request, classId, page);
                    getServletContext().getRequestDispatcher(TEACHER_STUDENTS_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
            }
            case GET_COMMAND: {
                Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
                String studentParam = request.getParameter(PARAM_STUDENT_ID);
                if (serviceManager.getUserService().isAccountExists(teacherId)) {
                    if (studentParam == null) {
                        doTeacherStudentsPage(request, classId, DEFAULT_PAGE);
                        getServletContext().getRequestDispatcher(TEACHER_STUDENTS_PAGE).forward(request, response);
                    } else {
                        if (doTeacherGradesPage(request, session, Long.parseLong(studentParam), subjectId)) {
                            getServletContext().getRequestDispatcher(TEACHER_GRADES_PAGE).forward(request, response);
                        } else {
                            doTeacherStudentsPage(request, classId, DEFAULT_PAGE);
                            getServletContext().getRequestDispatcher(TEACHER_STUDENTS_PAGE).forward(request, response);
                        }
                    }
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
        }
    }

    private boolean doTeacherGradesPage(HttpServletRequest request, HttpSession session,
                                        Long studentId, Long subjectId) {
        User student = serviceManager.getUserService().getUser(studentId);
        if (student == null) return false;
        session.setAttribute(PARAM_STUDENT_ID, student.getId());
        request.setAttribute(PARAM_STUDENT_LOGIN, student.getLogin());
        request.setAttribute(PARAM_STUDENT_NAME, student.getUserInfo().getFirstName());
        request.setAttribute(PARAM_STUDENT_SURNAME, student.getUserInfo().getLastName());
        request.setAttribute(PARAM_STUDENT_MAIL, student.getUserInfo().getMail());
        List<Grade> grades = serviceManager
                .getGradeService()
                .getGradesByUserIdAndSubjectId(student.getId(),
                        subjectId,
                        session.getAttribute(PARAM_LOCALE).equals(RU_LOCALE));
        request.setAttribute(PARAM_PAGES, serviceManager.getGradeService().getGradeTablePages(grades));
        request.setAttribute(PARAM_GRADES, serviceManager.getGradeService().getCurrentPageGrades(grades, DEFAULT_PAGE));
        request.setAttribute(PARAM_DATES, serviceManager.getGradeService().getGradeNearbyDates());
        return true;
    }

    private boolean doProfilePage(HttpServletRequest request, Long teacherId) {
        User teacher = serviceManager.getUserService().getUser(teacherId);
        if (teacher == null) return false;
        request.setAttribute(ServletParameter.PARAM_NAME, teacher.getUserInfo().getFirstName());
        request.setAttribute(ServletParameter.PARAM_SURNAME, teacher.getUserInfo().getLastName());
        request.setAttribute(ServletParameter.PARAM_EMAIL, teacher.getUserInfo().getMail());
        return true;
    }

    private void doTeacherStudentsPage(HttpServletRequest request, Long classId, int page) {
        List<User> students = serviceManager.getUserService().getClassStudents(classId, page);
        List<Integer> pages = serviceManager.getUserService().getClassStudentTablePages(classId);
        request.setAttribute(PARAM_STUDENTS, students);
        request.setAttribute(PARAM_PAGES, pages);
    }

    private boolean doMainTeacherPage(HttpServletRequest request, HttpSession session, Long teacherId, Long subjectId) {
        User teacher = serviceManager.getUserService().getUser(teacherId);
        if (teacher == null) return false;
        session.removeAttribute(PARAM_CLASS_ID);
        request.setAttribute(PARAM_CLASSES,
                serviceManager.getStudyClassService().getClassesByTeacherIdAndSubjectId(teacherId, subjectId));
        return true;
    }
}
