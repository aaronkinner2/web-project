package com.org.iisg.web.filter;

import com.org.iisg.db.parameter.ServletParameter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(urlPatterns = "/userProfile")
public class UserProfileFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        String oldPassword = servletRequest.getParameter(ServletParameter.PARAM_OLD_PASSWORD);
        String newPassword = servletRequest.getParameter(ServletParameter.PARAM_NEW_PASSWORD);
        String newEmail = servletRequest.getParameter(ServletParameter.PARAM_NEW_MAIL);
        if (oldPassword != null & newPassword != null) {
            oldPassword = oldPassword.trim();
            newPassword = newPassword.trim();
            servletRequest.setAttribute(ServletParameter.PARAM_OLD_PASSWORD, oldPassword);
            servletRequest.setAttribute(ServletParameter.PARAM_NEW_PASSWORD, newPassword);
        }
        if (newEmail != null) {
            newEmail = newEmail.trim();
            servletRequest.setAttribute(ServletParameter.PARAM_NEW_MAIL, newEmail);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
