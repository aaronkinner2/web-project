package com.org.iisg.web.servlet;

import com.org.iisg.service.impl.manager.ServletServiceManager;
import com.org.iisg.service.mail.MailService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.org.iisg.db.parameter.CommandParameter.RETURN_COMMAND;
import static com.org.iisg.db.parameter.CommandParameter.SIGN_UP_COMMAND;
import static com.org.iisg.db.parameter.ErrorMessages.ERROR_USER_WITH_LOGIN_EXISTS_MESSAGE_EN;
import static com.org.iisg.db.parameter.ErrorMessages.ERROR_USER_WITH_LOGIN_EXISTS_MESSAGE_RU;
import static com.org.iisg.db.parameter.MailMessages.*;
import static com.org.iisg.db.parameter.PageParameter.CREATE_STUDENT_ACCOUNT_PAGE;
import static com.org.iisg.db.parameter.PageParameter.SIGN_IN_PAGE;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "CreateStudentAccount", urlPatterns = "/createStudentAccount")
public class CreateStudentAccountServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    private final MailService mailService = MailService.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        final HttpSession session = request.getSession();
        if (com.equals(SIGN_UP_COMMAND)) {
            String login = request.getParameter(PARAM_LOGIN);
            String name = request.getParameter(PARAM_NAME);
            String surname = request.getParameter(PARAM_SURNAME);
            String mail = request.getParameter(PARAM_EMAIL);
            String password = request.getParameter(PARAM_PASSWORD);
            if (serviceManager.getUserService().createStudent(login, password, name, surname, mail)) {
                sendMailForStudent(login, mail);
                response.sendRedirect(contextPath + SIGN_IN_PAGE);
            } else {
                doSignUpPage(request, session, login, name, surname, mail);
                getServletContext().getRequestDispatcher(CREATE_STUDENT_ACCOUNT_PAGE).forward(request, response);
            }
        } else if (com.equals(RETURN_COMMAND)) {
            response.sendRedirect(contextPath + SIGN_IN_PAGE);
        }
    }

    private void doSignUpPage(HttpServletRequest request, HttpSession session, String login,
                              String name, String surname, String mail) {
        if (session.getAttribute(PARAM_LOCALE).equals(EN_LOCALE)) {
            request.setAttribute(PARAM_ERROR, ERROR_USER_WITH_LOGIN_EXISTS_MESSAGE_EN);
        } else {
            request.setAttribute(PARAM_ERROR, ERROR_USER_WITH_LOGIN_EXISTS_MESSAGE_RU);
        }
        request.setAttribute(PARAM_LOGIN, login);
        request.setAttribute(PARAM_NAME, name);
        request.setAttribute(PARAM_SURNAME, surname);
        request.setAttribute(PARAM_EMAIL, mail);
    }

    private void sendMailForStudent(String login, String mail) {
        mailService.sendRegMessage(mail, SUCCESSFUL_SIGN_UP, START_TEXT + login + SUCCESSFUL_SIGN_UP_TEXT);
    }
}
