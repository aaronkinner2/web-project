package com.org.iisg.web.servlet;

import com.org.iisg.model.User;
import com.org.iisg.db.parameter.ErrorMessages;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "CreateClass", urlPatterns = "/createClass")
public class CreateClassServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        final HttpSession session = request.getSession();
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case CREATE_COMMAND: {
                if (createStudyClass(request.getParameter(PARAM_CLASS_NAME),
                        Arrays.asList(Long.parseLong(request.getParameter(PARAM_MATH_TEACHER_ID)),
                        Long.parseLong(request.getParameter(PARAM_INFORM_TEACHER_ID)),
                        Long.parseLong(request.getParameter(PARAM_CHEM_TEACHER_ID)),
                        Long.parseLong(request.getParameter(PARAM_PHY_TEACHER_ID)),
                        Long.parseLong(request.getParameter(PARAM_RU_TEACHER_ID)),
                        Long.parseLong(request.getParameter(PARAM_EN_TEACHER_ID))))) {
                    doAdminClasses(request);
                    getServletContext().getRequestDispatcher(ADMIN_CLASSES_PAGE).forward(request, response);
                } else {
                    doCreateClassPage(request, session);
                    getServletContext().getRequestDispatcher(CREATE_CLASS_PAGE).forward(request, response);
                }
                break;
            }
            case MAIN_COMMAND: {
                doAdminClasses(request);
                getServletContext().getRequestDispatcher(ADMIN_CLASSES_PAGE).forward(request, response);
                break;
            }
        }
    }

    private void doAdminClasses(HttpServletRequest request) {
        request.setAttribute(PARAM_CLASSES, serviceManager.getStudyClassService().getAll());
    }

    private boolean createStudyClass(String name, List<Long> ids) {
        for (int i = 0; i < ids.size(); i++) if (ids.get(i).equals(ERROR_ID)) ids.set(i, null);
        return serviceManager.getStudyClassService().createStudyClass(name, ids);
    }

    private void doCreateClassPage(HttpServletRequest request, HttpSession session) {
        String locale = (String) session.getAttribute(PARAM_LOCALE);
        if (locale.equals(RU_LOCALE)) {
            request.setAttribute(PARAM_ERROR, ErrorMessages.ERROR_CREATE_CLASS_RU);
        } else {
            request.setAttribute(PARAM_ERROR, ErrorMessages.ERROR_CREATE_CLASS_EN);
        }
        List<List<User>> teachers = serviceManager.getUserService()
                .getTeachersBySubject(locale.equals(RU_LOCALE));
        if (teachers == null) {
            for (int i = 0; i < SUBJECT_COUNT; i++) request.setAttribute(TEACHER_LISTS_PARAMS.get(i), null);
        } else {
            for (int i = 0; i < SUBJECT_COUNT; i++) request.setAttribute(TEACHER_LISTS_PARAMS.get(i), teachers.get(i));
        }
    }
}
