package com.org.iisg.web.servlet;

import com.org.iisg.model.Grade;
import com.org.iisg.model.User;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "AdminGrades", urlPatterns = "/adminGrades")
public class AdminGradesServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        switch (com) {
            case SIGN_OUT_COMMAND:
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            case SUBJECT_COMMAND: {
                Long subjectId = Long.parseLong(request.getParameter(PARAM_SUBJECT_ID));
                session.setAttribute(PARAM_SUBJECT_ID, subjectId);
                if (doAdminGradesPage(request, session, (Long) session.getAttribute(PARAM_STUDENT_ID),
                        subjectId, DEFAULT_PAGE)) {
                    getServletContext().getRequestDispatcher(ADMIN_GRADES_PAGE).forward(request, response);
                } else {
                    doAdminStudentsPage(request, session, classId);
                    getServletContext().getRequestDispatcher(ADMIN_STUDENTS_PAGE).forward(request, response);
                }
                break;
            }
            case MAIN_COMMAND: {
                if (doStudentProfilePage(request, session, (Long) session.getAttribute(PARAM_STUDENT_ID))) {
                    getServletContext()
                            .getRequestDispatcher(ADMIN_STUDENT_PROFILE_PAGE).forward(request, response);
                } else {
                    doAdminStudentsPage(request, session, classId);
                    getServletContext().getRequestDispatcher(ADMIN_STUDENTS_PAGE).forward(request, response);
                }
                break;
            }
            case PAGE_COMMAND: {
                int page = Integer.parseInt(request.getParameter(PARAM_PAGE_ID));
                session.setAttribute(PARAM_PAGE_ID, page);
                if (doAdminGradesPage(request, session, (Long) session.getAttribute(PARAM_STUDENT_ID),
                        (Long) session.getAttribute(PARAM_SUBJECT_ID), page)) {
                    getServletContext().getRequestDispatcher(ADMIN_GRADES_PAGE).forward(request, response);
                } else {
                    doAdminStudentsPage(request, session, classId);
                    getServletContext().getRequestDispatcher(ADMIN_STUDENTS_PAGE).forward(request, response);
                }
                break;
            }
            case DELETE_COMMAND: {
                String gradeParameter = request.getParameter(PARAM_GRADE_ID);
                if (gradeParameter != null) deleteGrade(Long.parseLong(gradeParameter));
                if (doAdminGradesPage(request, session, (Long) session.getAttribute(PARAM_STUDENT_ID),
                        (Long) session.getAttribute(PARAM_SUBJECT_ID), (int) session.getAttribute(PARAM_PAGE_ID))) {
                    getServletContext().getRequestDispatcher(ADMIN_GRADES_PAGE).forward(request, response);
                } else {
                    doAdminStudentsPage(request, session, classId);
                    getServletContext().getRequestDispatcher(ADMIN_STUDENTS_PAGE).forward(request, response);
                }
            }
        }
    }

    private boolean doStudentProfilePage(HttpServletRequest request, HttpSession session, Long studentId) {
        User student = serviceManager.getUserService().getUser(studentId);
        if (student == null) {
            return false;
        } else {
            session.setAttribute(PARAM_STUDENT_ID, student.getId());
            request.setAttribute(PARAM_STUDENT_LOGIN, student.getLogin());
            request.setAttribute(PARAM_STUDENT_NAME, student.getUserInfo().getFirstName());
            request.setAttribute(PARAM_STUDENT_SURNAME, student.getUserInfo().getLastName());
            request.setAttribute(PARAM_STUDENT_MAIL, student.getUserInfo().getMail());
            if (student.getStudyClass() == null) {
                request.setAttribute(PARAM_STUDENT_CLASS, null);
            } else {
                request.setAttribute(PARAM_STUDENT_CLASS, student.getStudyClass().getName());
            }
            request.setAttribute(PARAM_CLASSES, serviceManager.getStudyClassService().getAll());
            return true;
        }
    }

    private void doAdminStudentsPage(HttpServletRequest request, HttpSession session, Long classId) {
        session.removeAttribute(PARAM_SUBJECT_ID);
        session.removeAttribute(PARAM_PAGE_ID);
        if (classId.equals(NO_CLASS_ID)) {
            request.setAttribute(PARAM_PAGES, serviceManager.getUserService().getNoClassStudentTablePages());
            request.setAttribute(PARAM_STUDENTS, serviceManager.getUserService().getNoClassStudents(DEFAULT_PAGE));
        } else {
            request.setAttribute(PARAM_PAGES, serviceManager.getUserService().getClassStudentTablePages(classId));
            request.setAttribute(PARAM_STUDENTS,
                    serviceManager.getUserService().getClassStudents(classId, DEFAULT_PAGE));
        }
    }

    private boolean doAdminGradesPage(HttpServletRequest request, HttpSession session,
                                      Long studentId, Long subjectId, int page) {
        if (!serviceManager.getUserService().isAccountExists(studentId)) return false;
        List<Grade> grades = serviceManager.getGradeService().getGradesByUserId(studentId,
                session.getAttribute(PARAM_LOCALE).equals(RU_LOCALE));
        List<Double> subjectMarks = serviceManager.getGradeService().getAverageMark(grades);
        for (int i = 0; i <= SUBJECT_COUNT; i++) request.setAttribute(MARK_PARAMS.get(i), subjectMarks.get(i));
        if (!subjectId.equals(ALL_SUBJECTS_ID)) {
            grades = grades.stream().filter(g -> g.getSubject().getId().equals(subjectId)).collect(Collectors.toList());
        }
        request.setAttribute(PARAM_PAGES, serviceManager.getGradeService().getGradeTablePages(grades));
        request.setAttribute(PARAM_GRADES, serviceManager.getGradeService().getCurrentPageGrades(grades, page));
        return true;
    }

    private void deleteGrade(Long gradeId) {
        serviceManager.getGradeService().deleteGrade(gradeId);
    }
}
