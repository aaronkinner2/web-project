package com.org.iisg.web.filter;

import com.org.iisg.db.parameter.ServletParameter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(urlPatterns = "/createClass")
public class CreateClassFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        String className = servletRequest.getParameter(ServletParameter.PARAM_STUDY_CLASS_NAME);
        if (className != null) {
            className = className.trim();
            servletRequest.setAttribute(ServletParameter.PARAM_STUDY_CLASS_NAME, className);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
