package com.org.iisg.web.servlet;

import com.org.iisg.service.impl.manager.ServletServiceManager;
import com.org.iisg.service.mail.MailService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.ErrorMessages.ERROR_USER_WITH_LOGIN_EXISTS_MESSAGE_EN;
import static com.org.iisg.db.parameter.ErrorMessages.ERROR_USER_WITH_LOGIN_EXISTS_MESSAGE_RU;
import static com.org.iisg.db.parameter.MailMessages.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "CreateTeacherAccount", urlPatterns = "/createTeacherAccount")
public class CreateTeacherAccountServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    private final MailService mailService = MailService.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        final HttpSession session = request.getSession();
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case MAIN_COMMAND: {
                doAdminTeachersPage(request, (String) session.getAttribute(PARAM_LOCALE));
                getServletContext().getRequestDispatcher(ADMIN_TEACHERS_PAGE).forward(request, response);
                break;
            }
            case SIGN_UP_COMMAND: {
                String login = request.getParameter(PARAM_LOGIN);
                String name = request.getParameter(PARAM_NAME);
                String surname = request.getParameter(PARAM_SURNAME);
                String mail = request.getParameter(PARAM_EMAIL);
                Long subjectId = Long.parseLong(request.getParameter(PARAM_SUBJECT));
                String password = request.getParameter(PARAM_PASSWORD);
                if (serviceManager.getUserService().createTeacher(login, password, name, surname, mail, subjectId)) {
                    sendMailForTeacher(login, password, mail);
                    doAdminTeachersPage(request, (String) session.getAttribute(PARAM_LOCALE));
                    getServletContext().getRequestDispatcher(ADMIN_TEACHERS_PAGE).forward(request, response);
                } else {
                    doAddTeacherPage(request, session, login, name, surname, mail);
                    getServletContext().getRequestDispatcher(CREATE_TEACHER_ACCOUNT_PAGE).forward(request, response);
                }
                break;
            }
        }
    }

    private void doAddTeacherPage(HttpServletRequest request, HttpSession session, String login,
                                  String name, String surname, String email) {
        if (session.getAttribute(PARAM_LOCALE).equals(EN_LOCALE)) {
            request.setAttribute(PARAM_ERROR, ERROR_USER_WITH_LOGIN_EXISTS_MESSAGE_EN);
        } else {
            request.setAttribute(PARAM_ERROR, ERROR_USER_WITH_LOGIN_EXISTS_MESSAGE_RU);
        }
        request.setAttribute(PARAM_LOGIN, login);
        request.setAttribute(PARAM_NAME, name);
        request.setAttribute(PARAM_SURNAME, surname);
        request.setAttribute(PARAM_EMAIL, email);
    }

    private void doAdminTeachersPage(HttpServletRequest request, String locale) {
        request.setAttribute(PARAM_PAGES, serviceManager.getUserService().getTeacherTablePages());
        request.setAttribute(PARAM_TEACHERS,
                serviceManager.getUserService().getTeachers(locale.equals(RU_LOCALE), DEFAULT_PAGE));
    }

    private void sendMailForTeacher(String login, String password, String email) {
        mailService.sendRegMessage(email, SUCCESSFUL_SIGN_UP,
                SUCCESSFUL_SIGN_UP_TEXT_1 + login + SUCCESSFUL_SIGN_UP_TEXT_2 +
                        password + SUCCESSFUL_SIGN_UP_TEXT_3);
    }
}
