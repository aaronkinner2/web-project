package com.org.iisg.web.servlet;

import com.org.iisg.model.User;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "MainTeacher", urlPatterns = "/mainTeacher")
public class MainTeacherServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        Long teacherId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case PROFILE_COMMAND: {
                if (doProfilePage(request, teacherId)) {
                    getServletContext().getRequestDispatcher(USER_PROFILE_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case GET_COMMAND: {
                String classParam = request.getParameter(PARAM_CLASS_ID);
                if (classParam == null) {
                    doMainTeacherPage(request, session, teacherId, subjectId);
                    getServletContext().getRequestDispatcher(MAIN_TEACHER_PAGE).forward(request, response);
                } else {
                    if (doTeacherStudentsPage(request, session, teacherId, Long.parseLong(classParam))) {
                        getServletContext().getRequestDispatcher(TEACHER_STUDENTS_PAGE).forward(request, response);
                    } else {
                        getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                    }
                }
                break;
            }
        }
    }

    private boolean doTeacherStudentsPage(HttpServletRequest request, HttpSession session,
                                          Long teacherId, Long classId) {
        if (!serviceManager.getUserService().isAccountExists(teacherId)) return false;
        List<User> students = serviceManager.getUserService().getClassStudents(classId, DEFAULT_PAGE);
        List<Integer> pages = serviceManager.getUserService().getClassStudentTablePages(classId);
        request.setAttribute(PARAM_STUDENTS, students);
        request.setAttribute(PARAM_PAGES, pages);
        session.setAttribute(PARAM_CLASS_ID, classId);
        return true;
    }

    private boolean doProfilePage(HttpServletRequest request, Long teacherId) {
        User teacher = serviceManager.getUserService().getUser(teacherId);
        if (teacher == null) return false;
        request.setAttribute(PARAM_NAME, teacher.getUserInfo().getFirstName());
        request.setAttribute(PARAM_SURNAME, teacher.getUserInfo().getLastName());
        request.setAttribute(PARAM_EMAIL, teacher.getUserInfo().getMail());
        return true;
    }

    private void doMainTeacherPage(HttpServletRequest request, HttpSession session, Long teacherId, Long subjectId) {
        session.setAttribute(PARAM_SUBJECT_ID, subjectId);
        request.setAttribute(PARAM_CLASSES,
                serviceManager.getStudyClassService()
                        .getClassesByTeacherIdAndSubjectId(teacherId, subjectId));
    }
}
