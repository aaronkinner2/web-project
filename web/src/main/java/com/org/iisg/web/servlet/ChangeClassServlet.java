package com.org.iisg.web.servlet;

import com.org.iisg.model.StudyClassData;
import com.org.iisg.model.User;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "ChangeClass", urlPatterns = "/changeClass")
public class ChangeClassServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case CHANGE_COMMAND: {
                if (changeClass(classId, Arrays.asList(Long.parseLong(request.getParameter(PARAM_MATH_TEACHER_ID)),
                        Long.parseLong(request.getParameter(PARAM_INFORM_TEACHER_ID)),
                        Long.parseLong(request.getParameter(PARAM_CHEM_TEACHER_ID)),
                        Long.parseLong(request.getParameter(PARAM_PHY_TEACHER_ID)),
                        Long.parseLong(request.getParameter(PARAM_RU_TEACHER_ID)),
                        Long.parseLong(request.getParameter(PARAM_EN_TEACHER_ID))))) {
                    if (doAdminClassPage(request, classId)) {
                        getServletContext().getRequestDispatcher(ADMIN_CLASS_PAGE).forward(request, response);
                    } else {
                        doAdminClassesPage(request, session);
                        getServletContext().getRequestDispatcher(ADMIN_CLASSES_PAGE).forward(request, response);
                    }
                } else {
                    doAdminClassesPage(request, session);
                    getServletContext().getRequestDispatcher(ADMIN_CLASSES_PAGE).forward(request, response);
                }
                break;
            }
            case MAIN_COMMAND: {
                if (doAdminClassPage(request, classId)) {
                    getServletContext().getRequestDispatcher(ADMIN_CLASS_PAGE).forward(request, response);
                } else {
                    doAdminClassesPage(request, session);
                    getServletContext().getRequestDispatcher(ADMIN_CLASSES_PAGE).forward(request, response);
                }
                break;
            }
        }
    }

    private boolean doAdminClassPage(HttpServletRequest request, Long classId) {
        StudyClassData studyClassData = serviceManager.getStudyClassService().getClassDataByClassId(classId);
        if (studyClassData == null) return false;
        request.setAttribute(PARAM_CLASS_NAME, studyClassData.getName());
        for (int i = START_ARRAY_COUNT; i < SUBJECT_COUNT; i++) {
            User teacher = studyClassData.getTeachers().get(i);
            if (teacher == null) {
                request.setAttribute(TEACHER_NAME_PARAMS.get(i), null);
                request.setAttribute(TEACHER_SURNAME_PARAMS.get(i), null);
                request.setAttribute(TEACHER_MAIL_PARAMS.get(i), null);
            } else {
                request.setAttribute(TEACHER_NAME_PARAMS.get(i), teacher.getUserInfo().getFirstName());
                request.setAttribute(TEACHER_SURNAME_PARAMS.get(i), teacher.getUserInfo().getLastName());
                request.setAttribute(TEACHER_MAIL_PARAMS.get(i), teacher.getUserInfo().getMail());
            }
        }
        return true;
    }

    private void doAdminClassesPage(HttpServletRequest request, HttpSession session) {
        request.setAttribute(PARAM_CLASSES, serviceManager.getStudyClassService().getAll());
        session.removeAttribute(PARAM_CLASS_ID);
    }

    private boolean changeClass(Long classId, List<Long> teacherIds) {
        return serviceManager.getStudyClassService().changeClass(classId, teacherIds);
    }
}
