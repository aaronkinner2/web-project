package com.org.iisg.web.servlet;

import com.org.iisg.model.StudyClassData;
import com.org.iisg.model.User;
import com.org.iisg.db.parameter.ServletParameter;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "AdminStudents", urlPatterns = "/adminStudents")
public class AdminStudentsServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(ServletParameter.PARAM_COMMAND);
        HttpSession session = request.getSession();
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        switch (com) {
            case SIGN_OUT_COMMAND:
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            case GET_COMMAND: {
                String studentParam = request.getParameter(PARAM_STUDENT_ID);
                if (studentParam == null) {
                    doAdminStudentsPage(request, classId, DEFAULT_PAGE);
                    getServletContext().getRequestDispatcher(ADMIN_STUDENTS_PAGE).forward(request, response);
                } else {
                    if (doStudentProfilePage(request, session,
                            serviceManager.getUserService().getUser(Long.parseLong(studentParam)))) {
                        getServletContext().getRequestDispatcher(ADMIN_STUDENT_PROFILE_PAGE).forward(request, response);
                    } else {
                        doAdminStudentsPage(request, classId, DEFAULT_PAGE);
                        getServletContext().getRequestDispatcher(ADMIN_STUDENTS_PAGE).forward(request, response);
                    }
                }
                break;
            }
            case MAIN_COMMAND: {
                if (classId.equals(NO_CLASS_ID)) {
                    getServletContext().getRequestDispatcher(MAIN_ADMIN_PAGE).forward(request, response);
                } else {
                    if (doAdminClassPage(request, session, classId)) {
                        getServletContext().getRequestDispatcher(ADMIN_CLASS_PAGE).forward(request, response);
                    } else {
                        doAdminClassesPage(request, session);
                        getServletContext().getRequestDispatcher(ADMIN_CLASSES_PAGE).forward(request, response);
                    }
                }
                break;
            }
            case PAGE_COMMAND: {
                int page = Integer.parseInt(request.getParameter(PARAM_PAGE_ID));
                doAdminStudentsPage(request, classId, page);
                getServletContext().getRequestDispatcher(ADMIN_STUDENTS_PAGE).forward(request, response);
                break;
            }
        }
    }

    private boolean doStudentProfilePage(HttpServletRequest request, HttpSession session, User student) {
        if (student == null) {
            return false;
        } else {
            session.setAttribute(PARAM_STUDENT_ID, student.getId());
            request.setAttribute(PARAM_STUDENT_LOGIN, student.getLogin());
            request.setAttribute(PARAM_STUDENT_NAME, student.getUserInfo().getFirstName());
            request.setAttribute(PARAM_STUDENT_SURNAME, student.getUserInfo().getLastName());
            request.setAttribute(PARAM_STUDENT_MAIL, student.getUserInfo().getMail());
            if (student.getStudyClass() == null) {
                request.setAttribute(PARAM_STUDENT_CLASS, null);
            } else {
                request.setAttribute(PARAM_STUDENT_CLASS, student.getStudyClass().getName());
            }
            request.setAttribute(PARAM_CLASSES, serviceManager.getStudyClassService().getAll());
            return true;
        }
    }

    private void doAdminStudentsPage(HttpServletRequest request, Long classId, int page) {
        if (classId.equals(NO_CLASS_ID)) {
            request.setAttribute(PARAM_PAGES, serviceManager.getUserService().getNoClassStudentTablePages());
            request.setAttribute(PARAM_STUDENTS, serviceManager.getUserService().getNoClassStudents(page));
        } else {
            request.setAttribute(PARAM_PAGES, serviceManager.getUserService().getClassStudentTablePages(classId));
            request.setAttribute(PARAM_STUDENTS, serviceManager.getUserService().getClassStudents(classId, page));
        }
    }

    private boolean doAdminClassPage(HttpServletRequest request, HttpSession session, Long classId) {
        StudyClassData studyClassData = serviceManager.getStudyClassService().getClassDataByClassId(classId);
        if (studyClassData == null) return false;
        request.setAttribute(PARAM_CLASS_NAME, studyClassData.getName());
        for (int i = START_ARRAY_COUNT; i < SUBJECT_COUNT; i++) {
            User teacher = studyClassData.getTeachers().get(i);
            if (teacher == null) {
                request.setAttribute(TEACHER_NAME_PARAMS.get(i), null);
                request.setAttribute(TEACHER_SURNAME_PARAMS.get(i), null);
                request.setAttribute(TEACHER_MAIL_PARAMS.get(i), null);
            } else {
                request.setAttribute(TEACHER_NAME_PARAMS.get(i), teacher.getUserInfo().getFirstName());
                request.setAttribute(TEACHER_SURNAME_PARAMS.get(i), teacher.getUserInfo().getLastName());
                request.setAttribute(TEACHER_MAIL_PARAMS.get(i), teacher.getUserInfo().getMail());
            }
        }
        session.setAttribute(PARAM_CLASS_ID, classId);
        return true;
    }

    private void doAdminClassesPage(HttpServletRequest request, HttpSession session) {
        request.setAttribute(PARAM_CLASSES, serviceManager.getStudyClassService().getAll());
        session.removeAttribute(PARAM_CLASS_ID);
    }
}
