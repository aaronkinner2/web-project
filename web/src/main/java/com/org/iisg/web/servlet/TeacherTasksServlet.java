package com.org.iisg.web.servlet;

import com.org.iisg.model.Grade;
import com.org.iisg.model.User;
import com.org.iisg.db.parameter.ServletParameter;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "TeacherTasks", urlPatterns = "/teacherTasks")
public class TeacherTasksServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        Long teacherId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case MAIN_COMMAND: {
                if (serviceManager.getUserService().isAccountExists(teacherId)) {
                    Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
                    if (doTeacherGradesPage(request, session, studentId, subjectId)) {
                        getServletContext().getRequestDispatcher(TEACHER_GRADES_PAGE).forward(request, response);
                    } else {
                        doTeacherStudentsPage(request, session);
                        getServletContext().getRequestDispatcher(TEACHER_STUDENTS_PAGE).forward(request, response);
                    }
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case DELETE_COMMAND: {
                String taskParam = request.getParameter(PARAM_TASK_ID);
                if (taskParam != null) deleteTask(Long.parseLong(taskParam));
                Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
                if (doTeacherTasksPage(request, teacherId, studentId, subjectId, DEFAULT_PAGE)) {
                    getServletContext().getRequestDispatcher(TEACHER_TASKS_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case PAGE_COMMAND: {
                int page = Integer.parseInt(request.getParameter(PARAM_PAGE_ID));
                Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
                if (doTeacherTasksPage(request, teacherId, studentId, subjectId, page)) {
                    getServletContext().getRequestDispatcher(TEACHER_TASKS_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case CREATE_COMMAND: {
                if (serviceManager.getUserService().isAccountExists(teacherId)) {
                    Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
                    if (doCreateTaskPage(request, studentId)) {
                        getServletContext().getRequestDispatcher(CREATE_TASK_PAGE).forward(request, response);
                    } else {
                        doTeacherStudentsPage(request, session);
                        getServletContext().getRequestDispatcher(TEACHER_STUDENTS_PAGE).forward(request, response);
                    }
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case PROFILE_COMMAND: {
                if (doProfilePage(request, teacherId)) {
                    getServletContext().getRequestDispatcher(USER_PROFILE_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
        }
    }

    private boolean doTeacherTasksPage(HttpServletRequest request, Long teacherId,
                                       Long studentId, Long subjectId, int page) {
        if (!serviceManager.getUserService().isAccountExists(teacherId)) return false;
        request.setAttribute(PARAM_TASKS,
                serviceManager.getTaskService().getTasksByStudentId(studentId, subjectId, page));
        request.setAttribute(PARAM_PAGES,
                serviceManager.getTaskService().getStudentTaskTablePages(studentId, subjectId));
        request.setAttribute(PARAM_SUBJECT, serviceManager.getTaskService().getRuSubjectName(subjectId));
        return true;
    }

    private boolean doCreateTaskPage(HttpServletRequest request, Long studentId) {
        User student = serviceManager.getUserService().getUser(studentId);
        if (student == null) return false;
        request.setAttribute(PARAM_STUDENT_NAME, student.getUserInfo().getCutFullName());
        return true;
    }

    private boolean doProfilePage(HttpServletRequest request, Long teacherId) {
        User teacher = serviceManager.getUserService().getUser(teacherId);
        if (teacher == null) return false;
        request.setAttribute(ServletParameter.PARAM_NAME, teacher.getUserInfo().getFirstName());
        request.setAttribute(ServletParameter.PARAM_SURNAME, teacher.getUserInfo().getLastName());
        request.setAttribute(ServletParameter.PARAM_EMAIL, teacher.getUserInfo().getMail());
        return true;
    }

    private boolean doTeacherGradesPage(HttpServletRequest request, HttpSession session,
                                        Long studentId, Long subjectId) {
        User student = serviceManager.getUserService().getUser(studentId);
        if (student == null) return false;
        request.setAttribute(PARAM_STUDENT_LOGIN, student.getLogin());
        request.setAttribute(PARAM_STUDENT_NAME, student.getUserInfo().getFirstName());
        request.setAttribute(PARAM_STUDENT_SURNAME, student.getUserInfo().getLastName());
        request.setAttribute(PARAM_STUDENT_MAIL, student.getUserInfo().getMail());
        List<Grade> grades = serviceManager
                .getGradeService()
                .getGradesByUserIdAndSubjectId(student.getId(),
                        subjectId,
                        session.getAttribute(PARAM_LOCALE).equals(RU_LOCALE));
        request.setAttribute(PARAM_PAGES, serviceManager.getGradeService().getGradeTablePages(grades));
        request.setAttribute(PARAM_GRADES, serviceManager.getGradeService()
                .getCurrentPageGrades(grades, DEFAULT_PAGE));
        request.setAttribute(PARAM_DATES, serviceManager.getGradeService().getGradeNearbyDates());
        return true;
    }

    private void doTeacherStudentsPage(HttpServletRequest request, HttpSession session) {
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        List<User> students = serviceManager.getUserService().getClassStudents(classId, DEFAULT_PAGE);
        List<Integer> pages = serviceManager.getUserService().getClassStudentTablePages(classId);
        request.setAttribute(PARAM_STUDENTS, students);
        request.setAttribute(PARAM_PAGES, pages);
    }

    private void deleteTask(Long taskId) {
        serviceManager.getTaskService().deleteTask(taskId);
    }
}
