package com.org.iisg.web.servlet;

import com.org.iisg.model.User;
import com.org.iisg.db.parameter.ServletParameter;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "CreateTask", urlPatterns = "/createTask")
public class CreateTaskServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        Long teacherId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case MAIN_COMMAND: {
                if (serviceManager.getUserService().isAccountExists(teacherId)) {
                    Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
                    if (doTeacherTasksPage(request, studentId, subjectId)) {
                        getServletContext().getRequestDispatcher(TEACHER_TASKS_PAGE).forward(request, response);
                    } else {
                        doTeacherStudentsPage(request, session);
                        getServletContext().getRequestDispatcher(TEACHER_STUDENTS_PAGE).forward(request, response);
                    }
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case CREATE_COMMAND: {
                if (serviceManager.getUserService().isAccountExists(teacherId)) {
                    String description = request.getParameter(PARAM_DESCRIPTION);
                    Long studentId = Long.parseLong(request.getParameter(PARAM_STUDENT_ID));
                    createTask(session, description, studentId, subjectId);
                    if (doTeacherTasksPage(request, studentId, subjectId)) {
                        getServletContext().getRequestDispatcher(TEACHER_TASKS_PAGE).forward(request, response);
                    } else {
                        doTeacherStudentsPage(request, session);
                        getServletContext().getRequestDispatcher(TEACHER_STUDENTS_PAGE).forward(request, response);
                    }
                } else {
                    doTeacherStudentsPage(request, session);
                    getServletContext().getRequestDispatcher(TEACHER_STUDENTS_PAGE).forward(request, response);
                }
                break;
            }
            case PROFILE_COMMAND: {
                if (doProfilePage(request, teacherId)) {
                    getServletContext().getRequestDispatcher(USER_PROFILE_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
        }
    }

    private boolean doTeacherTasksPage(HttpServletRequest request, Long studentId, Long subjectId) {
        if (!serviceManager.getUserService().isAccountExists(studentId)) return false;
        request.setAttribute(PARAM_TASKS,
                serviceManager.getTaskService().getTasksByStudentId(studentId, subjectId, DEFAULT_PAGE));
        request.setAttribute(PARAM_PAGES,
                serviceManager.getTaskService().getStudentTaskTablePages(studentId, subjectId));
        request.setAttribute(PARAM_SUBJECT, serviceManager.getTaskService().getRuSubjectName(subjectId));
        return true;
    }

    private boolean doProfilePage(HttpServletRequest request, Long teacherId) {
        User teacher = serviceManager.getUserService().getUser(teacherId);
        if (teacher == null) return false;
        request.setAttribute(ServletParameter.PARAM_NAME, teacher.getUserInfo().getFirstName());
        request.setAttribute(ServletParameter.PARAM_SURNAME, teacher.getUserInfo().getLastName());
        request.setAttribute(ServletParameter.PARAM_EMAIL, teacher.getUserInfo().getMail());
        return true;
    }

    private void doTeacherStudentsPage(HttpServletRequest request, HttpSession session) {
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        List<User> students = serviceManager.getUserService().getClassStudents(classId, DEFAULT_PAGE);
        List<Integer> pages = serviceManager.getUserService().getClassStudentTablePages(classId);
        request.setAttribute(PARAM_STUDENTS, students);
        request.setAttribute(PARAM_PAGES, pages);
    }

    private void createTask(HttpSession session, String description, Long studentId, Long subjectId) {
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        if (studentId.equals(ALL_STUDENTS_ID)) {
            serviceManager.getTaskService().create(description, studentId, subjectId);
        } else {
            serviceManager.getTaskService().createForClass(description, classId, subjectId);
        }
    }
}
