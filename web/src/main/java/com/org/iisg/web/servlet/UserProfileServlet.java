package com.org.iisg.web.servlet;

import com.org.iisg.model.Grade;
import com.org.iisg.model.StudyClassData;
import com.org.iisg.model.User;
import com.org.iisg.service.impl.manager.ServletServiceManager;
import com.org.iisg.service.mail.MailService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.ErrorMessages.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "UserProfile", urlPatterns = "/userProfile")
public class UserProfileServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    private final MailService mailService = MailService.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        String login = (String) session.getAttribute(PARAM_LOGIN);
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long roleId = (Long) session.getAttribute(PARAM_ROLE);
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case MAIN_COMMAND: {
                if (roleId.equals(STUDENT_ID)) {
                    if (doMainStudentPage(request, session, userId)) {
                        getServletContext().getRequestDispatcher(MAIN_STUDENT_PAGE).forward(request, response);
                    } else {
                        getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                    }
                } else if (roleId.equals(TEACHER_ID)) {
                    Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
                    if (doMainTeacherPage(request, userId, subjectId)) {
                        getServletContext().getRequestDispatcher(MAIN_TEACHER_PAGE).forward(request, response);
                    } else {
                        getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                    }
                }
                break;
            }
            case CHANGE_PASSWORD_COMMAND: {
                String oldPassword = request.getParameter(PARAM_OLD_PASSWORD);
                String newPassword = request.getParameter(PARAM_NEW_PASSWORD);
                if (serviceManager.getUserService().checkEqualityOfPasswords(oldPassword, newPassword)) {
                    setErrorEqualPasswords(request, session);
                } else if (serviceManager.getUserService().changePassword(userId, oldPassword, newPassword)) {
                    mailService.sendPasswordChangeMessage(login,
                            serviceManager.getUserService().getUser(userId).getUserInfo().getMail());
                } else {
                    setErrorIncorrectPasswords(request, session);
                }
                if (doProfilePage(request, userId)) {
                    getServletContext().getRequestDispatcher(USER_PROFILE_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
            case CHANGE_EMAIL_COMMAND: {
                String newMail = request.getParameter(PARAM_NEW_MAIL);
                serviceManager.getUserService().changeMail(userId, newMail);
                mailService.sendEmailChangeMessage(login, newMail);
                if (doProfilePage(request, userId)) {
                    getServletContext().getRequestDispatcher(USER_PROFILE_PAGE).forward(request, response);
                } else {
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                }
                break;
            }
        }
    }

    private boolean doProfilePage(HttpServletRequest request, Long userId) {
        User user = serviceManager.getUserService().getUser(userId);
        if (user == null) return false;
        request.setAttribute(PARAM_NAME, user.getUserInfo().getFirstName());
        request.setAttribute(PARAM_SURNAME, user.getUserInfo().getLastName());
        request.setAttribute(PARAM_EMAIL, user.getUserInfo().getMail());
        return true;
    }

    private boolean doMainStudentPage(HttpServletRequest request, HttpSession session, Long studentId) {
        if (!serviceManager.getUserService().isAccountExists(studentId)) return false;
        List<Grade> grades = serviceManager.getGradeService().getGradesByUserId(studentId,
                session.getAttribute(PARAM_LOCALE).equals(RU_LOCALE));
        List<Double> subjectMarks = serviceManager.getGradeService().getAverageMark(grades);
        for (int i = 0; i <= SUBJECT_COUNT; i++) {
            request.setAttribute(MARK_PARAMS.get(i), subjectMarks.get(i));
        }
        StudyClassData classData = serviceManager.getStudyClassService().getClassDataByStudentId(studentId);
        if (classData == null) return false;
        for (int i = 0; i < SUBJECT_COUNT; i++) {
            if (classData.getTeachers().get(i) == null) {
                request.setAttribute(TEACHER_NAME_SURNAME_PARAMS.get(i), null);
                request.setAttribute(TEACHER_MAIL_PARAMS.get(i), null);
            } else {
                request.setAttribute(TEACHER_NAME_SURNAME_PARAMS.get(i),
                        classData.getTeachers().get(i).getUserInfo().getCutFullName());
                request.setAttribute(TEACHER_MAIL_PARAMS.get(i),
                        classData.getTeachers().get(i).getUserInfo().getMail());
            }
        }
        request.setAttribute(PARAM_STUDY_CLASS_NAME, classData.getName());
        request.setAttribute(PARAM_GRADES, serviceManager.getGradeService().getCurrentPageGrades(grades, DEFAULT_PAGE));
        request.setAttribute(PARAM_PAGES, serviceManager.getGradeService().getGradeTablePages(grades));
        session.setAttribute(PARAM_SUBJECT_ID, ALL_SUBJECTS_ID);
        return true;
    }

    private boolean doMainTeacherPage(HttpServletRequest request, Long teacherId, Long subjectId) {
        if (!serviceManager.getUserService().isAccountExists(teacherId)) return false;
        request.setAttribute(PARAM_CLASSES,
                serviceManager.getStudyClassService()
                        .getClassesByTeacherIdAndSubjectId(teacherId, subjectId));
        return true;
    }

    private void setErrorIncorrectPasswords(HttpServletRequest request, HttpSession session) {
        if (session.getAttribute(PARAM_LOCALE).equals(EN_LOCALE)) {
            request.setAttribute(PARAM_ERROR, ERROR_CHANGE_PASSWORD_MESSAGE_INCORRECT_INPUT_EN);
        } else {
            request.setAttribute(PARAM_ERROR, ERROR_CHANGE_PASSWORD_MESSAGE_INCORRECT_INPUT_RU);
        }
    }

    private void setErrorEqualPasswords(HttpServletRequest request, HttpSession session) {
        if (session.getAttribute(PARAM_LOCALE).equals(EN_LOCALE)) {
            request.setAttribute(PARAM_ERROR, ERROR_CHANGE_PASSWORD_MESSAGE_EQUAL_INPUT_EN);
        } else {
            request.setAttribute(PARAM_ERROR, ERROR_CHANGE_PASSWORD_MESSAGE_EQUAL_INPUT_RU);
        }
    }
}
