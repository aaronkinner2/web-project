package com.org.iisg.web.servlet;

import com.org.iisg.model.Grade;
import com.org.iisg.model.Role;
import com.org.iisg.model.StudyClassData;
import com.org.iisg.model.User;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.org.iisg.db.parameter.CommandParameter.CREATE_COMMAND;
import static com.org.iisg.db.parameter.CommandParameter.SIGN_IN_COMMAND;
import static com.org.iisg.db.parameter.ErrorMessages.ERROR_SIGN_IN_MESSAGE_EN;
import static com.org.iisg.db.parameter.ErrorMessages.ERROR_SIGN_IN_MESSAGE_RU;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "SignIn", urlPatterns = "/signIn")
public class SignInServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        switch (com) {
            case CREATE_COMMAND: {
                response.sendRedirect(contextPath + CREATE_STUDENT_ACCOUNT_PAGE);
                break;
            }
            case SIGN_IN_COMMAND: {
                String login = request.getParameter(PARAM_LOGIN);
                String password = request.getParameter(PARAM_PASSWORD);
                final User user = serviceManager.getUserService().authorize(login, password);
                if (user == null) {
                    doSignInPage(request, session, login);
                    getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                } else {
                    final Role role = user.getRole();
                    if (role.getId().equals(ADMIN_ID)) {
                        setParamsForUserSession(session, user);
                        getServletContext().getRequestDispatcher(MAIN_ADMIN_PAGE).forward(request, response);
                    } else {
                        setParamsForUserSession(session, user);
                        if (role.getId().equals(STUDENT_ID)) {
                            if (doMainStudentPage(request, session, user.getId())) {
                                getServletContext().getRequestDispatcher(MAIN_STUDENT_PAGE).forward(request, response);
                            } else {
                                getServletContext().getRequestDispatcher(SIGN_IN_PAGE).forward(request, response);
                            }
                        } else if (role.getId().equals(TEACHER_ID)) {
                            doMainTeacherPage(request, session, user);
                            getServletContext().getRequestDispatcher(MAIN_TEACHER_PAGE).forward(request, response);
                        }
                    }
                }
                break;
            }
        }
    }

    private boolean doMainStudentPage(HttpServletRequest request, HttpSession session, Long studentId) {
        List<Grade> grades = serviceManager.getGradeService().getGradesByUserId(studentId,
                session.getAttribute(PARAM_LOCALE).equals(RU_LOCALE));
        List<Double> subjectMarks = serviceManager.getGradeService().getAverageMark(grades);
        for (int i = 0; i <= SUBJECT_COUNT; i++) request.setAttribute(MARK_PARAMS.get(i), subjectMarks.get(i));
        StudyClassData classData = serviceManager.getStudyClassService().getClassDataByStudentId(studentId);
        if (classData == null) return false;
        for (int i = 0; i < SUBJECT_COUNT; i++) {
            if (classData.getTeachers().get(i) == null) {
                request.setAttribute(TEACHER_NAME_SURNAME_PARAMS.get(i), null);
                request.setAttribute(TEACHER_MAIL_PARAMS.get(i), null);
            } else {
                request.setAttribute(TEACHER_NAME_SURNAME_PARAMS.get(i),
                        classData.getTeachers().get(i).getUserInfo().getCutFullName());
                request.setAttribute(TEACHER_MAIL_PARAMS.get(i),
                        classData.getTeachers().get(i).getUserInfo().getMail());
            }
        }
        request.setAttribute(PARAM_STUDY_CLASS_NAME, classData.getName());
        request.setAttribute(PARAM_GRADES, serviceManager.getGradeService().getCurrentPageGrades(grades, DEFAULT_PAGE));
        request.setAttribute(PARAM_PAGES, serviceManager.getGradeService().getGradeTablePages(grades));
        session.setAttribute(PARAM_SUBJECT_ID, ALL_SUBJECTS_ID);
        return true;
    }

    private void doMainTeacherPage(HttpServletRequest request, HttpSession session, User teacher) {
        session.setAttribute(PARAM_SUBJECT_ID, teacher.getSubject().getId());
        request.setAttribute(PARAM_CLASSES,
                serviceManager.getStudyClassService()
                        .getClassesByTeacherIdAndSubjectId(teacher.getId(), teacher.getSubject().getId()));
    }

    private void doSignInPage(HttpServletRequest request, HttpSession session, String login) {
        request.setAttribute(PARAM_NO_CORRECT_LOGIN, login);
        if (session.getAttribute(PARAM_LOCALE).equals(EN_LOCALE)) {
            request.setAttribute(PARAM_ERROR, ERROR_SIGN_IN_MESSAGE_EN);
        } else {
            request.setAttribute(PARAM_ERROR, ERROR_SIGN_IN_MESSAGE_RU);
        }
    }

    private void setParamsForUserSession(HttpSession session, User user) {
        session.setAttribute(PARAM_ID, user.getId());
        session.setAttribute(PARAM_LOGIN, user.getLogin());
        session.setAttribute(PARAM_ROLE, user.getRole().getId());
    }
}
