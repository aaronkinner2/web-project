package com.org.iisg.web.servlet;

import com.org.iisg.model.StudyClassData;
import com.org.iisg.model.User;
import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "AdminClasses", urlPatterns = "/adminClasses")
public class AdminClassesServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case GET_COMMAND: {
                if (doAdminClassPage(request, session)) {
                    getServletContext().getRequestDispatcher(ADMIN_CLASS_PAGE).forward(request, response);
                } else {
                    doAdminClasses(request);
                    getServletContext().getRequestDispatcher(ADMIN_CLASSES_PAGE).forward(request, response);
                }
                break;
            }
            case CREATE_COMMAND: {
                doCreateClassPage(request, session);
                getServletContext().getRequestDispatcher(CREATE_CLASS_PAGE).forward(request, response);
                break;
            }
            case BREAK_COMMAND: {
                doAdminClasses(request);
                getServletContext().getRequestDispatcher(ADMIN_CLASSES_PAGE).forward(request, response);
                break;
            }
        }
    }

    private boolean doAdminClassPage(HttpServletRequest request, HttpSession session) {
        String classParam = request.getParameter(PARAM_CLASS_ID);
        if (classParam == null) return false;
        Long classId = Long.parseLong(classParam);
        StudyClassData studyClassData = serviceManager.getStudyClassService().getClassDataByClassId(classId);
        if (studyClassData == null) return false;
        request.setAttribute(PARAM_CLASS_NAME, studyClassData.getName());
        for (int i = START_ARRAY_COUNT; i < SUBJECT_COUNT; i++) {
            User teacher = studyClassData.getTeachers().get(i);
            if (teacher == null) {
                request.setAttribute(TEACHER_NAME_PARAMS.get(i), null);
                request.setAttribute(TEACHER_SURNAME_PARAMS.get(i), null);
                request.setAttribute(TEACHER_MAIL_PARAMS.get(i), null);
            } else {
                request.setAttribute(TEACHER_NAME_PARAMS.get(i), teacher.getUserInfo().getFirstName());
                request.setAttribute(TEACHER_SURNAME_PARAMS.get(i), teacher.getUserInfo().getLastName());
                request.setAttribute(TEACHER_MAIL_PARAMS.get(i), teacher.getUserInfo().getMail());
            }
        }
        session.setAttribute(PARAM_CLASS_ID, classId);
        return true;
    }

    private void doAdminClasses(HttpServletRequest request) {
        request.setAttribute(PARAM_CLASSES, serviceManager.getStudyClassService().getAll());
    }

    private void doCreateClassPage(HttpServletRequest request, HttpSession session) {
        String locale = (String) session.getAttribute(PARAM_LOCALE);
        List<List<User>> teachers = serviceManager.getUserService()
                .getTeachersBySubject(locale.equals(RU_LOCALE));
        if (teachers == null) {
            for (int i = START_ARRAY_COUNT; i < SUBJECT_COUNT; i++)
                request.setAttribute(TEACHER_LISTS_PARAMS.get(i), null);
        } else {
            for (int i = START_ARRAY_COUNT; i < SUBJECT_COUNT; i++)
                request.setAttribute(TEACHER_LISTS_PARAMS.get(i), teachers.get(i));
        }
    }
}
