package com.org.iisg.web.servlet;

import com.org.iisg.service.impl.manager.ServletServiceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.*;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "MainAdmin", urlPatterns = "/mainAdmin")
public class MainAdminServlet extends HttpServlet {

    private final ServletServiceManager serviceManager = ServletServiceManager.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        switch (com) {
            case SIGN_OUT_COMMAND: {
                session.invalidate();
                response.sendRedirect(contextPath + INDEX_PAGE);
                break;
            }
            case GET_TEACHERS_COMMAND: {
                doAdminTeachersPage(request, (String) session.getAttribute(PARAM_LOCALE));
                getServletContext().getRequestDispatcher(ADMIN_TEACHERS_PAGE).forward(request, response);
                break;
            }
            case GET_NO_CLASS_STUDENTS_COMMAND: {
                doAdminStudentsPage(request, session);
                getServletContext().getRequestDispatcher(ADMIN_STUDENTS_PAGE).forward(request, response);
                break;
            }
            case GET_CLASSES_COMMAND: {
                doAdminClasses(request);
                getServletContext().getRequestDispatcher(ADMIN_CLASSES_PAGE).forward(request, response);
                break;
            }
        }
    }

    private void doAdminStudentsPage(HttpServletRequest request, HttpSession session) {
        request.setAttribute(PARAM_PAGES, serviceManager.getUserService().getNoClassStudentTablePages());
        request.setAttribute(PARAM_STUDENTS, serviceManager.getUserService().getNoClassStudents(DEFAULT_PAGE));
        session.setAttribute(PARAM_CLASS_ID, NO_CLASS_ID);
    }

    private void doAdminTeachersPage(HttpServletRequest request, String locale) {
        request.setAttribute(PARAM_PAGES, serviceManager.getUserService().getTeacherTablePages());
        request.setAttribute(PARAM_TEACHERS,
                serviceManager.getUserService().getTeachers(locale.equals(RU_LOCALE), DEFAULT_PAGE));
    }

    private void doAdminClasses(HttpServletRequest request) {
        request.setAttribute(PARAM_CLASSES, serviceManager.getStudyClassService().getAll());
    }
}
