package com.org.iisg.web.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.org.iisg.db.parameter.CommandParameter.*;
import static com.org.iisg.db.parameter.PageParameter.SIGN_IN_PAGE;
import static com.org.iisg.db.parameter.ServletParameter.*;

@WebServlet(name = "Index", urlPatterns = "/index")
public class IndexServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String contextPath = request.getContextPath();
        String com = request.getParameter(PARAM_COMMAND);
        HttpSession session = request.getSession();
        if (com.equals(RU_START_COMMAND)) {
            session.setAttribute(PARAM_LOCALE, RU_LOCALE);
            response.sendRedirect(contextPath + SIGN_IN_PAGE);
        } else if (com.equals(EN_START_COMMAND)) {
            session.setAttribute(PARAM_LOCALE, EN_LOCALE);
            response.sendRedirect(contextPath + SIGN_IN_PAGE);
        }
    }
}
