package com.org.iisg.db.parameter;

public interface PageParameter {

    String SIGN_IN_PAGE = "/signIn.jsp";

    String USER_PROFILE_PAGE = "/userProfile.jsp";

    String INDEX_PAGE = "/index.jsp";

    String MAIN_STUDENT_PAGE = "/mainStudent.jsp";

    String MAIN_TEACHER_PAGE = "/mainTeacher.jsp";

    String MAIN_ADMIN_PAGE = "/mainAdmin.jsp";

    String TEACHER_STUDENTS_PAGE = "/teacherStudents.jsp";

    String TEACHER_GRADES_PAGE = "/teacherGrades.jsp";

    String TEACHER_TASKS_PAGE = "/teacherTasks.jsp";

    String ADMIN_CLASS_PAGE = "/adminClass.jsp";

    String ADMIN_CLASSES_PAGE = "/adminClasses.jsp";

    String ADMIN_GRADES_PAGE = "/adminGrades.jsp";

    String ADMIN_STUDENT_PROFILE_PAGE = "/adminStudentProfile.jsp";

    String ADMIN_STUDENTS_PAGE = "/adminStudents.jsp";

    String ADMIN_TEACHERS_PAGE = "/adminTeachers.jsp";

    String CHANGE_CLASS_PAGE = "/changeClass.jsp";

    String CREATE_CLASS_PAGE = "/createClass.jsp";

    String CREATE_STUDENT_ACCOUNT_PAGE = "/createStudentAccount.jsp";

    String CREATE_TASK_PAGE = "/createTask.jsp";

    String CREATE_TEACHER_ACCOUNT_PAGE = "/createTeacherAccount.jsp";

    String STUDENT_TASKS_PAGE = "/studentTasks.jsp";
}
