package com.org.iisg.db.parameter;

import java.util.Arrays;
import java.util.List;

public interface ServletParameter {

    String PARAM_LOGIN = "login";

    String PARAM_ID = "id";

    String PARAM_ROLE = "role";

    String PARAM_PASSWORD = "password";

    String PARAM_EMAIL = "email";

    String PARAM_NAME = "name";

    String PARAM_SURNAME = "surname";

    String PARAM_OLD_PASSWORD = "oldPassword";

    String PARAM_NEW_PASSWORD = "newPassword";

    String PARAM_NEW_MAIL = "newEmail";

    String PARAM_SUBJECT = "subject";

    String PARAM_MARK = "mark";

    String PARAM_DATE = "date";

    String PARAM_STUDY_CLASS_NAME = "className";

    String PARAM_TASK_ID = "taskId";

    String PARAM_GRADE_ID = "gradeId";

    String PARAM_CLASS_ID = "classId";

    String PARAM_PAGE_ID = "pageId";

    String PARAM_SUBJECT_ID = "subjectId";

    String PARAM_STUDENT_ID = "studentId";

    String PARAM_TEACHER_ID_PARAM = "teacherId";

    String PARAM_MATH_TEACHER_ID = "mathTeacherId";

    String PARAM_INFORM_TEACHER_ID = "informTeacherId";

    String PARAM_CHEM_TEACHER_ID = "chemTeacherId";

    String PARAM_PHY_TEACHER_ID = "phyTeacherId";

    String PARAM_RU_TEACHER_ID = "ruTeacherId";

    String PARAM_EN_TEACHER_ID = "enTeacherId";

    String PARAM_COMMAND = "command";

    String PARAM_NO_CORRECT_LOGIN = "loginError";

    String PARAM_LOCALE = "locale";

    List<String> MARK_PARAMS = Arrays.asList("commonAvMark", "mathAvMark",
            "informAvMark", "chemAvMark", "phyAvMark", "rusAvMark", "enAvMark", "gradeList");

    List<String> TEACHER_NAME_PARAMS = Arrays.asList("tMathName", "tInformName",
            "tChemName", "tPhyName", "tRuName", "tEnName");

    List<String> TEACHER_SURNAME_PARAMS = Arrays.asList("tMathSurname", "tInformSurname",
            "tChemSurname", "tPhySurname", "tRuSurname", "tEnSurname");

    List<String> TEACHER_NAME_SURNAME_PARAMS = Arrays.asList("tMathNameSurname",
            "tInformNameSurname", "tChemNameSurname", "tPhyNameSurname", "tRuNameSurname", "tEnNameSurname");

    List<String> TEACHER_MAIL_PARAMS = Arrays.asList("tMathMail", "tInformMail", "tChemMail",
            "tPhyMail", "tRuMail", "tEnMail");

    List<String> DEFAULT_CLASS_TEACHER_PARAMS = Arrays.asList("defTMath", "defTInform", "defTChem",
            "defTPhy", "defTRu", "defTEn");

    List<String> TEACHER_LISTS_PARAMS = Arrays.asList("mathTeachers", "informTeachers", "chemTeachers",
            "phyTeachers", "ruTeachers", "enTeachers");

    String PARAM_GRADES = "grades";

    String PARAM_CLASSES = "classes";

    String PARAM_STUDENTS = "students";

    String PARAM_TEACHERS = "teachers";

    String PARAM_PAGES = "pages";

    String PARAM_DATES = "dates";

    String PARAM_TASKS = "tasks";

    String PARAM_STUDENT_LOGIN = "studentLogin";

    String PARAM_STUDENT_NAME = "studentName";

    String PARAM_STUDENT_SURNAME = "studentSurname";

    String PARAM_STUDENT_CLASS = "studentStudyClass";

    String PARAM_STUDENT_MAIL = "studentMail";

    String PARAM_CLASS_NAME = "className";

    String PARAM_ERROR = "error";

    String PARAM_DESCRIPTION = "description";

    int DEFAULT_PAGE = 1;

    int START_ARRAY_COUNT = 0;

    int SUBJECT_COUNT = 6;

    String SPACE = " ";

    Long STUDENT_ID = 1L;

    Long TEACHER_ID = 2L;

    Long ADMIN_ID = 3L;

    Long ALL_SUBJECTS_ID = 0L;

    Long ALL_STUDENTS_ID = 0L;

    Long ERROR_ID = 0L;

    Long NO_CLASS_ID = 0L;

    String RU_LOCALE = "ru_RU";

    String EN_LOCALE = "en_US";
}
