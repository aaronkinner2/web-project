package com.org.iisg.db.parameter;

public interface ErrorMessages {

    String ERROR_CREATE_CLASS_EN = "Such class name is already used, use another.";

    String ERROR_CREATE_CLASS_RU = "Такое имя класса уже используется, выберите другое.";

    String ERROR_CHANGE_PASSWORD_MESSAGE_INCORRECT_INPUT_EN =
            "You didn't input your old password correct";

    String ERROR_CHANGE_PASSWORD_MESSAGE_INCORRECT_INPUT_RU =
            "Вы неправильно ввели старый пароль";

    String ERROR_CHANGE_PASSWORD_MESSAGE_EQUAL_INPUT_EN = "Old and new passwords are equal";

    String ERROR_CHANGE_PASSWORD_MESSAGE_EQUAL_INPUT_RU = "Старый и новый пароль совпадают";

    String ERROR_SIGN_IN_MESSAGE_EN = "Username or password are incorrect";

    String ERROR_SIGN_IN_MESSAGE_RU = "Логин или пароль введены неверно";

    String ERROR_USER_WITH_LOGIN_EXISTS_MESSAGE_EN = "This login is already used. Use another.";

    String ERROR_USER_WITH_LOGIN_EXISTS_MESSAGE_RU = "Этот логин уже занят. Используйте другой.";
}
