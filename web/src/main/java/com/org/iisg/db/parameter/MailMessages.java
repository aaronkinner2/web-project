package com.org.iisg.db.parameter;

public interface MailMessages {

    String SUCCESSFUL_SIGN_UP = "Successful registration - IISG";

    String SUCCESSFUL_CHANGE_PASSWORD = "Successful password change - IISG";

    String SUCCESSFUL_CHANGE_MAIL = "Successful email change - IISG";

    String START_TEXT = "Hello, ";

    String SUCCESSFUL_SIGN_UP_TEXT = ".\nThank you for the registration in our service." +
            "\nEnjoy your time and good luck at studying.\nSee you soon:)";

    String SUCCESSFUL_SIGN_UP_TEXT_1 = "Hello.\nToday you have your own account as a teacher" +
            " in IISG.\nYour login: ";

    String SUCCESSFUL_SIGN_UP_TEXT_2 = ".\nYour password: ";

    String SUCCESSFUL_SIGN_UP_TEXT_3 = ".\nHope, you will be the great teacher. See you soon:)";

    String SUCCESSFUL_CHANGE_PASSWORD_TEXT = ".\nYour password successfully changed." +
            "\nDon't forget it.\nSee you soon:)";

    String SUCCESSFUL_CHANGE_MAIL_TEXT = ".\nYour email address successfully changed." +
            "\nHope your new email will be better.\nSee you soon:)";
}
