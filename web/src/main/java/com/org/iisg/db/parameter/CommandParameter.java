package com.org.iisg.db.parameter;

public interface CommandParameter {

    String RU_START_COMMAND = "ruStart";

    String EN_START_COMMAND = "enStart";

    String SIGN_UP_COMMAND = "register";

    String RETURN_COMMAND = "back";

    String SIGN_OUT_COMMAND = "signOut";

    String SIGN_IN_COMMAND = "signIn";

    String PROFILE_COMMAND = "profile";

    String CHANGE_PASSWORD_COMMAND = "changePassword";

    String CHANGE_EMAIL_COMMAND = "changeEmail";

    String DELETE_COMMAND = "delete";

    String ADD_COMMAND = "add";

    String BREAK_COMMAND = "break";

    String GET_NO_CLASS_STUDENTS_COMMAND = "getNoClassStudents";

    String GET_TEACHERS_COMMAND = "getTeachers";

    String GET_CLASSES_COMMAND = "getClasses";

    String MAIN_COMMAND = "main";

    String PAGE_COMMAND = "page";

    String TASKS_COMMAND = "tasks";

    String GET_COMMAND = "get";

    String CHANGE_COMMAND = "change";

    String SUBJECT_COMMAND = "subject";

    String CREATE_COMMAND = "create";
}
