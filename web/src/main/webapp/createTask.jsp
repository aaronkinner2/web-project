<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message bundle="${lang}" key="page.main"/>
        <%=session.getAttribute("login") == null ? "" : session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/fontawesome.css">
    <link rel="stylesheet" href="bootstrap/css/createTask.css">
    <link rel="stylesheet" href="bootstrap/css/common.css">
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;"
     href="javascript:void(0)">
    <a class="navBar-brand">
        <img src="bootstrap/css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="teacher-name" id="login">
        <%=session.getAttribute("login")%>
    </div>
    <form autocomplete="off" method="post" action="teacherTasks">
        <input type="hidden" name="command" value="main"/>
        <button type="submit" class="btn-color-light-blue btn btn-navBar btn-block my-3">
            <fmt:message bundle="${lang}" key="button.tasks"/>
        </button>
    </form>
    <form autocomplete="off" method="post" action="teacherTasks">
        <input type="hidden" name="command" value="profile"/>
        <button type="submit" class="btn-color-orange btn btn-navBar btn-block my-3">
            <fmt:message bundle="${lang}" key="button.profile"/>
        </button>
    </form>
    <form autocomplete="off" method="post" action="teacherTasks">
        <input type="hidden" name="command" value="signOut"/>
        <button type="submit" class="btn-color-red btn btn-navBar btn-block my-3-2">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-12">
            <div class="task-panel">
                <h4><fmt:message bundle="${lang}" key="create.task"/></h4>
                <form autocomplete="off" method="post" action="createTask">
                    <input type="hidden" name="command" value="create"/>
                    <div class="task-panel-m">
                        <label>
                            <select class="select-panel" name="studentId" size="1" required>
                                <option value="<%=session.getAttribute("studentId")%>">
                                    <%=request.getAttribute("studentName")%>
                                </option>
                                <option value="0">
                                    <fmt:message bundle="${lang}" key="allStudents"/>
                                </option>
                            </select>
                        </label>
                        <dt class="input-label h m">
                            <label class="form-label h6">
                                <fmt:message bundle="${lang}" key="description"/>
                            </label>
                        </dt>
                        <textarea name="description" required></textarea>
                    </div>
                    <button type="submit" class="btn-color-plum btn btn-block my-3-4">
                        <fmt:message bundle="${lang}" key="button.createTask"/>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
