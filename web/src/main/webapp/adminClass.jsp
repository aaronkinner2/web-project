<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message bundle="${lang}" key="page.main"/>
        <%=session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/fontawesome.css">
    <link rel="stylesheet" href="bootstrap/css/adminClass.css">
    <link rel="stylesheet" href="bootstrap/css/common.css">
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;" href="javascript:void(0)">
    <a class="navBar-brand">
        <img src="bootstrap/css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="admin-name"><%=session.getAttribute("login")%>
    </div>
    <form autocomplete="off" method="post" action="adminClass">
        <input type="hidden" name="command" value="main"/>
        <button type="submit" class="btn btn-color-light-blue btn-navBar btn-block my-3">
            <fmt:message bundle="${lang}" key="button.classes"/>
        </button>
    </form>
    <form autocomplete="off" method="post" action="adminClass">
        <input type="hidden" name="command" value="signOut"/>
        <button type="submit" class="btn btn-color-red btn-navBar btn-block my-3-3">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-9">
            <div class="teacher-panel">
                <h3><fmt:message bundle="${lang}" key="class"/> <%=request.getAttribute("className")%>
                </h3>
                <div class="teacher-panel-l">
                    <div class="teacher-panel-m">
                        <table class="teacher-table" id="student-grade-table">
                            <thead>
                            <tr>
                                <th class="th-1">
                                    <fmt:message bundle="${lang}" key="subject"/>
                                </th>
                                <th class="th-2">
                                    <fmt:message bundle="${lang}" key="name"/>
                                </th>
                                <th class="th-3">
                                    <fmt:message bundle="${lang}" key="surname"/>
                                </th>
                                <th class="th-4">
                                    <fmt:message bundle="${lang}" key="email"/>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="my-td-1">
                                    <fmt:message bundle="${lang}" key="math"/>
                                </td>
                                <td class="my-td-2">
                                    <%=request.getAttribute("tMathName") == null ? "##########" :
                                            request.getAttribute("tMathName")%>
                                </td>
                                <td class="my-td-3">
                                    <%=request.getAttribute("tMathSurname") == null ? "##########" :
                                            request.getAttribute("tMathSurname")%>
                                </td>
                                <td class="my-td-4">
                                    <%=request.getAttribute("tMathMail") == null ? "##########" :
                                            request.getAttribute("tMathMail")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="my-td-1">
                                    <fmt:message bundle="${lang}" key="inform"/>
                                </td>
                                <td class="my-td-2">
                                    <%=request.getAttribute("tInformName") == null ? "##########" :
                                            request.getAttribute("tInformName")%>
                                </td>
                                <td class="my-td-3">
                                    <%=request.getAttribute("tInformSurname") == null ? "##########" :
                                            request.getAttribute("tInformSurname")%>
                                </td>
                                <td class="my-td-4">
                                    <%=request.getAttribute("tInformMail") == null ? "##########" :
                                            request.getAttribute("tInformMail")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="my-td-1">
                                    <fmt:message bundle="${lang}" key="chem"/>
                                </td>
                                <td class="my-td-2">
                                    <%=request.getAttribute("tChemName") == null ? "##########" :
                                            request.getAttribute("tChemName")%>
                                </td>
                                <td class="my-td-3">
                                    <%=request.getAttribute("tChemSurname") == null ? "##########" :
                                            request.getAttribute("tChemSurname")%>
                                </td>
                                <td class="my-td-4">
                                    <%=request.getAttribute("tChemMail") == null ? "##########" :
                                            request.getAttribute("tChemMail")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="my-td-1">
                                    <fmt:message bundle="${lang}" key="phy"/>
                                </td>
                                <td class="my-td-2">
                                    <%=request.getAttribute("tPhyName") == null ? "##########" :
                                            request.getAttribute("tPhyName")%>
                                </td>
                                <td class="my-td-3">
                                    <%=request.getAttribute("tPhySurname") == null ? "##########" :
                                            request.getAttribute("tPhySurname")%>
                                </td>
                                <td class="my-td-4">
                                    <%=request.getAttribute("tPhyMail") == null ? "##########" :
                                            request.getAttribute("tPhyMail")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="my-td-1">
                                    <fmt:message bundle="${lang}" key="ru"/>
                                </td>
                                <td class="my-td-2">
                                    <%=request.getAttribute("tRuName") == null ? "##########" :
                                            request.getAttribute("tRuName")%>
                                </td>
                                <td class="my-td-3">
                                    <%=request.getAttribute("tRuSurname") == null ? "##########" :
                                            request.getAttribute("tRuSurname")%>
                                </td>
                                <td class="my-td-4">
                                    <%=request.getAttribute("tRuMail") == null ? "##########" :
                                            request.getAttribute("tRuMail")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="my-td-1">
                                    <fmt:message bundle="${lang}" key="en"/>
                                </td>
                                <td class="my-td-2">
                                    <%=request.getAttribute("tEnName") == null ? "##########" :
                                            request.getAttribute("tEnName")%>
                                </td>
                                <td class="my-td-3">
                                    <%=request.getAttribute("tEnSurname") == null ? "##########" :
                                            request.getAttribute("tEnSurname")%>
                                </td>
                                <td class="my-td-4">
                                    <%=request.getAttribute("tEnMail") == null ? "##########" :
                                            request.getAttribute("tEnMail")%>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xl-3">
            <div class="button-panel">
                <form autocomplete="off" method="post" action="adminClass">
                    <input type="hidden" name="command" value="get"/>
                    <button type="submit" class="btn-color-green btn btn-block my-3-2">
                        <fmt:message bundle="${lang}" key="button.students"/>
                    </button>
                </form>
                <form autocomplete="off" method="post" action="adminClass">
                    <input type="hidden" name="command" value="change"/>
                    <button type="submit" class="btn-color-red-blue btn btn-block my-3-2">
                        <fmt:message bundle="${lang}" key="button.changeClassTeachers"/>
                    </button>
                </form>
                <form autocomplete="off" method="post" action="adminClass">
                    <input type="hidden" name="command" value="delete"/>
                    <button type="submit" class="btn-color-orange btn btn-block my-3-2">
                        <fmt:message bundle="${lang}" key="button.deleteClass"/>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

