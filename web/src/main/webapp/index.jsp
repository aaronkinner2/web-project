<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setLocale value="en_US"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="UTF-16">
    <title><fmt:message bundle="${lang}" key="page.index"/></title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/fontawesome.css">
    <link rel="stylesheet" href="bootstrap/css/index.css">
    <link rel="stylesheet" href="bootstrap/css/common.css">
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;">
    <a class="navBar-brand">
        <img src="bootstrap/css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12">
            <div class="lang-panel">
                <h3>Choose language/ Выберите язык</h3>
            </div>
            <div class="myPanel">
                <form class="cl-btn" autocomplete="off" method="post" action="index">
                    <input type="hidden" name="command" value="enStart"/>
                    <button type="submit" class="btn btn-orange-color f4 btn-block">
                        <fmt:message bundle="${lang}" key="language.english"/>
                    </button>
                </form>
                <form class="cl-btn-2" autocomplete="off" method="post" action="index">
                    <input type="hidden" name="command" value="ruStart"/>
                    <button type="submit" class="btn btn-blue-color f4 btn-block">
                        <fmt:message bundle="${lang}" key="language.russian"/>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
