<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <title><fmt:message bundle="${lang}" key="page.main"/>
        <%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/fontawesome.css">
    <link rel="stylesheet" href="bootstrap/css/adminStudents.css">
    <link rel="stylesheet" href="bootstrap/css/common.css">
    <script type="text/javascript" src="bootstrap/js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery.tablesorter.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#student-table").tablesorter();
        });
    </script>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;">
    <a class="navBar-brand">
        <img src="bootstrap/css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="admin-name" id="login"><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </div>
    <form autocomplete="off" method="post" action="adminStudents">
        <input type="hidden" name="command" value="main"/>
        <button type="submit" class="btn-color-orange btn btn-navBar btn-block my-3">
            <fmt:message bundle="${lang}" key="button.mainPage"/>
        </button>
    </form>
    <form autocomplete="off" method="post" action="adminStudents">
        <input type="hidden" name="command" value="signOut"/>
        <button type="submit" class="btn btn-color-red btn-navBar btn-block my-3-3">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-12">
            <h3><fmt:message bundle="${lang}" key="students"/></h3>
            <form autocomplete="off" method="post" action="adminStudents">
                <input type="hidden" name="command" value="page"/>
                <label>
                    <select class="select-page" name="pageId" id="pageId" size="1">
                        <jsp:useBean id="pages" scope="request" type="java.util.List"/>
                        <c:forEach items="${pages}" var="page">
                            <option value="<c:out value="${page}"/>">
                                <c:out value="${page}"/>
                            </option>
                        </c:forEach>
                    </select>
                </label>
                <button type="submit" class="btn-color-blue btn f4 btn-block my-3-4">
                    <fmt:message bundle="${lang}" key="button.go"/>
                </button>
            </form>
        </div>
        <div class="col-xl-12">
            <div class="student-panel">
                <form autocomplete="off" method="post" action="adminStudents">
                    <input type="hidden" name="command" value="get"/>
                    <div class="student-panel-m">
                        <table class="my-student-table" id="student-table">
                            <thead>
                            <tr>
                                <th class="th-1">

                                </th>
                                <th class="th-2">
                                    <fmt:message bundle="${lang}" key="login"/>
                                </th>
                                <th class="th-3">
                                    <fmt:message bundle="${lang}" key="name"/>
                                </th>
                                <th class="th-4">
                                    <fmt:message bundle="${lang}" key="surname"/>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <jsp:useBean id="students" scope="request" type="java.util.List"/>
                            <c:forEach items="${students}" var="student">
                                <tr>
                                    <td class="my-td-1">
                                        <input name="studentId" id="studentId" type="radio"
                                               title="<fmt:message bundle="${lang}"
                                                                      key="chooseOneField"/>"
                                               oninvalid="this.setCustomValidity
                                                       ('<fmt:message bundle="${lang}"
                                                                      key="chooseOneField"/>')"
                                               oninput="setCustomValidity('')"
                                               value=<c:out value="${student.id}"/>>
                                    </td>
                                    <td class="my-td-2">
                                        <c:out value="${student.login}"/>
                                    </td>
                                    <td class="my-td-3">
                                        <c:out value="${student.userInfo.firstName}"/>
                                    </td>
                                    <td class="my-td-4">
                                        <c:out value="${student.userInfo.lastName}"/>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <button type="submit" class="btn-color-green btn f4 btn-block my-3-2">
                        <fmt:message bundle="${lang}" key="button.get"/>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
