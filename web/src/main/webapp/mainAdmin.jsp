<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <title><fmt:message bundle="${lang}" key="page.main"/><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/fontawesome.css">
    <link rel="stylesheet" href="bootstrap/css/mainAdmin.css">
    <link rel="stylesheet" href="bootstrap/css/common.css">
    <script type="text/javascript" src="bootstrap/js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery.tablesorter.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#online-table").tablesorter();
        });
    </script>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;">
    <a class="navBar-brand">
        <img src="bootstrap/css/icons/graduation-hat.png" class="d-inline-block align-top img-size"
             alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="admin-name" id="login"><%=session.getAttribute("login") == null ? "12345678901234" :
            session.getAttribute("login")%>
    </div>
    <form autocomplete="off" method="post" action="mainAdmin">
        <input type="hidden" name="command" value="signOut"/>
        <button type="submit" class="btn btn-color-red btn-navBar btn-block my-3-1">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-12">
            <div class="button-panel">
                <form autocomplete="off" method="post" action="mainAdmin">
                    <input type="hidden" name="command" value="getTeachers"/>
                    <button type="submit" class="btn-color-green btn f4 btn-block my-3-2">
                        <fmt:message bundle="${lang}" key="button.teacher"/>
                    </button>
                </form>
                <form autocomplete="off" method="post" action="mainAdmin">
                    <input type="hidden" name="command" value="getNoClassStudents"/>
                    <button type="submit" class="btn-color-red-blue btn f4 btn-block my-3-2">
                        <fmt:message bundle="${lang}" key="button.noClassStudents"/>
                    </button>
                </form>
                <form autocomplete="off" method="post" action="mainAdmin">
                    <input type="hidden" name="command" value="getClasses"/>
                    <button type="submit" class="btn-color-orange btn f4 btn-block my-3-2">
                        <fmt:message bundle="${lang}" key="button.classes"/>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>