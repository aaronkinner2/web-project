<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="UTF-16">
    <title><fmt:message bundle="${lang}" key="page.login"/></title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/fontawesome.css">
    <link rel="stylesheet" href="bootstrap/css/signIn.css">
    <link rel="stylesheet" href="bootstrap/css/common.css">
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;">
    <a class="navBar-brand">
        <img src="bootstrap/css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-7 my-auto">
            <div class="description">
                <h1><fmt:message bundle="${lang}" key="label.signIn.title"/></h1>
                <p>
                <h2><fmt:message bundle="${lang}" key="label.signIn.main"/></h2>
            </div>
        </div>
        <div class="col-5">
            <div class="myPanel">
                <h3><fmt:message bundle="${lang}" key="label.form.title"/></h3>
                <form class="home-signIn" autocomplete="off" method="post" action="signIn">
                    <input type="hidden" name="command" value="signIn"/>
                    <div class="username-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="login">
                                    <fmt:message bundle="${lang}" key="login"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="text" name="login" id="login"
                                       class="form-control form-control-lg input-block" autocomplete="off" required
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputLogin"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputLogin"/>')"
                                       oninput="setCustomValidity('')"
                                       value=<%=request.getAttribute("loginError") == null ? "" :
                                       request.getAttribute("loginError")%>>
                            </dd>
                        </dl>
                    </div>
                    <div class="password-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="password">
                                    <fmt:message bundle="${lang}" key="password"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="password" name="password" id="password"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputPassword"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputPassword"/>')"
                                       oninput="setCustomValidity('')"
                                       class="form-control form-control-lg input-block" autocomplete="off" required>
                            </dd>
                        </dl>
                    </div>
                    <div class="error-signIn">
                        <%=request.getAttribute("error") == null ? "" : request.getAttribute("error")%>
                    </div>
                    <button type="submit" class="btn btn-color-blue f4 btn-block my-3">
                        <fmt:message bundle="${lang}" key="button.signIn"/>
                    </button>
                </form>
                <form class="home-signup" autocomplete="off" method="post" action="signIn">
                    <input type="hidden" name="command" value="create"/>
                    <button type="submit" class="btn btn-color-green f4 btn-block my-4">
                        <fmt:message bundle="${lang}" key="button.signUp"/>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

