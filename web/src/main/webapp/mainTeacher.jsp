<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message bundle="${lang}" key="page.main"/><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/fontawesome.css">
    <link rel="stylesheet" href="bootstrap/css/mainTeacher.css">
    <link rel="stylesheet" href="bootstrap/css/common.css">
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;" href="javascript:void(0)">
    <a class="navBar-brand">
        <img src="bootstrap/css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="teacher-name" id="login"><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </div>
    <form autocomplete="off" method="post" action="mainTeacher">
        <input type="hidden" name="command" value="profile"/>
        <button type="submit" class="btn-color-orange btn btn-navBar btn-block my-3">
            <fmt:message bundle="${lang}" key="button.profile"/>
        </button>
    </form>
    <form autocomplete="off" method="post" action="mainTeacher">
        <input type="hidden" name="command" value="signOut"/>
        <button type="submit" class="btn-color-red btn btn-navBar btn-block my-3-2">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-12">
            <div class="class-panel">
                <h3><fmt:message bundle="${lang}" key="yourClasses"/></h3>
                <div class="class-panel-m">
                    <form autocomplete="off" method="post" action="mainTeacher">
                        <input type="hidden" name="command" value="get"/>
                        <select class="select-class" name="classId" id="classId" size="1">
                            <option disabled>
                                <fmt:message bundle="${lang}" key="chooseClass"/>
                            </option>
                            <jsp:useBean id="classes" scope="request" type="java.util.List"/>
                            <c:forEach items="${classes}" var="class">
                                <option value="<c:out value="${class.id}"/>">
                                    <c:out value="${class.name}"/>
                                </option>
                            </c:forEach>
                        </select>
                        <button type="submit" class="btn-color-blue btn f4 btn-block my-3-4">
                            <fmt:message bundle="${lang}" key="button.goOver"/>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
