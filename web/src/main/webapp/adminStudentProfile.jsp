<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message bundle="${lang}" key="page.main"/><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/fontawesome.css">
    <link rel="stylesheet" href="bootstrap/css/adminStudentProfile.css">
    <link rel="stylesheet" href="bootstrap/css/common.css">
    <script type="text/javascript" src="bootstrap/js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery.tablesorter.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#student-grade-table").tablesorter();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#my-student-table").tablesorter();
        });
    </script>
    <script type="text/javascript" src="bootstrap/js/jquery.tablesorter.widgets.js"></script>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;"
     href="javascript:void(0)">
    <a class="navBar-brand">
        <img src="bootstrap/css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="admin-name" id="login"><%=session.getAttribute("login")%>
    </div>
    <form autocomplete="off" method="post" action="adminStudentProfile">
        <input type="hidden" name="command" value="main"/>
        <button type="submit" class="btn-color-light-blue btn btn-navBar btn-block my-3">
            <fmt:message bundle="${lang}" key="button.students"/>
        </button>
    </form>
    <form autocomplete="off" method="post" action="adminStudentProfile">
        <input type="hidden" name="command" value="signOut"/>
        <button type="submit" class="btn-color-red btn btn-navBar btn-block my-3-2">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-6">
            <div class="student-data-panel">
                <h6><fmt:message bundle="${lang}" key="studentData"/></h6>
                <div class="student-profile">
                    <div>
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="userName">
                                    <fmt:message bundle="${lang}" key="login"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="userName">
                                    <%=request.getAttribute("studentLogin") == null ? "######" :
                                            request.getAttribute("studentLogin")%>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="name">
                                    <fmt:message bundle="${lang}" key="name"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="name">
                                    <%=request.getAttribute("studentName") == null ? "######" :
                                            request.getAttribute("studentName")%>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="surname">
                                    <fmt:message bundle="${lang}" key="surname"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="surname">
                                    <%=request.getAttribute("studentSurname") == null ? "######" :
                                            request.getAttribute("studentSurname")%>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="mail">
                                    <fmt:message bundle="${lang}" key="email"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="mail">
                                    <%=request.getAttribute("studentMail") == null ? "######" :
                                            request.getAttribute("studentMail")%>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="studyClass">
                                    <fmt:message bundle="${lang}" key="class"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="studyClass">
                                    <%=request.getAttribute("studentStudyClass") == null ? "######" :
                                            request.getAttribute("studentStudyClass")%>
                                </label>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="button-panel">
                <form autocomplete="off" method="post" action="adminStudentProfile">
                    <input type="hidden" name="command" value="get"/>
                    <button type="submit" class="btn-color-green btn btn-block my-3-3">
                        <fmt:message bundle="${lang}" key="button.grades"/>
                    </button>
                </form>
                <form autocomplete="off" method="post" action="adminStudentProfile">
                    <select class="select-class" name="classId" id="classId" size="1">
                        <option disabled>
                            <fmt:message bundle="${lang}" key="chooseClass"/>
                        </option>
                        <jsp:useBean id="classes" scope="request" type="java.util.List"/>
                        <c:forEach items="${classes}" var="class">
                            <option value="<c:out value="${class.id}"/>">
                                <c:out value="${class.name}"/>
                            </option>
                        </c:forEach>
                        <option value="0">
                            <fmt:message bundle="${lang}" key="noClass"/>
                        </option>
                    </select>
                    <input type="hidden" name="command" value="change"/>
                    <button type="submit" class="btn-color-red-blue btn btn-block my-3-3">
                        <fmt:message bundle="${lang}" key="button.changeClass"/>
                    </button>
                </form>
                <form autocomplete="off" method="post" action="adminStudentProfile">
                    <input type="hidden" name="command" value="delete"/>
                    <button type="submit" class="btn-color-blue btn btn-block my-3-4">
                        <fmt:message bundle="${lang}" key="button.deleteStudent"/>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>