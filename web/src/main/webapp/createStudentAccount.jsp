<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="UTF-16">
    <title><fmt:message bundle="${lang}" key="page.signUp"/></title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/fontawesome.css">
    <link rel="stylesheet" href="bootstrap/css/createStudentAccount.css">
    <link rel="stylesheet" href="bootstrap/css/common.css">
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;">
    <a class="navBar-brand">
        <img src="bootstrap/css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12">
            <div class="description">
                <fmt:message bundle="${lang}" key="signUp"/>
            </div>
            <div class="myPanel">
                <h3><fmt:message bundle="${lang}" key="label.form.title"/></h3>
                <form class="home-signUp" autocomplete="off" method="post" action="createStudentAccount">
                    <input type="hidden" name="command" value="register"/>
                    <div class="username-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="login">
                                    <fmt:message bundle="${lang}" key="login"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="text" name="login" id="login"
                                       class="form-control form-control-lg input-block"
                                       autocomplete="off" required spellcheck="false" pattern="[A-z0-9]{4,18}"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputLoginHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputLoginHint"/>')"
                                       oninput="setCustomValidity('')"
                                       value=<%=request.getAttribute("login") == null ? "" :
                                       request.getAttribute("login")%>>
                            </dd>
                        </dl>
                    </div>
                    <div class="name-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="name">
                                    <fmt:message bundle="${lang}" key="name"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="text" name="name" id="name"
                                       class="form-control form-control-lg input-block" autocomplete="off"
                                       required spellcheck="false"
                                       pattern="{2, 15}"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputNameHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputNameHint"/>')"
                                       oninput="setCustomValidity('')"
                                       value=<%=request.getAttribute("name") == null ? "" :
                                       request.getAttribute("name")%>>
                            </dd>
                        </dl>
                    </div>
                    <div class="surname-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="surname">
                                    <fmt:message bundle="${lang}" key="surname"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="text" name="surname" id="surname"
                                       class="form-control form-control-lg input-block" autocomplete="off"
                                       required spellcheck="false"
                                       pattern="{2, 15}"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputSurnameHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputSurnameHint"/>')"
                                       oninput="setCustomValidity('')"
                                       value=<%=request.getAttribute("surname") == null ? "" :
                                       request.getAttribute("surname")%>>
                            </dd>
                        </dl>
                    </div>
                    <div class="email-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="email">
                                    <fmt:message bundle="${lang}" key="email"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="email" name="email" id="email"
                                       class="form-control form-control-lg input-block" autocomplete="off"
                                       spellcheck="false" required
                                       pattern="([A-z0-9_.-]{1,})@([A-z0-9_.-]{1,}).([A-z]{2,8})"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputEmailHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputEmailHint"/>')"
                                       oninput="setCustomValidity('')"
                                       value=<%= request.getAttribute("email") == null ? "" :
                                       request.getAttribute("email")%>>
                            </dd>
                        </dl>
                    </div>
                    <div class="password-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="password">
                                    <fmt:message bundle="${lang}" key="password"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="password" name="password" id="password"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputPasswordHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputPasswordHint"/>')"
                                       oninput="setCustomValidity('')"
                                       class="form-control form-control-lg input-block" autocomplete="off" required
                                       pattern="[A-z0-9]{8,15}">
                            </dd>
                        </dl>
                    </div>
                    <div class="error-createStudentAccount">
                        <%=request.getAttribute("error") == null ? "" : request.getAttribute("error")%>
                    </div>
                    <button type="submit" class="btn btn-blue-color btn-block">
                        <fmt:message bundle="${lang}" key="button.createAccount"/>
                    </button>
                </form>
                <form class="home-back" autocomplete="off" method="post" action="createStudentAccount">
                    <input type="hidden" name="command" value="back"/>
                    <button type="submit" class="btn btn-red-color btn-block my-3-2">
                        <fmt:message bundle="${lang}" key="button.return"/>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
