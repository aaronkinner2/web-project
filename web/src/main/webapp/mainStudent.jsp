<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title>
        <fmt:message bundle="${lang}" key="page.main"/><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/fontawesome.css">
    <link rel="stylesheet" href="bootstrap/css/mainStudent.css">
    <link rel="stylesheet" href="bootstrap/css/common.css">
    <script type="text/javascript" src="bootstrap/js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery.tablesorter.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#my-grades-table").tablesorter();
        });
    </script>
    <script type="text/javascript" src="bootstrap/js/jquery.tablesorter.widgets.js"></script>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;">
    <a class="navBar-brand">
        <img src="bootstrap/css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="student-name" id="login"><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </div>
    <form class="home-profile" autocomplete="off" method="post" action="mainStudent">
        <input type="hidden" name="command" value="profile"/>
        <button type="submit" class="btn-color-orange btn f4 btn-block my-3 btn-navBar">
            <fmt:message bundle="${lang}" key="button.profile"/>
        </button>
    </form>
    <form class="home-signOut" autocomplete="off" method="post" action="mainStudent">
        <input type="hidden" name="command" value="signOut"/>
        <button type="submit" class="btn-color-red btn f4 btn-block my-3-2 btn-navBar">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-6">
            <div class="grade-panel">
                <h3><fmt:message bundle="${lang}" key="studentGrades"/></h3>
                <form autocomplete="off" method="post" action="mainStudent">
                    <input type="hidden" name="command" value="page"/>
                    <label>
                        <select class="select-page" name="pageId" size="1">
                            <jsp:useBean id="pages" scope="request" type="java.util.List"/>
                            <c:forEach items="${pages}" var="page">
                                <option value="<c:out value="${page}"/>">
                                    <c:out value="${page}"/>
                                </option>
                            </c:forEach>
                        </select>
                    </label>
                    <button type="submit" class="btn-color-blue btn btn-block my-3-4">
                        <fmt:message bundle="${lang}" key="button.go"/>
                    </button>
                </form>
                <div class="grade-panel-m">
                    <table class="my-grades-table" id="my-grades-table">
                        <thead>
                        <tr>
                            <th class="th-1">
                                <fmt:message bundle="${lang}" key="gradeDate"/>
                            </th>
                            <th class="th-2">
                                <fmt:message bundle="${lang}" key="gradeSubject"/>
                            </th>
                            <th class="th-3">
                                <fmt:message bundle="${lang}" key="gradeMark"/>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <jsp:useBean id="grades" scope="request" type="java.util.List"/>
                        <c:forEach items="${grades}" var="grade">
                            <tr>
                                <td class="my-td-1">
                                    <c:out value="${grade.date}"/>
                                </td>
                                <td class="my-td-2">
                                    <c:out value="${grade.subject.name}"/>
                                </td>
                                <td class="my-td-3">
                                    <c:out value="${grade.mark}"/>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="teacher-panel">
                <h5>
                    <%=request.getAttribute("className") == null ? "---" :
                        request.getAttribute("className")%>
                </h5>
                <table class="my-teacher-table">
                    <tr>
                        <td class="my-td-1-1-0">
                            <fmt:message bundle="${lang}" key="math"/>
                        </td>
                        <td class="my-td-1-1-1">
                            <%=request.getAttribute("tMathNameSurname") == null ? "##########" :
                                    request.getAttribute("tMathNameSurname")%>
                        </td>
                        <td class="my-td-1-1-2">
                            <%=request.getAttribute("tMathMail") == null ? "##########" :
                                    request.getAttribute("tMathMail")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1-0">
                            <fmt:message bundle="${lang}" key="inform"/>
                        </td>
                        <td class="my-td-1-1-1">
                            <%=request.getAttribute("tInformNameSurname") == null ? "##########" :
                                    request.getAttribute("tInformNameSurname")%>
                        </td>
                        <td class="my-td-1-1-2">
                            <%=request.getAttribute("tInformMail") == null ? "##########" :
                                    request.getAttribute("tInformMail")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1-0">
                            <fmt:message bundle="${lang}" key="chem"/>
                        </td>
                        <td class="my-td-1-1-1">
                            <%=request.getAttribute("tChemNameSurname") == null ? "##########" :
                                    request.getAttribute("tChemNameSurname")%>
                        </td>
                        <td class="my-td-1-1-2">
                            <%=request.getAttribute("tChemMail") == null ? "##########" :
                                    request.getAttribute("tChemMail")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1-0">
                            <fmt:message bundle="${lang}" key="phy"/>
                        </td>
                        <td class="my-td-1-1-1">
                            <%=request.getAttribute("tPhyNameSurname") == null ? "##########" :
                                    request.getAttribute("tPhyNameSurname")%>
                        </td>
                        <td class="my-td-1-1-2">
                            <%=request.getAttribute("tPhyMail") == null ? "##########" :
                                    request.getAttribute("tPhyMail")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1-0">
                            <fmt:message bundle="${lang}" key="ru"/>
                        </td>
                        <td class="my-td-1-1-1">
                            <%=request.getAttribute("tRuNameSurname") == null ? "##########" :
                                    request.getAttribute("tRuNameSurname")%>
                        </td>
                        <td class="my-td-1-1-2">
                            <%=request.getAttribute("tRuMail") == null ? "##########" :
                                    request.getAttribute("tRuMail")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1-0">
                            <fmt:message bundle="${lang}" key="en"/>
                        </td>
                        <td class="my-td-1-1-1">
                            <%=request.getAttribute("tEnNameSurname") == null ? "##########" :
                                    request.getAttribute("tEnNameSurname")%>
                        </td>
                        <td class="my-td-1-1-2">
                            <%=request.getAttribute("tEnMail") == null ? "##########" :
                                    request.getAttribute("tEnMail")%>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="row">
                <div class="grade-choose-panel">
                    <h4><fmt:message bundle="${lang}" key="gradesSubject"/></h4>
                    <form autocomplete="off" method="post" action="mainStudent">
                        <input type="hidden" name="command" value="change"/>
                        <div class="change-grade">
                            <label>
                                <select class="select-s" name="subjectId" size="1">
                                    <option disabled>
                                        <fmt:message bundle="${lang}" key="chooseSubject"/>
                                    </option>
                                    <option value="1">
                                        <fmt:message bundle="${lang}" key="math"/>
                                    </option>
                                    <option value="2">
                                        <fmt:message bundle="${lang}" key="inform"/>
                                    </option>
                                    <option value="3">
                                        <fmt:message bundle="${lang}" key="chem"/>
                                    </option>
                                    <option value="4">
                                        <fmt:message bundle="${lang}" key="phy"/>
                                    </option>
                                    <option value="5">
                                        <fmt:message bundle="${lang}" key="ru"/>
                                    </option>
                                    <option value="6">
                                        <fmt:message bundle="${lang}" key="en"/>
                                    </option>
                                    <option selected value="0">
                                        <fmt:message bundle="${lang}" key="all"/>
                                    </option>
                                </select>
                            </label>
                            <button type="submit" class="btn-color-green btn btn-block my-3-3">
                                <fmt:message bundle="${lang}" key="button.change"/>
                            </button>
                        </div>
                    </form>
                    <form autocomplete="off" method="post" action="mainStudent">
                        <input type="hidden" name="command" value="tasks"/>
                        <div class="get-tasks">
                            <label>
                                <select class="select-s" name="subjectId" size="1">
                                    <option disabled>
                                        <fmt:message bundle="${lang}" key="chooseSubject"/>
                                    </option>
                                    <option value="1">
                                        <fmt:message bundle="${lang}" key="math"/>
                                    </option>
                                    <option value="2">
                                        <fmt:message bundle="${lang}" key="inform"/>
                                    </option>
                                    <option value="3">
                                        <fmt:message bundle="${lang}" key="chem"/>
                                    </option>
                                    <option value="4">
                                        <fmt:message bundle="${lang}" key="phy"/>
                                    </option>
                                    <option value="5">
                                        <fmt:message bundle="${lang}" key="ru"/>
                                    </option>
                                    <option value="6">
                                        <fmt:message bundle="${lang}" key="en"/>
                                    </option>
                                </select>
                            </label>
                            <button type="submit" class="btn-color-plum btn btn-block my-3-3">
                                <fmt:message bundle="${lang}" key="button.tasks"/>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="average-panel">
                    <h6><fmt:message bundle="${lang}" key="averageGrades"/></h6>
                    <table class="my-aver-grades-table">
                        <tr>
                            <td class="my-td-1-0">
                                <fmt:message bundle="${lang}" key="common"/>
                            </td>
                            <td class="my-td-1-0">
                                <%=request.getAttribute("commonAvMark") == null ? "-" :
                                        request.getAttribute("commonAvMark")%>
                            </td>
                        </tr>
                        <tr>
                            <td class="my-td-1-1">
                                <fmt:message bundle="${lang}" key="math"/>
                            </td>
                            <td class="my-td-1-1">
                                <%=request.getAttribute("mathAvMark") == null ? "-" :
                                        request.getAttribute("mathAvMark")%>
                            </td>
                        </tr>
                        <tr>
                            <td class="my-td-1-1">
                                <fmt:message bundle="${lang}" key="inform"/>
                            </td>
                            <td class="my-td-1-1">
                                <%=request.getAttribute("informAvMark") == null ? "-" :
                                        request.getAttribute("informAvMark")%>
                            </td>
                        </tr>
                        <tr>
                            <td class="my-td-1-1">
                                <fmt:message bundle="${lang}" key="chem"/>
                            </td>
                            <td class="my-td-1-1">
                                <%=request.getAttribute("chemAvMark") == null ? "-" :
                                        request.getAttribute("chemAvMark")%>
                            </td>
                        </tr>
                        <tr>
                            <td class="my-td-1-1">
                                <fmt:message bundle="${lang}" key="phy"/>
                            </td>
                            <td class="my-td-1-1">
                                <%=request.getAttribute("phyAvMark") == null ? "-" :
                                        request.getAttribute("phyAvMark")%>
                            </td>
                        </tr>
                        <tr>
                            <td class="my-td-1-1">
                                <fmt:message bundle="${lang}" key="ru"/>
                            </td>
                            <td class="my-td-1-1">
                                <%=request.getAttribute("rusAvMark") == null ? "-" :
                                        request.getAttribute("rusAvMark")%>
                            </td>
                        </tr>
                        <tr>
                            <td class="my-td-1-1">
                                <fmt:message bundle="${lang}" key="en"/>
                            </td>
                            <td class="my-td-1-1">
                                <%=request.getAttribute("enAvMark") == null ? "-" :
                                        request.getAttribute("enAvMark")%>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

