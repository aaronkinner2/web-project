<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message bundle="${lang}" key="page.main"/><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/fontawesome.css">
    <link rel="stylesheet" href="bootstrap/css/teacherGrades.css">
    <link rel="stylesheet" href="bootstrap/css/common.css">
    <script type="text/javascript" src="bootstrap/js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery.tablesorter.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#student-grade-table").tablesorter();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#my-student-table").tablesorter();
        });
    </script>
    <script type="text/javascript" src="bootstrap/js/jquery.tablesorter.widgets.js"></script>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;"
     href="javascript:void(0)">
    <a class="navBar-brand">
        <img src="bootstrap/css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="teacher-name" id="login">
        <%=session.getAttribute("login")%>
    </div>
    <form autocomplete="off" method="post" action="teacherGrades">
        <input type="hidden" name="command" value="main"/>
        <button type="submit" class="btn-color-light-blue btn btn-navBar btn-block my-3">
            <fmt:message bundle="${lang}" key="button.students"/>
        </button>
    </form>
    <form autocomplete="off" method="post" action="teacherGrades">
        <input type="hidden" name="command" value="profile"/>
        <button type="submit" class="btn-color-orange btn btn-navBar btn-block my-3">
            <fmt:message bundle="${lang}" key="button.profile"/>
        </button>
    </form>
    <form autocomplete="off" method="post" action="teacherGrades">
        <input type="hidden" name="command" value="signOut"/>
        <button type="submit" class="btn-color-red btn btn-navBar btn-block my-3-2">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-4">
            <div class="student-data-panel">
                <h6><fmt:message bundle="${lang}" key="studentData"/></h6>
                <div class="student-profile">
                    <div>
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="userName">
                                    <fmt:message bundle="${lang}" key="login"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="userName">
                                    <%=request.getAttribute("studentLogin") == null ? "" :
                                            request.getAttribute("studentLogin")%>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="name">
                                    <fmt:message bundle="${lang}" key="name"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="name">
                                    <%=request.getAttribute("studentName") == null ? "" :
                                            request.getAttribute("studentName")%>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="surname">
                                    <fmt:message bundle="${lang}" key="surname"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="surname">
                                    <%=request.getAttribute("studentSurname") == null ? "" :
                                            request.getAttribute("studentSurname")%>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="email">
                                    <fmt:message bundle="${lang}" key="email"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="email">
                                    <%=request.getAttribute("studentMail") == null ? "" :
                                            request.getAttribute("studentMail")%>
                                </label>
                            </dd>
                        </dl>
                    </div>
                </div>
                <form autocomplete="off" method="post" action="teacherGrades">
                    <input type="hidden" name="command" value="tasks"/>
                    <button type="submit" class="btn-color-dark-green btn btn-block my-3-6">
                        <fmt:message bundle="${lang}" key="button.tasks"/>
                    </button>
                </form>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="grade-panel">
                <h4><fmt:message bundle="${lang}" key="student.grades"/></h4>
                <form autocomplete="off" method="post" action="teacherGrades">
                    <input type="hidden" name="command" value="page"/>
                    <label>
                        <select class="select-page" name="pageId" id="pageId" size="1">
                            <jsp:useBean id="pages" scope="request" type="java.util.List"/>
                            <c:forEach items="${pages}" var="page">
                                <option value="<c:out value="${page}"/>">
                                    <c:out value="${page}"/>
                                </option>
                            </c:forEach>
                        </select>
                    </label>
                    <button type="submit" class="btn-color-blue btn f4 btn-block my-3-5">
                        <fmt:message bundle="${lang}" key="button.go"/>
                    </button>
                </form>
                <form class="home-delete-grade" autocomplete="off" method="post" action="teacherGrades">
                    <input type="hidden" name="command" value="delete"/>
                    <div class="grade-panel-m">
                        <table class="student-grade-table" id="student-grade-table">
                            <thead>
                            <tr>
                                <th class="th-1">

                                </th>
                                <th class="th-2">
                                    <fmt:message bundle="${lang}" key="gradeMark"/>
                                </th>
                                <th class="th-3">
                                    <fmt:message bundle="${lang}" key="gradeDate"/>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <jsp:useBean id="grades" scope="request" type="java.util.List"/>
                            <c:forEach items="${grades}" var="grade">
                                <tr>
                                    <td class="my-td-1">
                                        <input name="gradeId" id="gradeId" type="radio"
                                               title="<fmt:message bundle="${lang}"
                                                                      key="chooseOneField"/>"
                                               oninvalid="this.setCustomValidity
                                                       ('<fmt:message bundle="${lang}"
                                                                      key="chooseOneField"/>')"
                                               oninput="setCustomValidity('')"
                                               value=<c:out value="${grade.id}"/>>
                                    </td>
                                    <td class="my-td-2">
                                        <c:out value="${grade.mark}"/>
                                    </td>
                                    <td class="my-td-3">
                                        <c:out value="${grade.date}"/>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <button type="submit" class="btn-color-blue btn btn-block my-3-4">
                        <fmt:message bundle="${lang}" key="button.deleteGrade"/>
                    </button>
                </form>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="add-grade-panel">
                <h5><fmt:message bundle="${lang}" key="label.form.addGrade.title"/></h5>
                <form class="home-profile" autocomplete="off" method="post" action="teacherGrades">
                    <input type="hidden" name="command" value="add"/>
                    <div class="add-grade">
                        <dt class="input-label h m">
                            <label class="form-label h6" for="mark">
                                <fmt:message bundle="${lang}" key="mark"/>
                            </label>
                        </dt>
                        <select class="select-panel" name="mark" id="mark" size="1" required>
                            <option disabled>
                                <fmt:message bundle="${lang}" key="chooseMark"/>
                            </option>
                            <option value="1">
                                <fmt:message bundle="${lang}" key="one"/>
                            </option>
                            <option value="2">
                                <fmt:message bundle="${lang}" key="two"/>
                            </option>
                            <option value="3">
                                <fmt:message bundle="${lang}" key="three"/>
                            </option>
                            <option value="4">
                                <fmt:message bundle="${lang}" key="four"/>
                            </option>
                            <option value="5">
                                <fmt:message bundle="${lang}" key="five"/>
                            </option>
                            <option value="6">
                                <fmt:message bundle="${lang}" key="six"/>
                            </option>
                            <option value="7">
                                <fmt:message bundle="${lang}" key="seven"/>
                            </option>
                            <option value="8">
                                <fmt:message bundle="${lang}" key="eight"/>
                            </option>
                            <option value="9">
                                <fmt:message bundle="${lang}" key="nine"/>
                            </option>
                            <option value="10">
                                <fmt:message bundle="${lang}" key="ten"/>
                            </option>
                        </select>
                        <dt class="input-label h m">
                            <label class="form-label h6" for="date">
                                <fmt:message bundle="${lang}" key="gradeDate"/>
                            </label>
                        </dt>
                        <select class="select-panel" name="date" id="date" size="1" required>
                            <option disabled>
                                <fmt:message bundle="${lang}" key="chooseDate"/>
                            </option>
                            <jsp:useBean id="dates" scope="request" type="java.util.List"/>
                            <c:forEach items="${dates}" var="date">
                                <option value=<c:out value="${date}"/>>
                                    <c:out value="${date}"/>
                                </option>
                            </c:forEach>
                        </select>
                        <dt class="input-label h m">
                            <label class="form-label h6">
                                <fmt:message bundle="${lang}" key="textPost"/>
                            </label>
                        </dt>
                        <textarea name="email-text" required
                                  title="<fmt:message bundle="${lang}"
                                                                      key="inputText"/>"
                                  oninvalid="this.setCustomValidity
                                          ('<fmt:message bundle="${lang}"
                                                         key="inputText"/>')"
                                  oninput="setCustomValidity('')">
                        </textarea>
                        <button type="submit" class="btn-color-green btn btn-block my-3-3">
                            <fmt:message bundle="${lang}" key="button.addGrade"/>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
