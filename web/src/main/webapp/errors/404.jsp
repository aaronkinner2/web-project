<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setLocale value="en_US"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message bundle="${lang}" key="page.404.error"/></title>
    <link rel="stylesheet" href="bootstrap/css/404.css">
</head>
<body>
</body>
</html>
