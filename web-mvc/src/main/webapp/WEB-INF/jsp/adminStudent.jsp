<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message bundle="${lang}" key="page.main"/><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/adminStudent.css"/>
    <script type="text/javascript" src="js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="js/jquery.tablesorter.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#student-grade-table").tablesorter();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#my-student-table").tablesorter();
        });
    </script>
    <script type="text/javascript" src="js/jquery.tablesorter.widgets.js"></script>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;"
     href="javascript:void(0)">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="admin-name" id="login"><%=session.getAttribute("login")%>
    </div>
    <form:form autocomplete="off" method="post" action="adminStudent">
        <input type="hidden" name="adminStudent1" value=""/>
        <button type="submit" class="btn-color-light-blue btn btn-navBar btn-block my-3 size-1">
            <fmt:message bundle="${lang}" key="button.students"/>
        </button>
    </form:form>
    <form:form autocomplete="off" method="post" action="/web/singOut">
        <button type="submit" class="btn-color-red btn btn-navBar btn-block my-3-2 size-2">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form:form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-6">
            <div class="student-data-panel">
                <h6><fmt:message bundle="${lang}" key="studentData"/></h6>
                <div class="student-profile">
                    <div>
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="userName">
                                    <fmt:message bundle="${lang}" key="login"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="userName">
                                    <c:choose>
                                        <c:when test="${not empty studentLogin}">
                                            ${studentLogin}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="name">
                                    <fmt:message bundle="${lang}" key="name"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="name">
                                    <c:choose>
                                        <c:when test="${not empty studentName}">
                                            ${studentName}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="surname">
                                    <fmt:message bundle="${lang}" key="surname"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="surname">
                                    <c:choose>
                                        <c:when test="${not empty studentSurname}">
                                            ${studentSurname}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="mail">
                                    <fmt:message bundle="${lang}" key="email"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="mail">
                                    <c:choose>
                                        <c:when test="${not empty studentMail}">
                                            ${studentMail}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="studyClass">
                                    <fmt:message bundle="${lang}" key="class"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="studyClass">
                                    <c:choose>
                                        <c:when test="${not empty studentStudyClass}">
                                            ${studentStudyClass}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </label>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="button-panel">
                <form:form autocomplete="off" method="post" action="adminStudent">
                    <input type="hidden" name="adminStudent2" value=""/>
                    <button type="submit" class="btn-color-green btn btn-block my-3-3">
                        <fmt:message bundle="${lang}" key="button.grades"/>
                    </button>
                </form:form>
                <form:form autocomplete="off" method="post" action="adminStudent">
                    <select class="select-class" name="classId" size="1">
                        <option disabled>
                            <fmt:message bundle="${lang}" key="chooseClass"/>
                        </option>
                        <c:forEach items="${classes}" var="study">
                            <option value="<c:out value="${study.id}"/>">
                                <c:out value="${study.name}"/>
                            </option>
                        </c:forEach>
                        <option value="0">
                            <fmt:message bundle="${lang}" key="noClass"/>
                        </option>
                    </select>
                    <button type="submit" class="btn-color-red-blue btn btn-block my-3-3">
                        <fmt:message bundle="${lang}" key="button.changeClass"/>
                    </button>
                </form:form>
                <form:form autocomplete="off" method="post" action="adminStudent">
                    <input type="hidden" name="command" value="delete"/>
                    <button type="submit" class="btn-color-blue btn btn-block my-3-4">
                        <fmt:message bundle="${lang}" key="button.deleteStudent"/>
                    </button>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>