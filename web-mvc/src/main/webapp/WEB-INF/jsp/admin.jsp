<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <title><fmt:message bundle="${lang}" key="page.main"/><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/admin.css"/>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size"
             alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="admin-name" id="login"><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </div>
    <form:form autocomplete="off" method="post" action="/web/signOut">
        <button type="submit" class="btn btn-color-red btn-navBar btn-block my-3-1 size-1">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form:form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-12">
            <div class="button-panel">
                <form:form autocomplete="off" method="post" action="admin">
                    <input type="hidden" name="admin1" value=""/>
                    <button type="submit" class="btn-color-green btn f4 btn-block my-3-2">
                        <fmt:message bundle="${lang}" key="button.teacher"/>
                    </button>
                </form:form>
                <form:form autocomplete="off" method="post" action="admin">
                    <input type="hidden" name="admin2" value=""/>
                    <button type="submit" class="btn-color-red-blue btn f4 btn-block my-3-2">
                        <fmt:message bundle="${lang}" key="button.noClassStudents"/>
                    </button>
                </form:form>
                <form:form autocomplete="off" method="post" action="admin">
                    <input type="hidden" name="admin3" value=""/>
                    <button type="submit" class="btn-color-orange btn f4 btn-block my-3-2">
                        <fmt:message bundle="${lang}" key="button.classes"/>
                    </button>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>