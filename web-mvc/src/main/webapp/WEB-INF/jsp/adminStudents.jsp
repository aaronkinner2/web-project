<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <title><fmt:message bundle="${lang}" key="page.main"/>
        <%=session.getAttribute("login") == null ? "" :
                session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/adminStudents.css"/>
    <script type="text/javascript" src="js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="js/jquery.tablesorter.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#student-table").tablesorter();
        });
    </script>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="admin-name" id="login"><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </div>
    <form:form autocomplete="off" method="post" action="adminStudents">
        <input type="hidden" name="command" value="main"/>
        <button type="submit" class="btn-color-orange btn btn-navBar btn-block my-3 size-1">
            <fmt:message bundle="${lang}" key="button.mainPage"/>
        </button>
    </form:form>
    <form:form autocomplete="off" method="post" action="/web/signOut">
        <input type="hidden" name="command" value="signOut"/>
        <button type="submit" class="btn btn-color-red btn-navBar btn-block my-3-3 size-2">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form:form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-12">
            <h3><fmt:message bundle="${lang}" key="students"/></h3>
            <form:form autocomplete="off" method="post" action="adminStudents">
                <label>
                    <select class="select-page" name="pageId" id="pageId" size="1">
                        <c:forEach items="${pages}" var="page">
                            <option value="<c:out value="${page}"/>">
                                <c:out value="${page}"/>
                            </option>
                        </c:forEach>
                    </select>
                </label>
                <button type="submit" class="btn-color-blue btn f4 btn-block my-3-4">
                    <fmt:message bundle="${lang}" key="button.go"/>
                </button>
            </form:form>
        </div>
        <div class="col-xl-12">
            <div class="student-panel">
                <form:form autocomplete="off" method="post" action="adminStudents">
                    <div class="student-panel-m">
                        <table class="my-student-table" id="student-table">
                            <thead>
                            <tr>
                                <th class="th-1">

                                </th>
                                <th class="th-2">
                                    <fmt:message bundle="${lang}" key="login"/>
                                </th>
                                <th class="th-3">
                                    <fmt:message bundle="${lang}" key="name"/>
                                </th>
                                <th class="th-4">
                                    <fmt:message bundle="${lang}" key="surname"/>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${students}" var="student">
                                <tr>
                                    <td class="my-td-1">
                                        <button type="submit" name="studentId"
                                                class="btn-color-green btn btn-block my-3-2"
                                                value=<c:out value="${student.id}"/>>
                                            ◉
                                        </button>
                                    </td>
                                    <td class="my-td-2">
                                        <c:out value="${student.login}"/>
                                    </td>
                                    <td class="my-td-3">
                                        <c:out value="${student.userInfo.firstName}"/>
                                    </td>
                                    <td class="my-td-4">
                                        <c:out value="${student.userInfo.lastName}"/>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
