<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message bundle="${lang}" key="page.addTeacher"/></title>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/adminCreateTeacher.css"/>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="admin-name">
        <%=session.getAttribute("login")%>
    </div>
    <form:form autocomplete="off" method="post" action="adminCreateTeacher">
        <button type="submit" class="btn btn-color-orange btn-navBar btn-block my-3 size-1">
            <fmt:message bundle="${lang}" key="button.teacher"/>
        </button>
    </form:form>
    <form:form autocomplete="off" method="post" action="/web/signOut">
        <button type="submit" class="btn btn-navBar btn-color-red btn-block my-3-3 size-2">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form:form>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12">
            <div class="myPanel">
                <h3><fmt:message bundle="${lang}" key="label.form.add.teacher.title"/></h3>
                <form:form class="home-signUp" autocomplete="off" method="post" action="adminCreateTeacher">
                    <div class="username-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="login">
                                    <fmt:message bundle="${lang}" key="login"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="text" name="login" id="login"
                                       class="form-control form-control-lg input-block"
                                       autocomplete="off" required spellcheck="false" pattern="[A-z0-9]{4,18}"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputLoginHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputLoginHint"/>')"
                                       oninput="setCustomValidity('')"
                                       value=<%=request.getAttribute("login") == null ? "" :
                                       request.getAttribute("login")%>>
                            </dd>
                        </dl>
                    </div>
                    <div class="name-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="name">
                                    <fmt:message bundle="${lang}" key="name"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="text" name="name" id="name"
                                       class="form-control form-control-lg input-block" autocomplete="off"
                                       required spellcheck="false"
                                       pattern="{2, 15}"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputNameHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputNameHint"/>')"
                                       oninput="setCustomValidity('')"
                                       value=<%=request.getAttribute("name") == null ? "" :
                                       request.getAttribute("name")%>>
                            </dd>
                        </dl>
                    </div>
                    <div class="surname-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="surname">
                                    <fmt:message bundle="${lang}" key="surname"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="text" name="surname" id="surname"
                                       class="form-control form-control-lg input-block" autocomplete="off"
                                       required spellcheck="false"
                                       pattern="{2, 15}"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputSurnameHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputSurnameHint"/>')"
                                       oninput="setCustomValidity('')"
                                       value=<%=request.getAttribute("surname") == null ? "" :
                                       request.getAttribute("surname")%>>
                            </dd>
                        </dl>
                    </div>
                    <div class="email-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="email">
                                    <fmt:message bundle="${lang}" key="email"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="email" name="email" id="email"
                                       class="form-control form-control-lg input-block" autocomplete="off"
                                       spellcheck="off" required
                                       pattern="([A-z0-9_.-]{1,})@([A-z0-9_.-]{1,}).([A-z]{2,8})"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputEmailHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputEmailHint"/>')"
                                       oninput="setCustomValidity('')"
                                       value=<%= request.getAttribute("email") == null ? "" :
                                       request.getAttribute("email")%>>
                            </dd>
                        </dl>
                    </div>
                    <div class="subject-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5">
                                    <fmt:message bundle="${lang}" key="subject"/>
                                </label>
                            </dt>
                            <dd>
                                <div class="border-class">
                                    <label>
                                        <select class="select-subject" name="subject" id="subject"
                                                size="1">
                                            <option disabled>
                                                <fmt:message bundle="${lang}" key="chooseSubject"/>
                                            </option>
                                            <option selected value="1">
                                                <fmt:message bundle="${lang}" key="math"/>
                                            </option>
                                            <option value="2">
                                                <fmt:message bundle="${lang}" key="inform"/>
                                            </option>
                                            <option value="3">
                                                <fmt:message bundle="${lang}" key="chem"/>
                                            </option>
                                            <option value="4">
                                                <fmt:message bundle="${lang}" key="phy"/>
                                            </option>
                                            <option value="5">
                                                <fmt:message bundle="${lang}" key="ru"/>
                                            </option>
                                            <option value="6">
                                                <fmt:message bundle="${lang}" key="en"/>
                                            </option>
                                        </select>
                                    </label>
                                </div>
                            </dd>
                        </dl>
                    </div>
                    <div class="password-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="password">
                                    <fmt:message bundle="${lang}" key="password"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="password" name="password" id="password"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputPasswordHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputPasswordHint"/>')"
                                       oninput="setCustomValidity('')"
                                       class="form-control form-control-lg input-block" autocomplete="off" required
                                       pattern="[A-z0-9]{8,15}">
                            </dd>
                        </dl>
                    </div>
                    <div class="error-signUp">
                        <c:if test="${not empty error}">
                            ${error}
                        </c:if>
                    </div>
                    <button type="submit" class="btn btn-color-green f4 btn-block">
                        <fmt:message bundle="${lang}" key="button.createAccount"/>
                    </button>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
