<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title>
        <fmt:message bundle="${lang}" key="page.main"/><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/mainStudent.css"/>
    <script type="text/javascript" src="js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="js/jquery.tablesorter.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#my-grades-table").tablesorter();
        });
    </script>
    <script type="text/javascript" src="js/jquery.tablesorter.widgets.js"></script>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="student-name" id="login"><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </div>
    <form:form class="home-profile" autocomplete="off" method="post" action="/web/profile">
        <button type="submit" class="btn-color-orange btn f4 btn-block my-3 btn-navBar size-1">
            <fmt:message bundle="${lang}" key="button.profile"/>
        </button>
    </form:form>
    <form:form class="home-signOut" autocomplete="off" method="post" action="/web/signOut">
        <button type="submit" class="btn-color-red btn f4 btn-block my-3-2 btn-navBar size-2">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form:form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-6">
            <div class="grade-panel">
                <h3><fmt:message bundle="${lang}" key="studentGrades"/></h3>
                <form:form autocomplete="off" method="post" action="student">
                    <label>
                        <select class="select-page" name="pageId" size="1">
                            <c:forEach items="${pages}" var="page">
                                <option value="<c:out value="${page}"/>">
                                    <c:out value="${page}"/>
                                </option>
                            </c:forEach>
                        </select>
                    </label>
                    <button type="submit" class="btn-color-blue btn btn-block my-3-4">
                        <fmt:message bundle="${lang}" key="button.go"/>
                    </button>
                </form:form>
                <div class="grade-panel-m">
                    <table class="my-grades-table" id="my-grades-table">
                        <thead>
                        <tr>
                            <th class="th-1">
                                <fmt:message bundle="${lang}" key="gradeDate"/>
                            </th>
                            <th class="th-2">
                                <fmt:message bundle="${lang}" key="gradeSubject"/>
                            </th>
                            <th class="th-3">
                                <fmt:message bundle="${lang}" key="gradeMark"/>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${grades}" var="grade">
                            <tr>
                                <td class="my-td-1">
                                    <c:out value="${grade.date}"/>
                                </td>
                                <td class="my-td-2">
                                    <c:out value="${grade.subject.name}"/>
                                </td>
                                <td class="my-td-3">
                                    <c:out value="${grade.mark}"/>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="teacher-panel">
                <h5>
                    <c:choose>
                        <c:when test="${not empty className}">
                            ${className}
                        </c:when>
                        <c:otherwise>
                            -
                        </c:otherwise>
                    </c:choose>
                </h5>
                <table class="my-teacher-table">
                    <tr>
                        <td class="my-td-1-1-0">
                            <fmt:message bundle="${lang}" key="math"/>
                        </td>
                        <td class="my-td-1-1-1">
                            <c:choose>
                                <c:when test="${not empty tMathNameSurname}">
                                    ${tMathNameSurname}
                                </c:when>
                                <c:otherwise>
                                    ##########
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="my-td-1-1-2">
                            <c:choose>
                                <c:when test="${not empty tMathMail}">
                                    ${tMathMail}
                                </c:when>
                                <c:otherwise>
                                    ##########
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1-0">
                            <fmt:message bundle="${lang}" key="inform"/>
                        </td>
                        <td class="my-td-1-1-1">
                            <c:choose>
                                <c:when test="${not empty tInformNameSurname}">
                                    ${tInformNameSurname}
                                </c:when>
                                <c:otherwise>
                                    ##########
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="my-td-1-1-2">
                            <c:choose>
                                <c:when test="${not empty tInformMail}">
                                    ${tInformMail}
                                </c:when>
                                <c:otherwise>
                                    ##########
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1-0">
                            <fmt:message bundle="${lang}" key="chem"/>
                        </td>
                        <td class="my-td-1-1-1">
                            <c:choose>
                                <c:when test="${not empty tChemNameSurname}">
                                    ${tChemNameSurname}
                                </c:when>
                                <c:otherwise>
                                    ##########
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="my-td-1-1-2">
                            <c:choose>
                                <c:when test="${not empty tChemMail}">
                                    ${tChemMail}
                                </c:when>
                                <c:otherwise>
                                    ##########
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1-0">
                            <fmt:message bundle="${lang}" key="phy"/>
                        </td>
                        <td class="my-td-1-1-1">
                            <c:choose>
                                <c:when test="${not empty tPhyNameSurname}">
                                    ${tPhyNameSurname}
                                </c:when>
                                <c:otherwise>
                                    ##########
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="my-td-1-1-2">
                            <c:choose>
                                <c:when test="${not empty tPhyMail}">
                                    ${tPhyMail}
                                </c:when>
                                <c:otherwise>
                                    ##########
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1-0">
                            <fmt:message bundle="${lang}" key="ru"/>
                        </td>
                        <td class="my-td-1-1-1">
                            <c:choose>
                                <c:when test="${not empty tRuNameSurname}">
                                    ${tRuNameSurname}
                                </c:when>
                                <c:otherwise>
                                    ##########
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="my-td-1-1-2">
                            <c:choose>
                                <c:when test="${not empty tRuMail}">
                                    ${tRuMail}
                                </c:when>
                                <c:otherwise>
                                    ##########
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1-0">
                            <fmt:message bundle="${lang}" key="en"/>
                        </td>
                        <td class="my-td-1-1-1">
                            <c:choose>
                                <c:when test="${not empty tEnNameSurname}">
                                    ${tEnNameSurname}
                                </c:when>
                                <c:otherwise>
                                    ##########
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="my-td-1-1-2">
                            <c:choose>
                                <c:when test="${not empty tEnMail}">
                                    ${tEnMail}
                                </c:when>
                                <c:otherwise>
                                    ##########
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="row">
                <div class="grade-choose-panel">
                    <h4><fmt:message bundle="${lang}" key="gradesSubject"/></h4>
                    <form:form autocomplete="off" method="post" action="student">
                        <div class="change-grade">
                            <label>
                                <select class="select-s" name="subjectId" size="1">
                                    <option disabled>
                                        <fmt:message bundle="${lang}" key="chooseSubject"/>
                                    </option>
                                    <option value="1">
                                        <fmt:message bundle="${lang}" key="math"/>
                                    </option>
                                    <option value="2">
                                        <fmt:message bundle="${lang}" key="inform"/>
                                    </option>
                                    <option value="3">
                                        <fmt:message bundle="${lang}" key="chem"/>
                                    </option>
                                    <option value="4">
                                        <fmt:message bundle="${lang}" key="phy"/>
                                    </option>
                                    <option value="5">
                                        <fmt:message bundle="${lang}" key="ru"/>
                                    </option>
                                    <option value="6">
                                        <fmt:message bundle="${lang}" key="en"/>
                                    </option>
                                    <option selected value="0">
                                        <fmt:message bundle="${lang}" key="all"/>
                                    </option>
                                </select>
                            </label>
                            <button type="submit" class="btn-color-green btn btn-block my-3-3">
                                <fmt:message bundle="${lang}" key="button.change"/>
                            </button>
                        </div>
                    </form:form>
                    <form:form autocomplete="off" method="post" action="student">
                        <div class="get-tasks">
                            <label>
                                <select class="select-s" name="pageSubjectId" size="1">
                                    <option disabled>
                                        <fmt:message bundle="${lang}" key="chooseSubject"/>
                                    </option>
                                    <option value="1">
                                        <fmt:message bundle="${lang}" key="math"/>
                                    </option>
                                    <option value="2">
                                        <fmt:message bundle="${lang}" key="inform"/>
                                    </option>
                                    <option value="3">
                                        <fmt:message bundle="${lang}" key="chem"/>
                                    </option>
                                    <option value="4">
                                        <fmt:message bundle="${lang}" key="phy"/>
                                    </option>
                                    <option value="5">
                                        <fmt:message bundle="${lang}" key="ru"/>
                                    </option>
                                    <option value="6">
                                        <fmt:message bundle="${lang}" key="en"/>
                                    </option>
                                </select>
                            </label>
                            <button type="submit" class="btn-color-plum btn btn-block my-3-3">
                                <fmt:message bundle="${lang}" key="button.tasks"/>
                            </button>
                        </div>
                    </form:form>
                </div>
                <div class="average-panel">
                    <h6><fmt:message bundle="${lang}" key="averageGrades"/></h6>
                    <table class="my-aver-grades-table">
                        <tr>
                            <td class="my-td-1-0">
                                <fmt:message bundle="${lang}" key="common"/>
                            </td>
                            <td class="my-td-1-0">
                                <c:choose>
                                    <c:when test="${not empty commonAvMark}">
                                        ${commonAvMark}
                                    </c:when>
                                    <c:otherwise>
                                        -
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <td class="my-td-1-1">
                                <fmt:message bundle="${lang}" key="math"/>
                            </td>
                            <td class="my-td-1-1">
                                <c:choose>
                                    <c:when test="${not empty mathAvMark}">
                                        ${mathAvMark}
                                    </c:when>
                                    <c:otherwise>
                                        -
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <td class="my-td-1-1">
                                <fmt:message bundle="${lang}" key="inform"/>
                            </td>
                            <td class="my-td-1-1">
                                <c:choose>
                                    <c:when test="${not empty informAvMark}">
                                        ${informAvMark}
                                    </c:when>
                                    <c:otherwise>
                                        -
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <td class="my-td-1-1">
                                <fmt:message bundle="${lang}" key="chem"/>
                            </td>
                            <td class="my-td-1-1">
                                <c:choose>
                                    <c:when test="${not empty chemAvMark}">
                                        ${chemAvMark}
                                    </c:when>
                                    <c:otherwise>
                                        -
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <td class="my-td-1-1">
                                <fmt:message bundle="${lang}" key="phy"/>
                            </td>
                            <td class="my-td-1-1">
                                <c:choose>
                                    <c:when test="${not empty phyAvMark}">
                                        ${phyAvMark}
                                    </c:when>
                                    <c:otherwise>
                                        -
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <td class="my-td-1-1">
                                <fmt:message bundle="${lang}" key="ru"/>
                            </td>
                            <td class="my-td-1-1">
                                <c:choose>
                                    <c:when test="${not empty rusAvMark}">
                                        ${rusAvMark}
                                    </c:when>
                                    <c:otherwise>
                                        -
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <td class="my-td-1-1">
                                <fmt:message bundle="${lang}" key="en"/>
                            </td>
                            <td class="my-td-1-1">
                                <c:choose>
                                    <c:when test="${not empty enAvMark}">
                                        ${enAvMark}
                                    </c:when>
                                    <c:otherwise>
                                        -
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

