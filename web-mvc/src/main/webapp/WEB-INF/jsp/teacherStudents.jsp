<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message bundle="${lang}" key="page.main"/>
        <%=session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/teacherStudents.css"/>
    <script type="text/javascript" src="js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="js/jquery.tablesorter.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#student-grade-table").tablesorter();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#my-student-table").tablesorter();
        });
    </script>
    <script type="text/javascript" src="js/jquery.tablesorter.widgets.js"></script>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;"
     href="javascript:void(0)">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="teacher-name" id="login"><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </div>
    <form:form autocomplete="off" method="post" action="teacherStudents">
        <input type="hidden" name="main1" value="main"/>
        <button type="submit" class="btn-color-light-blue btn btn-navBar btn-block my-3 size-1">
            <fmt:message bundle="${lang}" key="button.teacher.classes"/>
        </button>
    </form:form>
    <form:form autocomplete="off" method="post" action="/web/profile">
        <button type="submit" class="btn-color-orange btn btn-navBar btn-block my-3 size-2">
            <fmt:message bundle="${lang}" key="button.profile"/>
        </button>
    </form:form>
    <form:form autocomplete="off" method="post" action="/web/signOut">
        <button type="submit" class="btn-color-red btn btn-navBar btn-block my-3-2 size-3">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form:form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-12">
            <div class="student-panel">
                <h3><fmt:message bundle="${lang}" key="label.form.your.students.title"/></h3>
                <form:form autocomplete="off" method="post" action="teacherStudents">
                    <label>
                        <select class="select-page" name="pageId" id="pageId" size="1">
                            <c:forEach items="${pages}" var="page">
                                <option value="<c:out value="${page}"/>">
                                    <c:out value="${page}"/>
                                </option>
                            </c:forEach>
                        </select>
                    </label>
                    <button type="submit" class="btn-color-blue btn f4 btn-block my-3-4">
                        <fmt:message bundle="${lang}" key="button.go"/>
                    </button>
                </form:form>
                <form:form autocomplete="off" method="post" action="teacherStudents">
                    <div class="student-panel-m">
                        <table class="my-student-table" id="my-student-table">
                            <thead>
                            <tr>
                                <th class="th-1">

                                </th>
                                <th class="th-2">
                                    <fmt:message bundle="${lang}" key="login"/>
                                </th>
                                <th class="th-3">
                                    <fmt:message bundle="${lang}" key="name"/>
                                </th>
                                <th class="th-4">
                                    <fmt:message bundle="${lang}" key="surname"/>
                                </th>
                                <th class="th-5">
                                    <fmt:message bundle="${lang}" key="email"/>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${students}" var="student">
                                <tr>
                                    <td class="my-td-1">
                                        <input type="hidden" name="studentId" value=<c:out value="${student.id}"/>>
                                        <button type="submit" class="btn-color-blue btn btn-block my-3-5">
                                            ◉
                                        </button>
                                    </td>
                                    <td class="my-td-2">
                                        <c:out value="${student.login}"/>
                                    </td>
                                    <td class="my-td-3">
                                        <c:out value="${student.userInfo.firstName}"/>
                                    </td>
                                    <td class="my-td-4">
                                        <c:out value="${student.userInfo.lastName}"/>
                                    </td>
                                    <td class="my-td-5">
                                        <c:out value="${student.userInfo.mail}"/>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
