<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message bundle="${lang}" key="page.profile"/></title>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/profile.css"/>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;" href="javascript:void(0)">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="user-name mar" id="login"><%= session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </div>
    <form:form autocomplete="off" method="post" action="/web/main">
        <button type="submit" class="btn btn-color-orange btn-navBar btn-block my-3-5 mar size-1">
            <fmt:message bundle="${lang}" key="button.mainPage"/>
        </button>
    </form:form>
    <form:form autocomplete="off" method="post" action="/web/signOut">
        <button type="submit" class="btn btn-red-color btn-navBar btn btn-block mar size-2">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form:form>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-6">
            <div class="myPanel">
                <h3><fmt:message bundle="${lang}" key="label.form.profile.title"/></h3>
                <div class="user-profile">
                    <div class="username-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="userName">
                                    <fmt:message bundle="${lang}" key="login"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="userName">
                                    <%=session.getAttribute("login") == null ? "" :
                                            session.getAttribute("login")%>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div class="name-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="name">
                                    <fmt:message bundle="${lang}" key="name"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="name">
                                    <c:choose>
                                        <c:when test="${not empty name}">
                                            ${name}
                                        </c:when>
                                        <c:otherwise>
                                            -
                                        </c:otherwise>
                                    </c:choose>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div class="surname-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="surname">
                                    <fmt:message bundle="${lang}" key="surname"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6" id="surname">
                                    <c:choose>
                                        <c:when test="${not empty surname}">
                                            ${surname}
                                        </c:when>
                                        <c:otherwise>
                                            -
                                        </c:otherwise>
                                    </c:choose>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div class="email-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="email" id="email">
                                    <fmt:message bundle="${lang}" key="email"/>
                                </label>
                            </dt>
                            <dd>
                                <label class="form-label h6">
                                    <c:choose>
                                        <c:when test="${not empty email}">
                                            ${email}
                                        </c:when>
                                        <c:otherwise>
                                            -
                                        </c:otherwise>
                                    </c:choose>
                                </label>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="myPanel-2">
                <h3><fmt:message bundle="${lang}" key="label.form.changePassword.title"/></h3>
                <form:form class="home-profile-1" autocomplete="off" method="post" action="/web/profile">
                    <input type="hidden" name="command" value="changePassword"/>
                    <div class="old-password-panel">
                        <dt class="input-label">
                            <label class="form-label h7" for="oldPassword">
                                <fmt:message bundle="${lang}" key="oldPassword"/>
                            </label>
                        </dt>
                        <dd>
                            <input type="password" name="oldPassword" id="oldPassword"
                                   class="form-control input-block" autocomplete="off" required
                                   title="<fmt:message bundle="${lang}"
                                                                      key="inputOldPassword"/>"
                                   oninvalid="this.setCustomValidity
                                           ('<fmt:message bundle="${lang}"
                                                          key="inputOldPassword"/>')"
                                   oninput="setCustomValidity('')">
                        </dd>
                    </div>
                    <div class="new-password-panel">
                        <dt class="input-label">
                            <label class="form-label h7" for="newPassword">
                                <fmt:message bundle="${lang}" key="newPassword"/>
                            </label>
                        </dt>
                        <dd>
                            <input type="password" name="newPassword" id="newPassword"
                                   class="form-control input-block" autocomplete="off" required
                                   pattern="[A-z0-9]{8,15}"
                                   title="<fmt:message bundle="${lang}"
                                                                      key="inputNewPasswordHint"/>"
                                   oninvalid="this.setCustomValidity
                                           ('<fmt:message bundle="${lang}"
                                                          key="inputNewPasswordHint"/>')"
                                   oninput="setCustomValidity('')">
                        </dd>
                    </div>
                    <div class="change-form">
                        <%=request.getAttribute("error") == null ? "" : request.getAttribute("error")%>
                    </div>
                    <button type="submit" class="btn btn-green-color f4 btn-block my-3 w-1">
                        <fmt:message bundle="${lang}" key="button.savePassword"/>
                    </button>
                </form:form>
            </div>
            <div class="myPanel-3">
                <h3><fmt:message bundle="${lang}" key="changeEmail"/></h3>
                <form:form class="home-profile-2" autocomplete="off" method="post" action="/web/profile">
                    <div class="email-panel">
                        <dt class="input-label">
                            <label class="form:form-label h7" for="newEmail">
                                <fmt:message bundle="${lang}" key="newEmail"/>
                            </label>
                        </dt>
                        <dd>
                            <input type="email" name="newEmail" id="newEmail"
                                   pattern="([A-z0-9_.-]{1,})@([A-z0-9_.-]{1,}).([A-z]{2,8})"
                                   class="form-control input-block" autocomplete="off" required
                                   title="<fmt:message bundle="${lang}"
                                                                      key="inputEmailHint"/>"
                                   oninvalid="this.setCustomValidity
                                           ('<fmt:message bundle="${lang}"
                                                          key="inputEmailHint"/>')"
                                   oninput="setCustomValidity('')">
                        </dd>
                    </div>
                    <button type="submit" class="btn btn-orange-color btn-block my-3-3 w-1">
                        <fmt:message bundle="${lang}" key="button.saveEmail"/>
                    </button>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
