<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message bundle="${lang}" key="page.main"/><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/adminCreateClass.css"/>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;" href="javascript:void(0)">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="admin-name"><%=session.getAttribute("login")%>
    </div>
    <form:form autocomplete="off" method="post" action="adminCreateClass">
        <input type="hidden" name="adminClasses1" value=""/>
        <button type="submit" class="btn-color-red btn btn-navBar btn-block btn-margin size-1">
            <fmt:message bundle="${lang}" key="button.classes"/>
        </button>
    </form:form>
    <form:form autocomplete="off" method="post" action="/web/signOut">
        <button type="submit" class="btn-color-red btn btn-navBar btn-block btn-margin size-2">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form:form>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12">
            <h3><fmt:message bundle="${lang}" key="createClass"/></h3>
            <div class="myPanel">
                <form:form class="form-create" autocomplete="off" method="post" action="adminCreateClass">
                    <div class="username-panel">
                        <dl class="form-group mt-0 my-form">
                            <dt class="input-label">
                                <label class="form-label h5">
                                    <fmt:message bundle="${lang}" key="className"/>
                                </label>
                            </dt>
                            <dd>
                                <label>
                                    <input name="className"
                                           class="form-control form-control-lg input-block" autocomplete="off"
                                           spellcheck="false" required>
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0 my-form">
                            <dt class="input-label">
                                <label class="form-label h5">
                                    <fmt:message bundle="${lang}" key="mathTeacher"/>
                                </label>
                            </dt>
                            <dd>
                                <div class="border-class">
                                    <label>
                                        <select class="select-s" name="mathTeacherId" size="1">
                                            <option disabled>
                                                <fmt:message bundle="${lang}" key="chooseMathTeacher"/>
                                            </option>
                                            <c:forEach items="${mathTeachers}" var="mTeacher">
                                                <option value="<c:out value="${mTeacher.id}"/>">
                                                    <c:out value="${mTeacher.userInfo.firstName}"/> <c:out
                                                        value="${mTeacher.userInfo.lastName}"/>
                                                </option>
                                            </c:forEach>
                                            <option value="0">
                                                <fmt:message bundle="${lang}" key="chooseNoTeacher"/>
                                            </option>
                                        </select>
                                    </label>
                                </div>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0 my-form">
                            <dt class="input-label">
                                <label class="form-label h5">
                                    <fmt:message bundle="${lang}" key="informTeacher"/>
                                </label>
                            </dt>
                            <dd>
                                <div class="border-class">
                                    <label>
                                        <select class="select-s" name="informTeacherId" size="1">
                                            <option disabled>
                                                <fmt:message bundle="${lang}" key="chooseInformTeacher"/>
                                            </option>
                                            <c:forEach items="${informTeachers}" var="iTeacher">
                                                <option value="<c:out value="${iTeacher.id}"/>">
                                                    <c:out value="${iTeacher.userInfo.firstName}"/> <c:out
                                                        value="${iTeacher.userInfo.lastName}"/>
                                                </option>
                                            </c:forEach>
                                            <option value="0">
                                                <fmt:message bundle="${lang}" key="chooseNoTeacher"/>
                                            </option>
                                        </select>
                                    </label>
                                </div>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0 my-form">
                            <dt class="input-label">
                                <label class="form-label h5">
                                    <fmt:message bundle="${lang}" key="chemTeacher"/>
                                </label>
                            </dt>
                            <dd>
                                <div class="border-class">
                                    <label>
                                        <select class="select-s" name="chemTeacherId" size="1">
                                            <option disabled>
                                                <fmt:message bundle="${lang}" key="chooseChemTeacher"/>
                                            </option>
                                            <c:forEach items="${chemTeachers}" var="cTeacher">
                                                <option value="<c:out value="${cTeacher.id}"/>">
                                                    <c:out value="${cTeacher.userInfo.firstName}"/> <c:out
                                                        value="${cTeacher.userInfo.lastName}"/>
                                                </option>
                                            </c:forEach>
                                            <option value="0">
                                                <fmt:message bundle="${lang}" key="chooseNoTeacher"/>
                                            </option>
                                        </select>
                                    </label>
                                </div>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0 my-form">
                            <dt class="input-label">
                                <label class="form-label h5">
                                    <fmt:message bundle="${lang}" key="phyTeacher"/>
                                </label>
                            </dt>
                            <dd>
                                <div class="border-class">
                                    <label>
                                        <select class="select-s" name="phyTeacherId" size="1">
                                            <option disabled>
                                                <fmt:message bundle="${lang}" key="choosePhyTeacher"/>
                                            </option>
                                            <c:forEach items="${phyTeachers}" var="pTeacher">
                                                <option value="<c:out value="${pTeacher.id}"/>">
                                                    <c:out value="${pTeacher.userInfo.firstName}"/> <c:out
                                                        value="${pTeacher.userInfo.lastName}"/>
                                                </option>
                                            </c:forEach>
                                            <option value="0">
                                                <fmt:message bundle="${lang}" key="chooseNoTeacher"/>
                                            </option>
                                        </select>
                                    </label>
                                </div>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0 my-form">
                            <dt class="input-label">
                                <label class="form-label h5">
                                    <fmt:message bundle="${lang}" key="ruTeacher"/>
                                </label>
                            </dt>
                            <dd>
                                <div class="border-class">
                                    <label>
                                        <select class="select-s" name="ruTeacherId" size="1">
                                            <option disabled>
                                                <fmt:message bundle="${lang}" key="chooseRuTeacher"/>
                                            </option>
                                            <c:forEach items="${ruTeachers}" var="rTeacher">
                                                <option value="<c:out value="${rTeacher.id}"/>">
                                                    <c:out value="${rTeacher.userInfo.firstName}"/> <c:out
                                                        value="${rTeacher.userInfo.lastName}"/>
                                                </option>
                                            </c:forEach>
                                            <option value="0">
                                                <fmt:message bundle="${lang}" key="chooseNoTeacher"/>
                                            </option>
                                        </select>
                                    </label>
                                </div>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <dl class="form-group mt-0 my-form">
                            <dt class="input-label">
                                <label class="form-label h5">
                                    <fmt:message bundle="${lang}" key="phyTeacher"/>
                                </label>
                            </dt>
                            <dd>
                                <div class="border-class">
                                    <label>
                                        <select class="select-s" name="enTeacherId" size="1">
                                            <option disabled>
                                                <fmt:message bundle="${lang}" key="chooseEnTeacher"/>
                                            </option>
                                            <c:forEach items="${enTeachers}" var="eTeacher">
                                                <option value="<c:out value="${eTeacher.id}"/>">
                                                    <c:out value="${eTeacher.userInfo.firstName}"/> <c:out
                                                        value="${eTeacher.userInfo.lastName}"/>
                                                </option>
                                            </c:forEach>
                                            <option value="0">
                                                <fmt:message bundle="${lang}" key="chooseNoTeacher"/>
                                            </option>
                                        </select>
                                    </label>
                                </div>
                            </dd>
                        </dl>
                    </div>
                    <div class="error">
                        <c:if test="${not empty error}">
                            ${error}
                        </c:if>
                    </div>
                    <button type="submit" class="btn-color-op btn btn-block my-3-4">
                        <fmt:message bundle="${lang}" key="button.setTeacher"/>
                    </button>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
