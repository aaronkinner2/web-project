<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <title><fmt:message bundle="${lang}" key="page.main"/><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/adminTeachers.css"/>
    <script type="text/javascript" src="js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="js/jquery.tablesorter.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#teachers-table").tablesorter();
        });
    </script>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size"
             alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="admin-name" id="login"><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </div>
    <form:form class="prof" autocomplete="off" method="post" action="adminTeachers">
        <input type="hidden" name="adminTeachers1" value=""/>
        <button type="submit" class="btn-color-orange btn btn-navBar btn-block my-3 size-1">
            <fmt:message bundle="${lang}" key="button.mainPage"/>
        </button>
    </form:form>
    <form:form class="home-signOut" autocomplete="off" method="post" action="/web/signOut">
        <button type="submit" class="btn btn-color-red btn-navBar btn-block my-3-3 size-2">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form:form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-12">
            <h3><fmt:message bundle="${lang}" key="teachers"/></h3>
            <form:form autocomplete="off" method="post" action="adminTeachers">
                <label>
                    <select class="select-page" name="pageId" id="pageId" size="1">
                        <jsp:useBean id="pages" scope="request" type="java.util.List"/>
                        <c:forEach items="${pages}" var="page">
                            <option selected value="<c:out value="${page}"/>">
                                <c:out value="${page}"/>
                            </option>
                        </c:forEach>
                    </select>
                </label>
                <button type="submit" class="btn-color-blue btn f4 btn-block my-3-4">
                    <fmt:message bundle="${lang}" key="button.go"/>
                </button>
            </form:form>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="teachers-panel">
                <form:form autocomplete="off" method="post" action="adminTeachers">
                    <div class="teachers-panel-m">
                        <table class="teachers-table" id="teachers-table">
                            <thead>
                            <tr>
                                <th class="th-1">

                                </th>
                                <th class="th-2">
                                    <fmt:message bundle="${lang}" key="login"/>
                                </th>
                                <th class="th-3">
                                    <fmt:message bundle="${lang}" key="name"/>
                                </th>
                                <th class="th-4">
                                    <fmt:message bundle="${lang}" key="surname"/>
                                </th>
                                <th class="th-5">
                                    <fmt:message bundle="${lang}" key="email"/>
                                </th>
                                <th class="th-6">
                                    <fmt:message bundle="${lang}" key="subject"/>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${teachers}" var="teacher">
                                <tr>
                                    <td class="my-td-1">
                                        <button type="submit" class="btn-color-green btn btn-block my-3-2"
                                                name="teacherId" value=<c:out value="${teacher.id}"/>>
                                            ❖
                                        </button>
                                    </td>
                                    <td class="my-td-2">
                                        <c:out value="${teacher.login}"/>
                                    </td>
                                    <td class="my-td-3">
                                        <c:out value="${teacher.userInfo.firstName}"/>
                                    </td>
                                    <td class="my-td-4">
                                        <c:out value="${teacher.userInfo.lastName}"/>
                                    </td>
                                    <td class="my-td-5">
                                        <c:out value="${teacher.userInfo.mail}"/>
                                    </td>
                                    <td class="my-td-6">
                                        <c:out value="${teacher.subject.name}"/>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </form:form>
                <form:form autocomplete="off" method="post" action="adminTeachers">
                    <input type="hidden" name="adminTeachers2" value=""/>
                    <button type="submit" class="btn-color-red-blue btn btn-block my-3-5">
                        <fmt:message bundle="${lang}" key="button.createNewTeacher"/>
                    </button>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
