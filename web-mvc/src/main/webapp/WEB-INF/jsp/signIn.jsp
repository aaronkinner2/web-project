<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <title><fmt:message bundle="${lang}" key="page.login"/></title>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/signIn.css"/>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <form:form autocomplete="off" method="post" action="signIn">
        <input type="hidden" value="" name="signIn1">
        <button type="submit" class="btn-color-red btn btn-block my-3-2 btn-navBar size-1">
            <fmt:message bundle="${lang}" key="button.back"/>
        </button>
    </form:form>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-7 my-auto">
            <div class="description">
                <h1><fmt:message bundle="${lang}" key="label.signIn.title"/></h1>
                <p>
                <h2><fmt:message bundle="${lang}" key="label.signIn.main"/></h2>
            </div>
        </div>
        <div class="col-5">
            <div class="myPanel">
                <h3><fmt:message bundle="${lang}" key="label.form.title"/></h3>
                <form:form class="signIn-part" autocomplete="off" method="post" action="signIn">
                    <div class="username-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="login">
                                    <fmt:message bundle="${lang}" key="login"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="text" name="login" id="login"
                                       class="form-control form-control-lg input-block" autocomplete="off" required
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputLogin"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputLogin"/>')"
                                       oninput="setCustomValidity('')"
                                       value=<%=request.getAttribute("loginError") == null ? "" :
                                       request.getAttribute("loginError")%>>
                            </dd>
                        </dl>
                    </div>
                    <div class="password-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="password">
                                    <fmt:message bundle="${lang}" key="password"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="password" name="password" id="password"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputPassword"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputPassword"/>')"
                                       oninput="setCustomValidity('')"
                                       class="form-control form-control-lg input-block" autocomplete="off" required>
                            </dd>
                        </dl>
                    </div>
                    <div class="error-signIn">
                        <c:if test="${not empty error}">
                            ${error}
                        </c:if>
                    </div>
                    <button type="submit" class="btn btn-color-blue f4 btn-block my-3">
                        <fmt:message bundle="${lang}" key="button.signIn"/>
                    </button>
                </form:form>
                <form:form class="signUp-part" autocomplete="off" method="post" action="signIn">
                    <input type="hidden" value="" name="signIn2">
                    <button type="submit" class="btn btn-color-green btn-block f4">
                        <fmt:message bundle="${lang}" key="button.signUp"/>
                    </button>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

