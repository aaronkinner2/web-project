<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="UTF-16">
    <title><fmt:message bundle="${lang}" key="page.signUp"/></title>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/signUp.css"/>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <form:form autocomplete="off" method="post" action="signUp">
        <input type="hidden" name="command" value="signOut"/>
        <button type="submit" class="btn-color-red btn btn-block my-3-2 btn-navBar size-1">
            <fmt:message bundle="${lang}" key="button.back"/>
        </button>
    </form:form>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12">
            <div class="description">
                <fmt:message bundle="${lang}" key="signUp"/>
            </div>
            <div class="myPanel">
                <form:form class="home-signUp" autocomplete="off" method="post" action="/web/signUp">
                    <input type="hidden" name="command" value="register"/>
                    <div class="username-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="login">
                                    <fmt:message bundle="${lang}" key="login"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="text" name="login" id="login"
                                       class="form-control form-control-lg input-block"
                                       autocomplete="off" required spellcheck="false" pattern="[A-z0-9]{4,18}"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputLoginHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputLoginHint"/>')"
                                       oninput="setCustomValidity('')"
                                       value=<%=request.getAttribute("login") == null ? "" :
                                       request.getAttribute("login")%>>
                            </dd>
                        </dl>
                    </div>
                    <div class="name-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="name">
                                    <fmt:message bundle="${lang}" key="name"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="text" name="name" id="name"
                                       class="form-control form-control-lg input-block" autocomplete="off"
                                       required spellcheck="false"
                                       pattern="{2, 15}"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputNameHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputNameHint"/>')"
                                       oninput="setCustomValidity('')"
                                       value=<%=request.getAttribute("name") == null ? "" :
                                       request.getAttribute("name")%>>
                            </dd>
                        </dl>
                    </div>
                    <div class="surname-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="surname">
                                    <fmt:message bundle="${lang}" key="surname"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="text" name="surname" id="surname"
                                       class="form-control form-control-lg input-block" autocomplete="off"
                                       required spellcheck="false"
                                       pattern="{2, 15}"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputSurnameHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputSurnameHint"/>')"
                                       oninput="setCustomValidity('')"
                                       value=<%=request.getAttribute("surname") == null ? "" :
                                       request.getAttribute("surname")%>>
                            </dd>
                        </dl>
                    </div>
                    <div class="email-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="email">
                                    <fmt:message bundle="${lang}" key="email"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="email" name="email" id="email"
                                       class="form-control form-control-lg input-block" autocomplete="off"
                                       spellcheck="false" required
                                       pattern="([A-z0-9_.-]{1,})@([A-z0-9_.-]{1,}).([A-z]{2,8})"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputEmailHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputEmailHint"/>')"
                                       oninput="setCustomValidity('')"
                                       value=<%= request.getAttribute("email") == null ? "" :
                                       request.getAttribute("email")%>>
                            </dd>
                        </dl>
                    </div>
                    <div class="password-panel">
                        <dl class="form-group mt-0">
                            <dt class="input-label">
                                <label class="form-label h5" for="password">
                                    <fmt:message bundle="${lang}" key="password"/>
                                </label>
                            </dt>
                            <dd>
                                <input type="password" name="password" id="password"
                                       title="<fmt:message bundle="${lang}"
                                                                      key="inputPasswordHint"/>"
                                       oninvalid="this.setCustomValidity
                                               ('<fmt:message bundle="${lang}"
                                                              key="inputPasswordHint"/>')"
                                       oninput="setCustomValidity('')"
                                       class="form-control form-control-lg input-block" autocomplete="off" required
                                       pattern="[A-z0-9]{8,15}">
                            </dd>
                        </dl>
                    </div>
                    <div class="create-error">
                        <c:if test="${not empty error}">
                            ${error}
                        </c:if>
                    </div>
                    <button type="submit" class="btn btn-blue-color btn-block f4 my-10">
                        <fmt:message bundle="${lang}" key="button.createAccount"/>
                    </button>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
