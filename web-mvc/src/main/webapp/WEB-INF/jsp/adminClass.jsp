<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message bundle="${lang}" key="page.main"/>
        <%=session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/adminClass.css"/>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;" href="javascript:void(0)">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="admin-name"><%=session.getAttribute("login")%>
    </div>
    <form:form autocomplete="off" method="post" action="adminClass">
        <input type="hidden" name="adminClass1" value=""/>
        <button type="submit" class="btn btn-color-light-blue btn-navBar btn-block my-3 size-1">
            <fmt:message bundle="${lang}" key="button.classes"/>
        </button>
    </form:form>
    <form:form autocomplete="off" method="post" action="/web/signOut">
        <button type="submit" class="btn btn-color-red btn-navBar btn-block my-3-3 size-2">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form:form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-9">
            <div class="teacher-panel">
                <h3><fmt:message bundle="${lang}" key="class"/> <c:choose>
                    <c:when test="${not empty className}">
                        ${className}
                    </c:when>
                    <c:otherwise>
                        ✂
                    </c:otherwise>
                </c:choose>
                </h3>
                <div class="teacher-panel-l">
                    <div class="teacher-panel-m">
                        <table class="teacher-table" id="student-grade-table">
                            <thead>
                            <tr>
                                <th class="th-1">
                                    <fmt:message bundle="${lang}" key="subject"/>
                                </th>
                                <th class="th-2">
                                    <fmt:message bundle="${lang}" key="name"/>
                                </th>
                                <th class="th-3">
                                    <fmt:message bundle="${lang}" key="surname"/>
                                </th>
                                <th class="th-4">
                                    <fmt:message bundle="${lang}" key="email"/>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="my-td-1">
                                    <fmt:message bundle="${lang}" key="math"/>
                                </td>
                                <td class="my-td-2">
                                    <c:choose>
                                        <c:when test="${not empty tMathName}">
                                            ${tMathName}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="my-td-3">
                                    <c:choose>
                                        <c:when test="${not empty tMathSurname}">
                                            ${tMathSurname}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="my-td-4">
                                    <c:choose>
                                        <c:when test="${not empty tMathMail}">
                                            ${tMathMail}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                            <tr>
                                <td class="my-td-1">
                                    <fmt:message bundle="${lang}" key="inform"/>
                                </td>
                                <td class="my-td-2">
                                    <c:choose>
                                        <c:when test="${not empty tInformName}">
                                            ${tInformName}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="my-td-3">
                                    <c:choose>
                                        <c:when test="${not empty tInformSurname}">
                                            ${tInformSurname}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="my-td-4">
                                    <c:choose>
                                        <c:when test="${not empty tInformMail}">
                                            ${tInformMail}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                            <tr>
                                <td class="my-td-1">
                                    <fmt:message bundle="${lang}" key="chem"/>
                                </td>
                                <td class="my-td-2">
                                    <c:choose>
                                        <c:when test="${not empty tChemName}">
                                            ${tChemName}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="my-td-3">
                                    <c:choose>
                                        <c:when test="${not empty tChemSurname}">
                                            ${tChemSurname}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="my-td-4">
                                    <c:choose>
                                        <c:when test="${not empty tChemMail}">
                                            ${tChemMail}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                            <tr>
                                <td class="my-td-1">
                                    <fmt:message bundle="${lang}" key="phy"/>
                                </td>
                                <td class="my-td-2">
                                    <c:choose>
                                        <c:when test="${not empty tPhyName}">
                                            ${tPhyName}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="my-td-3">
                                    <c:choose>
                                        <c:when test="${not empty tPhySurname}">
                                            ${tPhySurname}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="my-td-4">
                                    <c:choose>
                                        <c:when test="${not empty tPhyMail}">
                                            ${tPhyMail}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                            <tr>
                                <td class="my-td-1">
                                    <fmt:message bundle="${lang}" key="ru"/>
                                </td>
                                <td class="my-td-2">
                                    <c:choose>
                                        <c:when test="${not empty tRuName}">
                                            ${tRuName}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="my-td-3">
                                    <c:choose>
                                        <c:when test="${not empty tRuSurname}">
                                            ${tRuSurname}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="my-td-4">
                                    <c:choose>
                                        <c:when test="${not empty tRuMail}">
                                            ${tRuMail}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                            <tr>
                                <td class="my-td-1">
                                    <fmt:message bundle="${lang}" key="en"/>
                                </td>
                                <td class="my-td-2">
                                    <c:choose>
                                        <c:when test="${not empty tEnName}">
                                            ${tEnName}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="my-td-3">
                                    <c:choose>
                                        <c:when test="${not empty tEnSurname}">
                                            ${tEnSurname}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="my-td-4">
                                    <c:choose>
                                        <c:when test="${not empty tEnMail}">
                                            ${tEnMail}
                                        </c:when>
                                        <c:otherwise>
                                            ##########
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xl-3">
            <div class="button-panel">
                <form:form autocomplete="off" method="post" action="adminClass">
                    <input type="hidden" name="adminClass2" value=""/>
                    <button type="submit" class="btn-color-green btn btn-block my-3-2">
                        <fmt:message bundle="${lang}" key="button.students"/>
                    </button>
                </form:form>
                <form:form autocomplete="off" method="post" action="adminClass">
                    <input type="hidden" name="adminClass3" value=""/>
                    <button type="submit" class="btn-color-red-blue btn btn-block my-3-2">
                        <fmt:message bundle="${lang}" key="button.changeClassTeachers"/>
                    </button>
                </form:form>
                <form:form autocomplete="off" method="post" action="adminClass">
                    <input type="hidden" name="adminClass4" value=""/>
                    <button type="submit" class="btn-color-orange btn btn-block my-3-2">
                        <fmt:message bundle="${lang}" key="button.deleteClass"/>
                    </button>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

