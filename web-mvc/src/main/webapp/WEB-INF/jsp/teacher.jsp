<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message bundle="${lang}" key="page.main"/><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/mainTeacher.css"/>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;" href="javascript:void(0)">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="teacher-name" id="login"><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </div>
    <form:form autocomplete="off" method="post" action="/web/profile">
        <button type="submit" class="btn-color-orange btn btn-navBar btn-block my-3 size-1">
            <fmt:message bundle="${lang}" key="button.profile"/>
        </button>
    </form:form>
    <form:form autocomplete="off" method="post" action="/web/signOut">
        <button type="submit" class="btn-color-red btn btn-navBar btn-block my-3-2 size-2">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form:form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-12">
            <div class="class-panel">
                <h3><fmt:message bundle="${lang}" key="yourClasses"/></h3>
                <div class="class-panel-m">
                    <form:form autocomplete="off" method="post" action="teacher">
                        <select class="select-class" name="classId" id="classId" size="1">
                            <option disabled>
                                <fmt:message bundle="${lang}" key="chooseClass"/>
                            </option>
                            <c:forEach items="${classes}" var="study">
                                <option value="<c:out value="${study.id}"/>">
                                    <c:out value="${study.name}"/>
                                </option>
                            </c:forEach>
                        </select>
                        <button type="submit" class="btn-color-blue btn f4 btn-block my-3-4">
                            <fmt:message bundle="${lang}" key="button.goOver"/>
                        </button>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
