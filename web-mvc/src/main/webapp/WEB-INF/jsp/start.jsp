<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<fmt:setLocale value="en_US"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/start.css"/>
    <title><fmt:message bundle="${lang}" key="page.index"/></title>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt=""/>
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12">
            <div class="lang-panel">
                <h3>Choose language/ Выберите язык</h3>
            </div>
            <div class="myPanel">
                <form:form class="cl-btn" autocomplete="off" method="post" action="start">
                    <input type="hidden" name="locale" value="enLocale"/>
                    <button type="submit" class="btn btn-orange-color btn-block">
                        <fmt:message bundle="${lang}" key="language.english"/>
                    </button>
                </form:form>
                <form:form class="cl-btn-2" autocomplete="off" method="post" action="start">
                    <input type="hidden" name="locale" value="ruLocale"/>
                    <button type="submit" class="btn btn-blue-color btn-block">
                        <fmt:message bundle="${lang}" key="language.russian"/>
                    </button>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

