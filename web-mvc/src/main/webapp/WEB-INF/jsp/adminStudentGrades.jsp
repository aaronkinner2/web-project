<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang" var="lang"/>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message bundle="${lang}" key="page.main"/><%=session.getAttribute("login") == null ? "" :
            session.getAttribute("login")%>
    </title>
    <link rel="stylesheet" type="text/css" href="css/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/common/all.css"/>
    <link rel="stylesheet" type="text/css" href="css/adminStudentGrades.css"/>
    <script type="text/javascript" src="js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="js/jquery.tablesorter.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#grades-table").tablesorter();
        });
    </script>
    <script type="text/javascript" src="js/jquery.tablesorter.widgets.js"></script>
</head>
<body>
<nav class="navbar navBar-inverse navBar-fixed-top" style="background-color: #1c1c1c;" href="javascript:void(0)">
    <a class="navBar-brand">
        <img src="css/icons/graduation-hat.png" class="d-inline-block align-top img-size" alt="">
        <fmt:message bundle="${lang}" key="label.iisg"/>
    </a>
    <div class="admin-name" id="login"><%=session.getAttribute("login")%>
    </div>
    <form:form autocomplete="off" method="post" action="adminStudentGrades">
        <button type="submit" class="btn-color-light-blue btn btn-navBar btn-block my-3 size-1">
            <fmt:message bundle="${lang}" key="button.studentProfile"/>
        </button>
    </form:form>
    <form:form autocomplete="off" method="post" action="/web/signOut">
        <input type="hidden" name="command" value="signOut"/>
        <button type="submit" class="btn-color-red btn btn-navBar btn-block my-3-2 size-2">
            <fmt:message bundle="${lang}" key="button.signOut"/>
        </button>
    </form:form>
</nav>
<div class="container-fluid pos">
    <div class="row">
        <div class="col-xl-7">
            <div class="grade-panel">
                <h4><fmt:message bundle="${lang}" key="student.grades"/></h4>
                <form:form autocomplete="off" method="post" action="adminStudentGrades">
                    <div class="grade-panel-m">
                        <table class="grades-table" id="grades-table">
                            <thead>
                            <tr>
                                <th class="th-1">

                                </th>
                                <th class="th-2">
                                    <fmt:message bundle="${lang}" key="gradeMark"/>
                                </th>
                                <th class="th-3">
                                    <fmt:message bundle="${lang}" key="gradeSubject"/>
                                </th>
                                <th class="th-4">
                                    <fmt:message bundle="${lang}" key="gradeDate"/>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${grades}" var="grade">
                                <tr>
                                    <td class="my-td-1">
                                        <button type="submit" class="btn-color-blue btn btn-block my-3-4"
                                                name="gradeId" value=<c:out value="${grade.id}"/>>
                                            ❖
                                        </button>
                                    </td>
                                    <td class="my-td-2">
                                        <c:out value="${grade.mark}"/>
                                    </td>
                                    <td class="my-td-3" height="26">
                                        <c:out value="${grade.subject.name}"/>
                                    </td>
                                    <td class="my-td-4">
                                        <c:out value="${grade.date}"/>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </form:form>
            </div>
        </div>
        <div class="col-xl-5">
            <div class="pad"></div>
            <div class="choose-panel">
                <h5><fmt:message bundle="${lang}" key="tablePage"/></h5>
                <form:form autocomplete="off" method="post" action="adminStudentGrades">
                    <div class="change">
                        <label>
                            <select class="select-s" name="pageId" size="1">
                                <c:forEach items="${pages}" var="page">
                                    <option value="<c:out value="${page}"/>">
                                        <c:out value="${page}"/>
                                    </option>
                                </c:forEach>
                            </select>
                        </label>
                        <button type="submit" class="btn-color-brown btn btn-block my-3-3">
                            <fmt:message bundle="${lang}" key="button.go"/>
                        </button>
                    </div>
                </form:form>
            </div>
            <div class="choose-panel">
                <h5><fmt:message bundle="${lang}" key="gradesSubject"/></h5>
                <form:form autocomplete="off" method="post" action="adminStudentGrades">
                    <div class="change">
                        <label>
                            <select class="select-s" name="subjectId" size="1">
                                <option disabled>
                                    <fmt:message bundle="${lang}" key="chooseSubject"/>
                                </option>
                                <option selected value="0">
                                    <fmt:message bundle="${lang}" key="all"/>
                                </option>
                                <option value="1">
                                    <fmt:message bundle="${lang}" key="math"/>
                                </option>
                                <option value="2">
                                    <fmt:message bundle="${lang}" key="inform"/>
                                </option>
                                <option value="3">
                                    <fmt:message bundle="${lang}" key="chem"/>
                                </option>
                                <option value="4">
                                    <fmt:message bundle="${lang}" key="phy"/>
                                </option>
                                <option value="5">
                                    <fmt:message bundle="${lang}" key="ru"/>
                                </option>
                                <option value="6">
                                    <fmt:message bundle="${lang}" key="en"/>
                                </option>
                            </select>
                        </label>
                        <button type="submit" class="btn-color-green btn btn-block my-3-3">
                            <fmt:message bundle="${lang}" key="button.change"/>
                        </button>
                    </div>
                </form:form>
            </div>
            <div class="average-panel">
                <h6><fmt:message bundle="${lang}" key="averageAdminGrades"/></h6>
                <table class="aver-grades-table">
                    <tr>
                        <td class="my-td-1-0">
                            <fmt:message bundle="${lang}" key="common"/>
                        </td>
                        <td class="my-td-1-0">
                            <%=request.getAttribute("commonAvMark") == null ? "-" :
                                    request.getAttribute("commonAvMark")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1">
                            <fmt:message bundle="${lang}" key="math"/>
                        </td>
                        <td class="my-td-1-1">
                            <%=request.getAttribute("mathAvMark") == null ? "-" :
                                    request.getAttribute("mathAvMark")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1">
                            <fmt:message bundle="${lang}" key="inform"/>
                        </td>
                        <td class="my-td-1-1">
                            <%=request.getAttribute("informAvMark") == null ? "-" :
                                    request.getAttribute("informAvMark")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1">
                            <fmt:message bundle="${lang}" key="chem"/>
                        </td>
                        <td class="my-td-1-1">
                            <%=request.getAttribute("chemAvMark") == null ? "-" :
                                    request.getAttribute("chemAvMark")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1">
                            <fmt:message bundle="${lang}" key="phy"/>
                        </td>
                        <td class="my-td-1-1">
                            <%=request.getAttribute("phyAvMark") == null ? "-" :
                                    request.getAttribute("phyAvMark")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1">
                            <fmt:message bundle="${lang}" key="ru"/>
                        </td>
                        <td class="my-td-1-1">
                            <%=request.getAttribute("rusAvMark") == null ? "-" :
                                    request.getAttribute("rusAvMark")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="my-td-1-1">
                            <fmt:message bundle="${lang}" key="en"/>
                        </td>
                        <td class="my-td-1-1">
                            <%=request.getAttribute("enAvMark") == null ? "-" :
                                    request.getAttribute("enAvMark")%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
