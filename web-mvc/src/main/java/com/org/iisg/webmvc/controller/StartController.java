package com.org.iisg.webmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

import static com.org.iisg.webmvc.parameter.ControllerParameter.*;
import static com.org.iisg.webmvc.parameter.PageParameter.REDIRECT_VALUE;
import static com.org.iisg.webmvc.parameter.PageParameter.SIGN_IN_PAGE;

@Controller
public class StartController {

    @RequestMapping(path = "/start", method = RequestMethod.GET)
    public void start() {
    }

    @RequestMapping(path = "/start", method = RequestMethod.POST, params = "locale")
    public String goToSignInPage(@RequestParam("locale") String locale, HttpSession session) {
        if (locale != NULL_VALUE) {
            if (locale.equals(EN_LOCALE_PARAM)) {
                session.setAttribute(LOCALE, EN_LOCALE);
            } else if (locale.equals(RU_LOCALE_PARAM)) {
                session.setAttribute(LOCALE, RU_LOCALE);
            }
        }
        return REDIRECT_VALUE + SIGN_IN_PAGE;
    }
}
