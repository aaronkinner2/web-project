package com.org.iisg.webmvc.controller;

import com.org.iisg.service.impl.manager.ControllerServiceManager;
import com.org.iisg.service.mail.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

import static com.org.iisg.webmvc.parameter.ControllerParameter.*;
import static com.org.iisg.webmvc.parameter.ErrorMessages.SIGN_UP_INCORRECT_LOGIN_EN_MESSAGE;
import static com.org.iisg.webmvc.parameter.ErrorMessages.SIGN_UP_INCORRECT_LOGIN_RU_MESSAGE;
import static com.org.iisg.webmvc.parameter.PageParameter.REDIRECT_VALUE;
import static com.org.iisg.webmvc.parameter.PageParameter.SIGN_IN_PAGE;

@Controller
public class SignUpController {

    @Autowired
    private ControllerServiceManager serviceManager;

    private final MailService mailService = MailService.getInstance();

    @RequestMapping(path = "/signUp", method = RequestMethod.GET)
    public void start() {
    }

    @RequestMapping(path = "/signUp", method = RequestMethod.POST)
    public String redirectToSignInPage() {
        return REDIRECT_VALUE + SIGN_IN_PAGE;
    }

    @RequestMapping(
            path = "/signUp",
            method = RequestMethod.POST,
            params = {"login", "name", "surname", "email", "password"}
    )
    public ModelAndView createAccount(@RequestParam("login") String login, @RequestParam("name") String name,
                                      @RequestParam("surname") String surname, @RequestParam("email") String email,
                                      @RequestParam("password") String password, ModelAndView modelAndView,
                                      HttpSession session) {
        if (serviceManager.getUserService().createStudent(login, password, name, surname, email)) {
            mailService.sendSuccessStudentSignUp(login, email);
            return new ModelAndView(SIGN_IN_PAGE);
        } else {
            modelAndView.addObject(
                    PARAM_ERROR,
                    session.getAttribute(LOCALE).equals(EN_LOCALE)
                            ? SIGN_UP_INCORRECT_LOGIN_EN_MESSAGE
                            : SIGN_UP_INCORRECT_LOGIN_RU_MESSAGE
            );
            return modelAndView;
        }
    }
}
