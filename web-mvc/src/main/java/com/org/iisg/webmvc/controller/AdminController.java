package com.org.iisg.webmvc.controller;

import com.org.iisg.service.impl.manager.ControllerServiceManager;
import com.org.iisg.service.mail.MailService;
import com.org.iisg.webmvc.filler.PageFiller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;

import static com.org.iisg.webmvc.parameter.ControllerParameter.*;
import static com.org.iisg.webmvc.parameter.PageParameter.ADMIN_CREATE_TEACHER_PAGE;
import static com.org.iisg.webmvc.parameter.PageParameter.ADMIN_PAGE;

@Controller
public class AdminController {

    @Autowired
    private ControllerServiceManager serviceManager;

    private final MailService mailService = MailService.getInstance();

    @RequestMapping(path = "/admin", method = RequestMethod.GET)
    public void start() {
    }

    @RequestMapping(path = "/admin", method = RequestMethod.POST, params = "admin1")
    public ModelAndView viewTeachersPage(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminTeachersPage(serviceManager, modelAndView, session, DEFAULT_PAGE);
        return modelAndView;
    }

    @RequestMapping(path = "/admin", method = RequestMethod.POST, params = "admin2")
    public ModelAndView viewNoClassStudentsPage(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminStudentsPage(serviceManager, modelAndView, session, DEFAULT_PAGE, NO_CLASS_ID);
        return modelAndView;
    }

    @RequestMapping(path = "/admin", method = RequestMethod.POST, params = "admin3")
    public ModelAndView viewClassesPage() {
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminClassesPage(serviceManager, modelAndView);
        return modelAndView;
    }

    @RequestMapping(path = "/adminStudents", method = RequestMethod.POST)
    public ModelAndView returnFromAdminStudents(HttpSession session) {
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        ModelAndView modelAndView = new ModelAndView();
        if (classId.equals(NO_CLASS_ID)) {
            modelAndView.setViewName(ADMIN_PAGE);
        } else {
            PageFiller.fillAdminClassPage(serviceManager, modelAndView, session, classId);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/adminStudents", method = RequestMethod.POST, params = "pageId")
    public ModelAndView changePageStudentsPage(@RequestParam("pageId") Integer pageId, HttpSession session) {
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminStudentsPage(serviceManager, modelAndView, session, pageId, classId);
        return modelAndView;
    }

    @RequestMapping(path = "/adminStudents", method = RequestMethod.POST, params = "studentId")
    public ModelAndView viewStudentPage(@RequestParam("studentId") Long studentId, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminStudentPage(serviceManager, modelAndView, session, studentId);
        return modelAndView;
    }

    @RequestMapping(path = "/adminStudent", method = RequestMethod.POST, params = "adminStudent1")
    public ModelAndView returnFromStudentPage(HttpSession session) {
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminStudentsPage(serviceManager, modelAndView, session, DEFAULT_PAGE, classId);
        return modelAndView;
    }

    @RequestMapping(path = "/adminStudent", method = RequestMethod.POST, params = "adminStudent2")
    public ModelAndView viewStudentGrades(HttpSession session) {
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminStudentGradesPage(
                serviceManager, modelAndView,
                session, DEFAULT_PAGE,
                studentId, ALL_SUBJECTS_ID
        );
        return modelAndView;
    }

    @RequestMapping(path = "/adminStudent", method = RequestMethod.POST)
    public ModelAndView deleteStudent(HttpSession session) {
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        serviceManager.getUserService().deleteUser(studentId);
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminStudentsPage(serviceManager, modelAndView, session, DEFAULT_PAGE, classId);
        return modelAndView;
    }

    @RequestMapping(path = "/adminStudent", method = RequestMethod.POST, params = "classId")
    public ModelAndView changeStudentClass(@RequestParam("classId") Long classId, HttpSession session) {
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        serviceManager.getUserService().changeClass(studentId, classId);
        session.setAttribute(PARAM_CLASS_ID, classId);
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminStudentPage(serviceManager, modelAndView, session, studentId);
        return modelAndView;
    }

    @RequestMapping(path = "/adminStudentGrades", method = RequestMethod.POST, params = "gradeId")
    public ModelAndView deleteGrade(@RequestParam("gradeId") Long gradeId, HttpSession session) {
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        serviceManager.getGradeService().deleteGrade(gradeId);
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminStudentGradesPage(
                serviceManager, modelAndView,
                session, DEFAULT_PAGE,
                studentId, subjectId
        );
        return modelAndView;
    }

    @RequestMapping(path = "/adminStudentGrades", method = RequestMethod.POST, params = "pageId")
    public ModelAndView changePageStudentGrades(@RequestParam("pageId") Integer pageId, HttpSession session) {
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminStudentGradesPage(
                serviceManager, modelAndView,
                session, pageId,
                studentId, subjectId
        );
        return modelAndView;
    }

    @RequestMapping(path = "/adminStudentGrades", method = RequestMethod.POST, params = "subjectId")
    public ModelAndView changeSubjectStudentGrades(@RequestParam("subjectId") Long subjectId, HttpSession session) {
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminStudentGradesPage(
                serviceManager, modelAndView,
                session, DEFAULT_PAGE,
                studentId, subjectId
        );
        return modelAndView;
    }

    @RequestMapping(path = "/adminStudentGrades", method = RequestMethod.POST)
    public ModelAndView backOnAdminStudentPage(HttpSession session) {
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminStudentPage(serviceManager, modelAndView, session, studentId);
        return modelAndView;
    }

    @RequestMapping(path = "/adminTeachers", method = RequestMethod.POST, params = "adminTeachers1")
    public ModelAndView backOnAdminPage() {
        return new ModelAndView(ADMIN_PAGE);
    }

    @RequestMapping(path = "/adminTeachers", method = RequestMethod.POST, params = "adminTeachers2")
    public ModelAndView viewCreateTeacherPage() {
        return new ModelAndView(ADMIN_CREATE_TEACHER_PAGE);
    }

    @RequestMapping(path = "/adminTeachers", method = RequestMethod.POST, params = "pageId")
    public ModelAndView changePageOnTeachersPage(@RequestParam("pageId") Integer pageId, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminTeachersPage(serviceManager, modelAndView, session, pageId);
        return modelAndView;
    }

    @RequestMapping(path = "/adminTeachers", method = RequestMethod.POST, params = "teacherId")
    public ModelAndView deleteTeacher(@RequestParam("teacherId") Long teacherId, HttpSession session) {
        serviceManager.getUserService().deleteUser(teacherId);
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminTeachersPage(serviceManager, modelAndView, session, DEFAULT_PAGE);
        return modelAndView;
    }

    @RequestMapping(path = "/adminCreateTeacher", method = RequestMethod.POST)
    public ModelAndView backOnTeachersPage(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminTeachersPage(serviceManager, modelAndView, session, DEFAULT_PAGE);
        return modelAndView;
    }

    @RequestMapping(
            path = "/adminCreateTeacher",
            method = RequestMethod.POST,
            params = {
                    "login",
                    "name",
                    "surname",
                    "email",
                    "subject",
                    "password"
            }
    )
    public ModelAndView createTeacher(@RequestParam("login") String login, @RequestParam("name") String name,
                                      @RequestParam("surname") String surname, @RequestParam("email") String email,
                                      @RequestParam("subject") Long subjectId, @RequestParam("password") String password,
                                      HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        if (serviceManager.getUserService().createTeacher(login, password, name, surname, email, subjectId)) {
            mailService.sendSuccessTeacherSignUp(login, password, email);
            PageFiller.fillAdminTeachersPage(serviceManager, modelAndView, session, DEFAULT_PAGE);
        } else {
            PageFiller.fillCreateTeacherPage(modelAndView, session, login, name, surname, email);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/adminClasses", method = RequestMethod.POST, params = "adminClasses1")
    public ModelAndView viewAdminPage() {
        return new ModelAndView(ADMIN_PAGE);
    }

    @RequestMapping(path = "/adminClasses", method = RequestMethod.POST, params = "adminClasses2")
    public ModelAndView viewCreateClassPage(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillCreateClassPage(serviceManager, modelAndView, session);
        return modelAndView;
    }

    @RequestMapping(path = "/adminClasses", method = RequestMethod.POST, params = "adminClasses3")
    public ModelAndView breakQuarter() {
        serviceManager.getGradeService().deleteAllGrades();
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminClassesPage(serviceManager, modelAndView);
        return modelAndView;
    }

    @RequestMapping(path = "/adminClasses", method = RequestMethod.POST, params = "classId")
    public ModelAndView viewClassPage(@RequestParam("classId") Long classId, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminClassPage(serviceManager, modelAndView, session, classId);
        return modelAndView;
    }

    @RequestMapping(path = "/adminCreateClass", method = RequestMethod.POST)
    public ModelAndView backOnClassesPage() {
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminClassesPage(serviceManager, modelAndView);
        return modelAndView;
    }

    @RequestMapping(
            path = "/adminCreateClass",
            method = RequestMethod.POST,
            params = {
                    "className",
                    "mathTeacherId",
                    "informTeacherId",
                    "chemTeacherId",
                    "phyTeacherId",
                    "ruTeacherId",
                    "enTeacherId"
            }
    )
    public ModelAndView createClass(@RequestParam("className") String className,
                                    @RequestParam("mathTeacherId") Long mathTeacherId,
                                    @RequestParam("informTeacherId") Long informTeacherId,
                                    @RequestParam("chemTeacherId") Long chemTeacherId,
                                    @RequestParam("phyTeacherId") Long phyTeacherId,
                                    @RequestParam("ruTeacherId") Long ruTeacherId,
                                    @RequestParam("enTeacherId") Long enTeacherId,
                                    HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        if (createStudyClass(
                className,
                Arrays.asList(mathTeacherId, informTeacherId, chemTeacherId, phyTeacherId, ruTeacherId, enTeacherId)
        )) {
            PageFiller.fillAdminClassesPage(serviceManager, modelAndView);
        } else {
            PageFiller.fillCreateClassPage(serviceManager, modelAndView, session);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/adminClass", method = RequestMethod.POST, params = "adminClass1")
    public ModelAndView returnOnClassesPage() {
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminClassesPage(serviceManager, modelAndView);
        return modelAndView;
    }

    @RequestMapping(path = "/adminClass", method = RequestMethod.POST, params = "adminClass2")
    public ModelAndView viewClassStudents(HttpSession session) {
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminStudentsPage(serviceManager, modelAndView, session, DEFAULT_PAGE, classId);
        return modelAndView;
    }

    @RequestMapping(path = "/adminClass", method = RequestMethod.POST, params = "adminClass3")
    public ModelAndView viewChangeClassPage(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillChangeClassPage(serviceManager, modelAndView, session);
        return modelAndView;
    }

    @RequestMapping(path = "/adminClass", method = RequestMethod.POST, params = "adminClass4")
    public ModelAndView deleteClass(HttpSession session) {
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        serviceManager.getStudyClassService().deleteClass(classId);
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminClassesPage(serviceManager, modelAndView);
        return modelAndView;
    }

    @RequestMapping(path = "/adminChangeClass", method = RequestMethod.POST)
    public ModelAndView backOnClassPage(HttpSession session) {
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        ModelAndView modelAndView = new ModelAndView();
        PageFiller.fillAdminClassPage(serviceManager, modelAndView, session, classId);
        return modelAndView;
    }

    @RequestMapping(
            path = "/adminChangeClass",
            method = RequestMethod.POST,
            params = {
                    "mathTeacherId",
                    "informTeacherId",
                    "chemTeacherId",
                    "phyTeacherId",
                    "ruTeacherId",
                    "enTeacherId"
            }
    )
    public ModelAndView changeClass(@RequestParam("mathTeacherId") Long mathTeacherId,
                                    @RequestParam("informTeacherId") Long informTeacherId,
                                    @RequestParam("chemTeacherId") Long chemTeacherId,
                                    @RequestParam("phyTeacherId") Long phyTeacherId,
                                    @RequestParam("ruTeacherId") Long ruTeacherId,
                                    @RequestParam("enTeacherId") Long enTeacherId,
                                    HttpSession session) {
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        ModelAndView modelAndView = new ModelAndView();
        serviceManager.getStudyClassService().changeClass(
                classId,
                Arrays.asList(mathTeacherId, informTeacherId, chemTeacherId, phyTeacherId, ruTeacherId, enTeacherId)
        );
        PageFiller.fillAdminClassPage(serviceManager, modelAndView, session, classId);
        return modelAndView;
    }

    private boolean createStudyClass(String name, List<Long> ids) {
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i).equals(ERROR_ID)) {
                ids.set(i, null);
            }
        }
        return serviceManager.getStudyClassService().createStudyClass(name, ids);
    }
}
