package com.org.iisg.webmvc.filler;

import com.org.iisg.model.Grade;
import com.org.iisg.model.StudyClassData;
import com.org.iisg.model.User;
import com.org.iisg.service.impl.manager.ControllerServiceManager;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

import static com.org.iisg.webmvc.parameter.ControllerParameter.*;
import static com.org.iisg.webmvc.parameter.ErrorMessages.*;
import static com.org.iisg.webmvc.parameter.PageParameter.*;

public class PageFiller {

    public static void fillStudentPage(ControllerServiceManager serviceManager, ModelAndView modelAndView,
                                       HttpSession session, int page, Long subjectId, Long studentId) {
        List<Grade> grades = serviceManager.getGradeService().getGradesByUserId(
                studentId,
                session.getAttribute(LOCALE).equals(RU_LOCALE)
        );
        List<Double> subjectMarks = serviceManager.getGradeService().getAverageMark(grades);
        for (int i = 0; i <= SUBJECT_COUNT; i++) {
            modelAndView.addObject(MARK_PARAMS.get(i), subjectMarks.get(i));
        }
        if (!subjectId.equals(ALL_SUBJECTS_ID)) {
            grades = grades.stream().filter(g -> g.getSubject().getId().equals(subjectId)).collect(Collectors.toList());
        }
        modelAndView.addObject(PARAM_PAGES, serviceManager.getGradeService().getGradeTablePages(grades));
        modelAndView.addObject(PARAM_GRADES, serviceManager.getGradeService().getCurrentPageGrades(grades, page));
        StudyClassData classData = serviceManager.getStudyClassService().getClassDataByStudentId(studentId);
        for (int i = 0; i < SUBJECT_COUNT; i++) {
            if (classData == NULL_VALUE) {
                modelAndView.addObject(TEACHER_NAME_SURNAME_PARAMS.get(i), NULL_VALUE);
                modelAndView.addObject(TEACHER_MAIL_PARAMS.get(i), NULL_VALUE);
            } else if (classData.getTeachers().get(i) == NULL_VALUE) {
                modelAndView.addObject(TEACHER_NAME_SURNAME_PARAMS.get(i), NULL_VALUE);
                modelAndView.addObject(TEACHER_MAIL_PARAMS.get(i), NULL_VALUE);
            } else {
                modelAndView.addObject(
                        TEACHER_NAME_SURNAME_PARAMS.get(i),
                        classData.getTeachers().get(i).getUserInfo().getCutFullName()
                );
                modelAndView.addObject(
                        TEACHER_MAIL_PARAMS.get(i),
                        classData.getTeachers().get(i).getUserInfo().getMail()
                );
            }
        }
        modelAndView.addObject(PARAM_STUDY_CLASS_NAME, classData == NULL_VALUE ? "No class" : classData.getName());
        session.setAttribute(PARAM_SUBJECT_ID, subjectId);
        modelAndView.setViewName(STUDENT_PAGE);
    }

    public static void fillTeacherPage(ControllerServiceManager serviceManager, ModelAndView modelAndView,
                                       HttpSession session, Long teacherId, Long subjectId) {
        session.setAttribute(PARAM_SUBJECT_ID, subjectId);
        modelAndView.addObject(
                PARAM_CLASSES,
                serviceManager.getStudyClassService().getClassesByTeacherIdAndSubjectId(teacherId, subjectId)
        );
        modelAndView.setViewName(TEACHER_PAGE);
    }

    public static void fillProfilePage(ControllerServiceManager serviceManager, Long userId, ModelAndView modelAndView) {
        User user = serviceManager.getUserService().getUser(userId);
        modelAndView.addObject(PARAM_NAME, user.getUserInfo().getFirstName());
        modelAndView.addObject(PARAM_SURNAME, user.getUserInfo().getLastName());
        modelAndView.addObject(PARAM_EMAIL, user.getUserInfo().getMail());
        modelAndView.setViewName(PROFILE_PAGE);
    }

    public static void fillStudentTasksPage(ControllerServiceManager serviceManager, ModelAndView modelAndView,
                                            HttpSession session, int page, Long subjectId) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        modelAndView.addObject(
                PARAM_TASKS,
                serviceManager.getTaskService().getTasksByStudentId(userId, subjectId, page)
        );
        modelAndView.addObject(
                PARAM_PAGES,
                serviceManager.getTaskService().getStudentTaskTablePages(userId, subjectId)
        );
        session.setAttribute(PARAM_SUBJECT_ID, subjectId);
        modelAndView.addObject(PARAM_SUBJECT, serviceManager.getTaskService().getRuSubjectName(subjectId));
        modelAndView.setViewName(STUDENT_TASKS_PAGE);
    }

    public static void fillTeacherStudentsPage(ControllerServiceManager serviceManager, ModelAndView modelAndView,
                                               HttpSession session, int page, Long classId) {
        List<User> students = serviceManager.getUserService().getClassStudents(classId, page);
        List<Integer> pages = serviceManager.getUserService().getClassStudentTablePages(classId);
        modelAndView.addObject(PARAM_STUDENTS, students);
        modelAndView.addObject(PARAM_PAGES, pages);
        session.setAttribute(PARAM_CLASS_ID, classId);
        modelAndView.setViewName(TEACHER_STUDENTS);
    }

    public static void fillTeacherGradesPage(ControllerServiceManager serviceManager, ModelAndView modelAndView,
                                             HttpSession session, int page, Long studentId, Long subjectId, Long classId) {
        User user = serviceManager.getUserService().getUser(studentId);
        if (user == NULL_VALUE) {
            fillTeacherStudentsPage(serviceManager, modelAndView, session, DEFAULT_PAGE, classId);
            modelAndView.setViewName(TEACHER_STUDENTS);
        } else {
            session.setAttribute(PARAM_STUDENT_ID, user.getId());
            modelAndView.addObject(PARAM_STUDENT_LOGIN, user.getLogin());
            modelAndView.addObject(PARAM_STUDENT_NAME, user.getUserInfo().getFirstName());
            modelAndView.addObject(PARAM_STUDENT_SURNAME, user.getUserInfo().getLastName());
            modelAndView.addObject(PARAM_STUDENT_MAIL, user.getUserInfo().getMail());
            List<Grade> grades = serviceManager.getGradeService().getGradesByUserIdAndSubjectId(
                    studentId,
                    subjectId,
                    session.getAttribute(LOCALE).equals(RU_LOCALE)
            );
            modelAndView.addObject(PARAM_PAGES, serviceManager.getGradeService().getGradeTablePages(grades));
            modelAndView.addObject(PARAM_GRADES, serviceManager.getGradeService().getCurrentPageGrades(grades, page));
            modelAndView.addObject(PARAM_DATES, serviceManager.getGradeService().getGradeNearbyDates());
            modelAndView.setViewName(TEACHER_GRADES);
        }
    }

    public static void fillTeacherTasksPage(ControllerServiceManager serviceManager, ModelAndView modelAndView,
                                            HttpSession session, int page, Long studentId, Long subjectId) {
        if (serviceManager.getUserService().isAccountExists(studentId)) {
            modelAndView.addObject(
                    PARAM_TASKS, serviceManager.getTaskService().getTasksByStudentId(studentId, subjectId, page)
            );
            modelAndView.addObject(
                    PARAM_PAGES,
                    serviceManager.getTaskService().getStudentTaskTablePages(studentId, subjectId)
            );
            modelAndView.addObject(PARAM_SUBJECT, serviceManager.getTaskService().getRuSubjectName(subjectId));
            modelAndView.setViewName(TEACHER_TASKS);
        } else {
            Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
            fillTeacherStudentsPage(serviceManager, modelAndView, session, DEFAULT_PAGE, classId);
            modelAndView.setViewName(TEACHER_STUDENTS);
        }
    }

    public static void fillCreateTaskPage(ControllerServiceManager serviceManager, ModelAndView modelAndView,
                                          HttpSession session, Long studentId) {
        User student = serviceManager.getUserService().getUser(studentId);
        if (student == NULL_VALUE) {
            Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
            fillTeacherStudentsPage(serviceManager, modelAndView, session, DEFAULT_PAGE, classId);
            modelAndView.setViewName(TEACHER_STUDENTS);
        } else {
            modelAndView.addObject(PARAM_STUDENT_NAME, student.getUserInfo().getCutFullName());
            modelAndView.setViewName(TEACHER_CREATE_TASK_PAGE);
        }
    }

    public static void fillAdminClassesPage(ControllerServiceManager serviceManager, ModelAndView modelAndView) {
        modelAndView.addObject(PARAM_CLASSES, serviceManager.getStudyClassService().getAll());
        modelAndView.setViewName(ADMIN_CLASSES_PAGE);
    }

    public static void fillCreateClassPage(ControllerServiceManager serviceManager, ModelAndView modelAndView,
                                           HttpSession session) {
        String locale = (String) session.getAttribute(LOCALE);
        List<List<User>> teachers = serviceManager.getUserService().getTeachersBySubject(locale.equals(RU_LOCALE));
        if (teachers == NULL_VALUE) {
            for (int i = START_ARRAY_COUNT; i < SUBJECT_COUNT; i++)
                modelAndView.addObject(TEACHER_LISTS_PARAMS.get(i), NULL_VALUE);
        } else {
            for (int i = START_ARRAY_COUNT; i < SUBJECT_COUNT; i++)
                modelAndView.addObject(TEACHER_LISTS_PARAMS.get(i), teachers.get(i));
        }
        modelAndView.setViewName(ADMIN_CREATE_CLASS_PAGE);
    }

    public static void fillAdminClassPage(ControllerServiceManager serviceManager, ModelAndView modelAndView,
                                          HttpSession session, Long classId) {
        StudyClassData studyClassData = serviceManager.getStudyClassService().getClassDataByClassId(classId);
        if (studyClassData == NULL_VALUE) {
            fillAdminClassesPage(serviceManager, modelAndView);
            modelAndView.setViewName(ADMIN_CLASSES_PAGE);
        } else {
            modelAndView.addObject(PARAM_CLASS_NAME, studyClassData.getName());
            for (int i = START_ARRAY_COUNT; i < SUBJECT_COUNT; i++) {
                User teacher = studyClassData.getTeachers().get(i);
                if (teacher == NULL_VALUE) {
                    modelAndView.addObject(TEACHER_NAME_PARAMS.get(i), NULL_VALUE);
                    modelAndView.addObject(TEACHER_SURNAME_PARAMS.get(i), NULL_VALUE);
                    modelAndView.addObject(TEACHER_MAIL_PARAMS.get(i), NULL_VALUE);
                } else {
                    modelAndView.addObject(TEACHER_NAME_PARAMS.get(i), teacher.getUserInfo().getFirstName());
                    modelAndView.addObject(TEACHER_SURNAME_PARAMS.get(i), teacher.getUserInfo().getLastName());
                    modelAndView.addObject(TEACHER_MAIL_PARAMS.get(i), teacher.getUserInfo().getMail());
                }
                session.setAttribute(PARAM_CLASS_ID, classId);
            }
            modelAndView.setViewName(ADMIN_CLASS_PAGE);
        }
    }

    public static void fillChangeClassPage(ControllerServiceManager serviceManager, ModelAndView modelAndView,
                                           HttpSession session) {
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        String locale = (String) session.getAttribute(LOCALE);
        StudyClassData studyClassData = serviceManager.getStudyClassService().getClassDataByClassId(classId);
        if (studyClassData == NULL_VALUE) {
            fillAdminClassesPage(serviceManager, modelAndView);
        } else {
            modelAndView.addObject(PARAM_CLASS_NAME, studyClassData.getName());
            for (int i = START_ARRAY_COUNT; i < SUBJECT_COUNT; i++) {
                User teacher = studyClassData.getTeachers().get(i);
                if (teacher == NULL_VALUE) {
                    modelAndView.addObject(DEFAULT_CLASS_TEACHER_PARAMS.get(i), NULL_VALUE);
                } else {
                    modelAndView.addObject(
                            DEFAULT_CLASS_TEACHER_PARAMS.get(i), 
                            teacher.getUserInfo().getFullName()
                    );
                }
            }
            List<List<User>> teachers =
                    serviceManager.getUserService().getTeachersBySubject(locale.equals(RU_LOCALE));
            if (teachers == NULL_VALUE) {
                for (int i = START_ARRAY_COUNT; i < SUBJECT_COUNT; i++)
                    modelAndView.addObject(TEACHER_LISTS_PARAMS.get(i), NULL_VALUE);
            } else {
                for (int i = START_ARRAY_COUNT; i < SUBJECT_COUNT; i++)
                    modelAndView.addObject(TEACHER_LISTS_PARAMS.get(i), teachers.get(i));
            }
            modelAndView.setViewName(ADMIN_CHANGE_CLASS_PAGE);
        }
    }

    public static void fillAdminTeachersPage(ControllerServiceManager serviceManager, ModelAndView modelAndView,
                                             HttpSession session, int page) {
        String locale = (String) session.getAttribute(LOCALE);
        modelAndView.addObject(PARAM_PAGES, serviceManager.getUserService().getTeacherTablePages());
        modelAndView.addObject(
                PARAM_TEACHERS,
                serviceManager.getUserService().getTeachers(locale.equals(RU_LOCALE), page)
        );
        modelAndView.setViewName(ADMIN_TEACHERS_PAGE);
    }

    public static void fillCreateTeacherPage(ModelAndView modelAndView, HttpSession session, String login,
                                  String name, String surname, String email) {
        if (session.getAttribute(LOCALE).equals(EN_LOCALE)) {
            modelAndView.addObject(PARAM_ERROR, SIGN_UP_INCORRECT_LOGIN_EN_MESSAGE);
        } else {
            modelAndView.addObject(PARAM_ERROR, SIGN_UP_INCORRECT_LOGIN_RU_MESSAGE);
        }
        modelAndView.addObject(PARAM_LOGIN, login);
        modelAndView.addObject(PARAM_NAME, name);
        modelAndView.addObject(PARAM_SURNAME, surname);
        modelAndView.addObject(PARAM_EMAIL, email);
        modelAndView.setViewName(ADMIN_CREATE_TEACHER_PAGE);
    }

    public static void fillAdminStudentsPage(ControllerServiceManager serviceManager, ModelAndView modelAndView,
                                             HttpSession session, int page, Long classId) {
        if (classId.equals(NO_CLASS_ID)) {
            modelAndView.addObject(PARAM_PAGES, serviceManager.getUserService().getNoClassStudentTablePages());
            modelAndView.addObject(PARAM_STUDENTS, serviceManager.getUserService().getNoClassStudents(page));
        } else {
            modelAndView.addObject(PARAM_PAGES, serviceManager.getUserService().getClassStudentTablePages(classId));
            modelAndView.addObject(PARAM_STUDENTS, serviceManager.getUserService().getClassStudents(classId, page));
        }
        session.setAttribute(PARAM_CLASS_ID, classId);
        modelAndView.setViewName(ADMIN_STUDENTS_PAGE);
    }

    public static void fillAdminStudentPage(ControllerServiceManager serviceManager, ModelAndView modelAndView,
                                            HttpSession session, Long studentId) {
        User student = serviceManager.getUserService().getUser(studentId);
        if (student == NULL_VALUE) {
            Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
            PageFiller.fillAdminStudentsPage(serviceManager, modelAndView, session, DEFAULT_PAGE, classId);
        } else {
            session.setAttribute(PARAM_STUDENT_ID, student.getId());
            modelAndView.addObject(PARAM_STUDENT_LOGIN, student.getLogin());
            modelAndView.addObject(PARAM_STUDENT_NAME, student.getUserInfo().getFirstName());
            modelAndView.addObject(PARAM_STUDENT_SURNAME, student.getUserInfo().getLastName());
            modelAndView.addObject(PARAM_STUDENT_MAIL, student.getUserInfo().getMail());
            if (student.getStudyClass() == NULL_VALUE) {
                modelAndView.addObject(PARAM_STUDENT_CLASS, NULL_VALUE);
            } else {
                modelAndView.addObject(PARAM_STUDENT_CLASS, student.getStudyClass().getName());
            }
            modelAndView.addObject(PARAM_CLASSES, serviceManager.getStudyClassService().getAll());
            modelAndView.setViewName(ADMIN_STUDENT_PAGE);
        }
    }

    public static void fillAdminStudentGradesPage(ControllerServiceManager serviceManager, ModelAndView modelAndView,
                                                  HttpSession session, int page, Long studentId, Long subjectId) {
        User student = serviceManager.getUserService().getUser(studentId);
        if (student == NULL_VALUE) {
            Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
            PageFiller.fillAdminStudentsPage(serviceManager, modelAndView, session, DEFAULT_PAGE, classId);
        } else {
            List<Grade> grades = serviceManager.getGradeService().getGradesByUserId(
                    student.getId(),
                    session.getAttribute(LOCALE).equals(RU_LOCALE)
            );
            List<Double> subjectMarks = serviceManager.getGradeService().getAverageMark(grades);
            for (int i = 0; i <= SUBJECT_COUNT; i++) {
                modelAndView.addObject(MARK_PARAMS.get(i), subjectMarks.get(i));
            }
            if (!subjectId.equals(ALL_SUBJECTS_ID)) {
                grades = grades
                        .stream()
                        .filter(g -> g.getSubject().getId().equals(subjectId)).collect(Collectors.toList());
            }
            modelAndView.addObject(PARAM_PAGES, serviceManager.getGradeService().getGradeTablePages(grades));
            modelAndView.addObject(PARAM_GRADES, serviceManager.getGradeService().getCurrentPageGrades(grades, page));
            session.setAttribute(PARAM_SUBJECT_ID, subjectId);
            modelAndView.setViewName(ADMIN_STUDENT_GRADES_PAGE);
        }
    }

    public static void setIncorrectPasswordsError(HttpSession session, ModelAndView modelAndView) {
        if (session.getAttribute(LOCALE).equals(EN_LOCALE)) {
            modelAndView.addObject(PARAM_ERROR, CHANGE_PASSWORD_INCORRECT_INPUT_EN_MESSAGE);
        } else {
            modelAndView.addObject(PARAM_ERROR, CHANGE_PASSWORD_INCORRECT_INPUT_RU_MESSAGE);
        }
    }

    public static void setEqualPasswordsError(HttpSession session, ModelAndView modelAndView) {
        if (session.getAttribute(LOCALE).equals(EN_LOCALE)) {
            modelAndView.addObject(PARAM_ERROR, CHANGE_PASSWORD_EQUAL_INPUT_EN_MESSAGE);
        } else {
            modelAndView.addObject(PARAM_ERROR, CHANGE_PASSWORD_EQUAL_INPUT_RU_MESSAGE);
        }
    }
}
