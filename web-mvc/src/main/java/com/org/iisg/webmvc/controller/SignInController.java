package com.org.iisg.webmvc.controller;

import com.org.iisg.model.Role;
import com.org.iisg.model.User;
import com.org.iisg.service.impl.manager.ControllerServiceManager;
import com.org.iisg.webmvc.filler.PageFiller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

import static com.org.iisg.webmvc.parameter.ControllerParameter.*;
import static com.org.iisg.webmvc.parameter.ErrorMessages.SIGN_IN_INCORRECT_INPUT_EN_MESSAGE;
import static com.org.iisg.webmvc.parameter.ErrorMessages.SIGN_IN_INCORRECT_INPUT_RU_MESSAGE;
import static com.org.iisg.webmvc.parameter.PageParameter.*;

@Controller
public class SignInController {

    @Autowired
    private ControllerServiceManager serviceManager;

    @RequestMapping(path = "/signIn", method = RequestMethod.GET)
    public void start() {
    }

    @RequestMapping(path = "/signIn", method = RequestMethod.POST, params = "signIn1")
    public String redirectToStartPage() {
        return REDIRECT_VALUE + START_PAGE;
    }

    @RequestMapping(path = "/signIn", method = RequestMethod.POST, params = "signIn2")
    public String redirectToSignUpPage() {
        return REDIRECT_VALUE + SIGN_UP_PAGE;
    }

    @RequestMapping(
            path = "/signIn",
            method = RequestMethod.POST,
            params = {
                    "login",
                    "password"
            }
    )
    public ModelAndView comeIn(@RequestParam("login") String login, @RequestParam("password") String password,
                               HttpSession session, ModelAndView modelAndView) {
        final User user = serviceManager.getUserService().authorize(login, password);
        if (user == null) {
            modelAndView.addObject(
                    PARAM_ERROR,
                    session.getAttribute(LOCALE).equals(EN_LOCALE)
                            ? SIGN_IN_INCORRECT_INPUT_EN_MESSAGE
                            : SIGN_IN_INCORRECT_INPUT_RU_MESSAGE
            );
            return modelAndView;
        } else {
            final Role role = user.getRole();
            session.setAttribute(PARAM_ID, user.getId());
            session.setAttribute(PARAM_LOGIN, user.getLogin());
            session.setAttribute(PARAM_ROLE, role.getId());
            if (role.getId().equals(STUDENT_ID)) {
                ModelAndView newModelAndView = new ModelAndView();
                PageFiller.fillStudentPage(
                        serviceManager,
                        newModelAndView,
                        session,
                        DEFAULT_PAGE,
                        ALL_SUBJECTS_ID,
                        user.getId()
                );
                return newModelAndView;
            } else if (role.getId().equals(TEACHER_ID)) {
                ModelAndView newModelAndView = new ModelAndView();
                PageFiller.fillTeacherPage(
                        serviceManager,
                        newModelAndView,
                        session,
                        user.getId(),
                        user.getSubject().getId()
                );
                return newModelAndView;
            } else if (role.getId().equals(ADMIN_ID)) {
                return new ModelAndView(ADMIN_PAGE);
            } else {
                modelAndView.addObject(
                        PARAM_ERROR,
                        session.getAttribute(LOCALE).equals(EN_LOCALE) ? SIGN_IN_INCORRECT_INPUT_EN_MESSAGE
                                : SIGN_IN_INCORRECT_INPUT_RU_MESSAGE
                );
                return modelAndView;
            }
        }
    }
}
