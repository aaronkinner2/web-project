package com.org.iisg.webmvc.controller;

import com.org.iisg.service.impl.manager.ControllerServiceManager;
import com.org.iisg.webmvc.filler.PageFiller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

import java.time.LocalDate;

import static com.org.iisg.webmvc.parameter.ControllerParameter.*;
import static com.org.iisg.webmvc.parameter.PageParameter.SIGN_IN_PAGE;

@Controller
public class TeacherController {

    @Autowired
    private ControllerServiceManager serviceManager;

    @RequestMapping(path = "/teacher", method = RequestMethod.GET)
    public void start() {
    }

    @RequestMapping(path = "/teacher", method = RequestMethod.POST, params = "classId")
    public ModelAndView viewStudentsPage(@RequestParam("classId") Long classId,
                                         HttpSession session,
                                         ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            if (classId == null) {
                PageFiller.fillTeacherPage(serviceManager, modelAndView, session, userId, subjectId);
            } else {
                PageFiller.fillTeacherStudentsPage(serviceManager, modelAndView, session, DEFAULT_PAGE, classId);
            }
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/teacherStudents", method = RequestMethod.POST)
    public ModelAndView viewMainTeacherPage(HttpSession session, ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillTeacherPage(serviceManager, modelAndView, session, userId, subjectId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/teacherGrades", method = RequestMethod.POST, params = "main1")
    public ModelAndView viewStudentsPage(HttpSession session, ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillTeacherStudentsPage(serviceManager, modelAndView, session, DEFAULT_PAGE, classId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/teacherTasks", method = RequestMethod.POST, params = "main")
    public ModelAndView viewGradesPage(HttpSession session, ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillTeacherGradesPage(
                    serviceManager,
                    modelAndView,
                    session,
                    DEFAULT_PAGE,
                    studentId,
                    subjectId,
                    classId
            );
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/teacherGrades", method = RequestMethod.POST)
    public ModelAndView viewTasksPage(HttpSession session, ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillTeacherTasksPage(serviceManager, modelAndView, session, DEFAULT_PAGE, studentId, subjectId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/teacherCreateTask", method = RequestMethod.POST, params = "main1")
    public ModelAndView viewTaskPage(HttpSession session, ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillTeacherTasksPage(serviceManager, modelAndView, session, DEFAULT_PAGE, studentId, subjectId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/teacherStudents", method = RequestMethod.POST, params = "pageId")
    public ModelAndView changeTableStudents(@RequestParam("pageId") Integer pageId, HttpSession session,
                                            ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillTeacherStudentsPage(serviceManager, modelAndView, session, pageId, classId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/teacherStudents", method = RequestMethod.POST, params = "studentId")
    public ModelAndView viewGradesPage(@RequestParam("studentId") Long studentId, HttpSession session,
                                       ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillTeacherGradesPage(
                    serviceManager,
                    modelAndView,
                    session,
                    DEFAULT_PAGE,
                    studentId,
                    subjectId,
                    classId
            );
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/teacherGrades", method = RequestMethod.POST, params = "pageId")
    public ModelAndView changeGradesTable(@RequestParam("pageId") Integer pageId, HttpSession session,
                                          ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillTeacherGradesPage(
                    serviceManager,
                    modelAndView,
                    session,
                    pageId,
                    studentId,
                    subjectId,
                    classId
            );
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/teacherGrades", method = RequestMethod.POST, params = "gradeId")
    public ModelAndView deleteGrade(@RequestParam("gradeId") Long gradeId, HttpSession session,
                                    ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        serviceManager.getGradeService().deleteGrade(gradeId);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillTeacherGradesPage(
                    serviceManager,
                    modelAndView,
                    session,
                    DEFAULT_PAGE,
                    studentId,
                    subjectId,
                    classId
            );
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/teacherGrades", method = RequestMethod.POST, params = {"mark", "date"})
    public ModelAndView addGrade(@RequestParam("mark") Integer mark, @RequestParam("date") String date,
                                 HttpSession session, ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        serviceManager.getGradeService().addGrade(studentId, mark, LocalDate.parse(date), subjectId);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillTeacherGradesPage(
                    serviceManager,
                    modelAndView,
                    session,
                    DEFAULT_PAGE,
                    studentId,
                    subjectId,
                    classId
            );
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/teacherTasks", method = RequestMethod.POST, params = "pageId")
    public ModelAndView changeTasksTable(@RequestParam("pageId") Integer pageId, HttpSession session,
                                         ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillTeacherTasksPage(serviceManager, modelAndView, session, pageId, studentId, subjectId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/teacherTasks", method = RequestMethod.POST)
    public ModelAndView viewCreateTaskPage(HttpSession session, ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long studentId = (Long) session.getAttribute(PARAM_STUDENT_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillCreateTaskPage(serviceManager, modelAndView, session, studentId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/teacherCreateTask", method = RequestMethod.POST, params = {"studentId", "description"})
    public ModelAndView createTask(@RequestParam("studentId") Long studentId, @RequestParam("description") String text,
                                   HttpSession session, ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long classId = (Long) session.getAttribute(PARAM_CLASS_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        if (studentId.equals(ALL_STUDENTS_ID)) {
            serviceManager.getTaskService().create(text, studentId, subjectId);
        } else {
            serviceManager.getTaskService().createForClass(text, classId, subjectId);
        }
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillCreateTaskPage(serviceManager, modelAndView, session, studentId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }
}
