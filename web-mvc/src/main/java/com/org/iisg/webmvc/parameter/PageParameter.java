package com.org.iisg.webmvc.parameter;

public interface PageParameter {

    String STUDENT_PAGE = "student";

    String ADMIN_PAGE = "admin";

    String TEACHER_PAGE = "teacher";

    String PROFILE_PAGE = "profile";

    String STUDENT_TASKS_PAGE = "studentTasks";

    String TEACHER_CREATE_TASK_PAGE = "teacherCreateTask";

    String TEACHER_GRADES = "teacherGrades";

    String TEACHER_STUDENTS = "teacherStudents";

    String TEACHER_TASKS = "teacherTasks";

    String START_PAGE = "start";

    String SIGN_IN_PAGE = "signIn";

    String SIGN_UP_PAGE = "signUp";

    String ADMIN_CHANGE_CLASS_PAGE = "adminChangeClass";

    String ADMIN_CLASS_PAGE = "adminClass";

    String ADMIN_CLASSES_PAGE = "adminClasses";

    String ADMIN_CREATE_CLASS_PAGE = "adminCreateClass";

    String ADMIN_CREATE_TEACHER_PAGE = "adminTeacherClass";

    String ADMIN_STUDENT_PAGE = "adminStudent";

    String ADMIN_STUDENTS_PAGE = "adminStudents";

    String ADMIN_TEACHERS_PAGE = "adminTeachers";

    String ADMIN_STUDENT_GRADES_PAGE = "adminStudentGrades";

    String REDIRECT_VALUE = "redirect:";
}
