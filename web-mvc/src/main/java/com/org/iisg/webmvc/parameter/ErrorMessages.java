package com.org.iisg.webmvc.parameter;

public interface ErrorMessages {

    String SIGN_UP_INCORRECT_LOGIN_EN_MESSAGE = "This login is already used. Use another.";

    String SIGN_UP_INCORRECT_LOGIN_RU_MESSAGE = "Этот логин уже занят. Используйте другой.";

    String SIGN_IN_INCORRECT_INPUT_EN_MESSAGE = "Username or password are incorrect";

    String SIGN_IN_INCORRECT_INPUT_RU_MESSAGE = "Логин или пароль введены неверно";

    String CHANGE_PASSWORD_INCORRECT_INPUT_EN_MESSAGE = "You didn't input your old password correct";

    String CHANGE_PASSWORD_INCORRECT_INPUT_RU_MESSAGE = "Вы неправильно ввели старый пароль";

    String CHANGE_PASSWORD_EQUAL_INPUT_EN_MESSAGE = "Old and new passwords are equal";

    String CHANGE_PASSWORD_EQUAL_INPUT_RU_MESSAGE = "Старый и новый пароль совпадают";
}
