package com.org.iisg.webmvc.controller;

import com.org.iisg.model.User;
import com.org.iisg.service.impl.manager.ControllerServiceManager;
import com.org.iisg.service.mail.MailService;
import com.org.iisg.webmvc.filler.PageFiller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

import static com.org.iisg.webmvc.parameter.ControllerParameter.*;
import static com.org.iisg.webmvc.parameter.PageParameter.*;

@Controller
public class CommonController {

    @Autowired
    private ControllerServiceManager serviceManager;

    private final MailService mailService = MailService.getInstance();

    @RequestMapping(path = "/profile", method = RequestMethod.GET)
    public void profile() {

    }

    @RequestMapping(path = "/signOut", method = RequestMethod.POST)
    public String signOut(HttpSession session) {
        session.invalidate();
        return REDIRECT_VALUE + START_PAGE;
    }

    @RequestMapping(path = "/profile", method = RequestMethod.POST)
    public ModelAndView viewProfile(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long roleId = (Long) session.getAttribute(PARAM_ROLE);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            if (roleId.equals(STUDENT_ID) || roleId.equals(TEACHER_ID)) {
                PageFiller.fillProfilePage(serviceManager, userId, modelAndView);
            } else {
                modelAndView.setViewName(SIGN_IN_PAGE);
            }
            return modelAndView;
        } else {
            return null;
        }
    }

    @RequestMapping(path = "/profile", method = RequestMethod.POST, params = "newEmail")
    public ModelAndView changeEmail(@RequestParam("newEmail") String email,
                                    HttpSession session,
                                    ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        String login = (String) session.getAttribute(PARAM_LOGIN);
        serviceManager.getUserService().changeMail(userId, email);
        mailService.sendEmailChangeMessage(login, email);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillProfilePage(serviceManager, userId, modelAndView);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/profile", method = RequestMethod.POST, params = {"oldPassword", "newPassword"})
    public ModelAndView changePassword(@RequestParam("oldPassword") String oldPassword,
                                       @RequestParam("newPassword") String newPassword,
                                       HttpSession session,
                                       ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        String login = (String) session.getAttribute(PARAM_LOGIN);
        if (serviceManager.getUserService().checkEqualityOfPasswords(oldPassword, newPassword)) {
            PageFiller.setEqualPasswordsError(session, modelAndView);
        } else if (serviceManager.getUserService().changePassword(userId, oldPassword, newPassword)) {
            mailService.sendPasswordChangeMessage(
                    login,
                    serviceManager.getUserService().getUser(userId).getUserInfo().getMail()
            );
        } else {
            PageFiller.setIncorrectPasswordsError(session, modelAndView);
        }
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillProfilePage(serviceManager, userId, modelAndView);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/main", method = RequestMethod.POST)
    public ModelAndView viewMainPage(ModelAndView modelAndView, HttpSession session) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long roleId = (Long) session.getAttribute(PARAM_ROLE);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            if (roleId.equals(STUDENT_ID)) {
                PageFiller.fillStudentPage(serviceManager, modelAndView, session, DEFAULT_PAGE, ALL_SUBJECTS_ID, userId);
            } else if (roleId.equals(TEACHER_ID)) {
                User user = serviceManager.getUserService().getUser(userId);
                PageFiller.fillTeacherPage(serviceManager, modelAndView, session, userId, user.getSubject().getId());
            }
        } else {
            session.invalidate();
            modelAndView.setViewName(START_PAGE);
        }
        return modelAndView;
    }
}
