package com.org.iisg.webmvc.security;

import com.org.iisg.model.User;
import com.org.iisg.service.impl.manager.ControllerServiceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SecurityAuthService implements UserDetailsService {

    @Autowired
    private ControllerServiceManager serviceManager;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = serviceManager.getUserService().getUserByLogin(login);
        if (user == null) {
            throw new UsernameNotFoundException("Such user don't exists");
        } else {
            return new org.springframework.security.core.userdetails.User(
                    user.getLogin(),
                    user.getPassword(),
                    getAuthorities(user)
            );
        }
    }

    private List<GrantedAuthority> getAuthorities(User user) {
        List<GrantedAuthority> result = new ArrayList<>();
        switch (user.getRole().getId().intValue()) {
            case 1:
                result.add(new SimpleGrantedAuthority("ROLE_STUDENT"));
                break;
            case 2:
                result.add(new SimpleGrantedAuthority("ROLE_TEACHER"));
                break;
            case 3:
                result.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
                break;
        }
        return result;
    }

}
