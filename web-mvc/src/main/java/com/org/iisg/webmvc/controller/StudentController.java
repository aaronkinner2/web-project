package com.org.iisg.webmvc.controller;

import com.org.iisg.service.impl.manager.ControllerServiceManager;
import com.org.iisg.webmvc.filler.PageFiller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

import static com.org.iisg.webmvc.parameter.ControllerParameter.*;
import static com.org.iisg.webmvc.parameter.PageParameter.SIGN_IN_PAGE;

@Controller
public class StudentController {

    @Autowired
    private ControllerServiceManager serviceManager;

    @RequestMapping(path = "/student", method = RequestMethod.GET)
    public void start() {
    }

    @RequestMapping(path = "/student", method = RequestMethod.POST, params = "pageId")
    public ModelAndView changePageMainPage(@RequestParam("pageId") Integer pageId,
                                           HttpSession session,
                                           ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillStudentPage(serviceManager, modelAndView, session, pageId, subjectId, userId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/student", method = RequestMethod.POST, params = "subjectId")
    public ModelAndView changeSubjectMainPage(@RequestParam("subjectId") Long subjectId,
                                              HttpSession session,
                                              ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillStudentPage(serviceManager, modelAndView, session, DEFAULT_PAGE, subjectId, userId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/student", method = RequestMethod.POST, params = "pageSubjectId")
    public ModelAndView viewTasksPage(@RequestParam("pageSubjectId") Long pageSubjectId,
                                      HttpSession session,
                                      ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillStudentTasksPage(serviceManager, modelAndView, session, DEFAULT_PAGE, pageSubjectId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/student", method = RequestMethod.POST)
    public ModelAndView viewStudentPage(HttpSession session, ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillStudentPage(serviceManager, modelAndView, session, DEFAULT_PAGE, ALL_SUBJECTS_ID, userId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/studentTasks", method = RequestMethod.POST, params = "pageId")
    public ModelAndView changePageTasksPage(@RequestParam("pageId") Integer pageId,
                                            HttpSession session,
                                            ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillStudentTasksPage(serviceManager, modelAndView, session, pageId, subjectId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/studentTasks", method = RequestMethod.POST, params = "subjectId")
    public ModelAndView changeSubjectTasksPage(@RequestParam("subjectId") Long subjectId,
                                               HttpSession session,
                                               ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            PageFiller.fillStudentTasksPage(serviceManager, modelAndView, session, DEFAULT_PAGE, subjectId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/studentTasks", method = RequestMethod.POST, params = "taskId")
    public ModelAndView markTask(@RequestParam("taskId") Long taskId,
                                 HttpSession session,
                                 ModelAndView modelAndView) {
        Long userId = (Long) session.getAttribute(PARAM_ID);
        Long subjectId = (Long) session.getAttribute(PARAM_SUBJECT_ID);
        if (serviceManager.getUserService().isAccountExists(userId)) {
            if (taskId != null) {
                serviceManager.getTaskService().deleteTask(taskId);
            }
            PageFiller.fillStudentTasksPage(serviceManager, modelAndView, session, DEFAULT_PAGE, subjectId);
        } else {
            modelAndView.setViewName(SIGN_IN_PAGE);
        }
        return modelAndView;
    }
}
